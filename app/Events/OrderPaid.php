<?php

namespace App\Events;

use App\Lian\Module\Contracts\OrderTicket;
use App\Models\Order;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class OrderPaid
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    protected $ticket;

    /**
     * Create a new event instance.
     *
     * @param OrderTicket $ticket 订单凭据
     *
     * @return void
     */
    public function __construct(OrderTicket $ticket)
    {
        $this->ticket = $ticket;
    }

    /**
     * @return OrderTicket
     */
    public function getTicket(): OrderTicket
    {
        return $this->ticket;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {

    }
}
