<?php

namespace App\Console\Commands;

use Encore\Admin\Auth\Database\Menu;
use Illuminate\Console\Command;

class MenuWrite extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'menu:write';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'admin menu table write to config menu';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     */
    public function handle()
    {
        $menus = Menu::all()->keyBy('id')->toArray();


        $text = '<?php ' . PHP_EOL;
        $text .= 'return ';
        $text .= var_export($menus, true);
        $text .= ';';

        $text = str_ireplace('array (', '[', $text);
        $text = str_ireplace(')', ']', $text);


        file_put_contents(config_path('menus.php'), $text);
    }
}
