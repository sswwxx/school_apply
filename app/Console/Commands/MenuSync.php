<?php

namespace App\Console\Commands;

use Encore\Admin\Auth\Database\Menu;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;

class MenuSync extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'menu:sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'sync config menus to database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     */
    public function handle()
    {
        $menus = config('menus', []);

        Menu::query()->where('id', '!=', '0')->delete();

        foreach ($menus as $menu) {
            $menu = Arr::only($menu, [
                    'id',
                    'parent_id',
                    'order',
                    'title',
                    'icon',
                    'uri',
                    'created_at',
                    'updated_at',
                    'permission',
                ]
            );

            if (empty($menu['id'])) continue;

            $dbMenu = new Menu();


            $dbMenu->setRawAttributes($menu);
            $dbMenu->save();
        }

        $this->line('menus synced');
    }
}
