<?php

namespace App\Console;

use App\Lian\Module\Manager;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $this->modulesSchedule($schedule);
    }

    protected function modulesSchedule(Schedule $schedule)
    {
        /** @var Manager $manager */
        $manager = resolve(Manager::class);
        $enabled = option('modules_enabled', []);
        foreach ($enabled as $slug) {
            try {
                $module = $manager->module($slug);
                $module->schedule($schedule);
            } catch (\Exception $exception) {

            }
        }
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
