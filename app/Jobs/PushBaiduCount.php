<?php

namespace App\Jobs;

use App\Lian\Module\Manager;
use App\Lian\Module\Modules\BaiduPush;
use App\Models\Column;
use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class PushBaiduCount implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $module;

    /**
     * Create a new job instance.
     *
     * @param BaiduPush $module
     *
     * @return void
     */
    public function __construct(BaiduPush $module)
    {
        $this->module = $module;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws
     *
     */
    public function handle()
    {
        $site = url('/');

        $token = $this->module->config('token');

        if (empty($token)) return;

        $urls = Column::allColumns()->pluck('url')->implode("\n");

        $api = "http://data.zz.baidu.com/urls?site={$site}&token={$token}";

        $client = new Client();

        $client->post($api, [
            'body' => $urls
        ]);
    }
}
