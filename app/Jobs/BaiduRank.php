<?php

namespace App\Jobs;

use App\Lian\Module\Modules\BaiduPush;
use App\Models\Baidu\Keyword;
use App\QueryRules\Baidu;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Collection;
use QL\QueryList;

class BaiduRank implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /** @var BaiduPush */
    protected $module;

    /**
     * Create a new job instance.
     *
     * @param BaiduPush $module
     *
     * @return void
     */
    public function __construct(BaiduPush $module)
    {
        $this->module = $module;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $keywords = $this->module->config('keywords');

        $keywords = explode(',', $keywords);

        if (empty($keywords)) return;

        $ql = QueryList::getInstance();
        $ql->use(Baidu::class);
        $baidu = $ql->baidu(50);

        foreach ($keywords as $keyword) {

            $searcher = $baidu->search($keyword, true);
            $searcher->getCount();

            /** @var Collection $data */
            $data = $searcher->page(1, true);

            $url = url('/');

            $rank = 0;

            foreach ($data as $key => $datum) {

                if (strpos($datum['link'], $url) === false) continue;

                $rank = $key;

                break;
            }

            if ($rank === 0) continue;

            Keyword::create(['keyword' => $keyword, 'rank' => $rank]);
        }
    }
}
