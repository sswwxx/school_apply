<?php
/**
 * Created by PhpStorm.
 * User: lian
 * Date: 2019/12/26
 * Time: 15:03
 */

namespace App\Lian\Helper;


use App\Lian\Module\Contracts\Module;

class OptionKey
{
    public static function moduleTemplate(Module $module): string
    {
        return 'modules.' . $module->getSlug() . '.template';
    }

    public static function moduleConfig(Module $module): string
    {
        return 'modules.' . $module->getSlug() . '.config';
    }
}
