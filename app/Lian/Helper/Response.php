<?php


namespace App\Lian\Helper;


use Illuminate\Support\ViewErrorBag;

class Response
{
    public static function success($data = [], $message = '')
    {
        return static::status(true, $data, $message);
    }

    public static function error($message = '', $data = [])
    {
        return static::status(false, $data, $message);
    }

    public static function status($status, $data = [], $message = '')
    {
        return \Response::json(['status' => $status, 'data' => $data, 'message' => $message]);
    }

    public static function sessionErrors($success = '')
    {
        /** @var ViewErrorBag $errors */
        $errors = session('errors');

        if (empty($errors)) return static::success([], $success);

        session()->remove('errors');

        return static::error('', $errors->all());
    }

}