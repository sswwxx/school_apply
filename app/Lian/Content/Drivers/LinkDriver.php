<?php
/**
 * Created by PhpStorm.
 * User: lian
 * Date: 2019/12/7
 * Time: 11:12
 */

namespace App\Lian\Content\Drivers;


use App\Admin\Components\Form;
use App\Lian\Content\Driver;
use App\Lian\Content\Router;

class LinkDriver extends Driver
{
    protected $slug = 'LINK';

    protected $name = '自定义链接';

    protected $hasAdminPage = false;


    public function route(Router $router): void
    {
        $router->get('/', 'redirect');
    }

    public function templates(): array
    {
        return [];
    }

    public function configForm(Form $form)
    {
        $form->url('url', '跳转地址');
    }

    public function redirect()
    {
        $url = $this->config('url', '/');

        return redirect($url);
    }
}
