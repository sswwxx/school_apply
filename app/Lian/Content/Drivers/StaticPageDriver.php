<?php
/**
 * Created by PhpStorm.
 * User: lian
 * Date: 2019/11/28
 * Time: 15:18
 */

namespace App\Lian\Content\Drivers;

use App\Lian\Content\Driver;
use App\Lian\Content\Router;
use App\Lian\Content\View;

class StaticPageDriver extends Driver
{
    protected $slug = 'STATIC_PAGE';

    protected $name = '静态页面';

    protected $hasAdminConfig = false;

    protected $hasAdminPage = false;


    public function route(Router $router): void
    {
        $router->get('/', 'index');
    }


    public function index()
    {
        return new View('tpl');
    }

    public function templates(): array
    {
        return ['tpl' => '模板页面'];
    }
}
