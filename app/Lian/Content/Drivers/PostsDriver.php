<?php
/**
 * Created by PhpStorm.
 * User: lian
 * Date: 2019/10/8
 * Time: 16:34
 */

namespace App\Lian\Content\Drivers;

use App\Admin\Components\Form;
use App\Admin\Components\Grid;
use App\Admin\ViewModel\Column;
use App\Lian\Content\Driver;
use App\Lian\Content\Router;
use App\Lian\Content\View as Page;
use App\Models\Post;
use Encore\Admin\Grid\Filter;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;


class PostsDriver extends Driver
{
    protected $slug = 'POSTS';

    protected $name = '文章内容';

    public function adminPage()
    {
        $grid = new Grid(new Post);

        $grid->disableExport();

        $grid->model()->orderByDesc('created_at');
        $grid->model()->orderByDesc('id');

        $grid->filter(function (Filter $filter) {
            $filter->like('title', '文章标题');
            $filter->between('created_at', '添加时间')->datetime();
        });

        $grid->model()->where('column_id', $this->column->id);
        $grid->quickSearch(function ($model, $keywords) {
            /** @var  Builder $model */
            $model->where('title', 'like', "%{$keywords}%");
        });


        $grid->column('id', '#');
        $grid->column('order', '排序值');
        $grid->column('thumb', '文章封面')->image();
        $grid->column('title', '文章标题')->display(function ($title) {
            return "<a target='_blank' href='{$this->url}'>{$title}</a>";
        });
        $grid->column('column.name', '栏目');

        $grid->column('created_at', '添加时间');

        $grid->column('top', '置顶')->switch($this->topSwitch());
        $grid->column('hot', '热门')->switch($this->hotSwitch());
        $grid->column('recommend', '推荐')->switch($this->recommendSwitch());

        $grid->column('view_num', '浏览量');

        return $grid;
    }

    protected function topSwitch()
    {
        return [
            'on' => ['value' => 1, 'text' => '是', 'color' => 'danger'],
            'off' => ['value' => 0, 'text' => '否', 'color' => 'default'],
        ];
    }

    protected function hotSwitch()
    {
        return [
            'on' => ['value' => 1, 'text' => '是', 'color' => 'danger'],
            'off' => ['value' => 0, 'text' => '否', 'color' => 'default'],
        ];
    }

    protected function recommendSwitch()
    {
        return [
            'on' => ['value' => 1, 'text' => '是', 'color' => 'danger'],
            'off' => ['value' => 0, 'text' => '否', 'color' => 'default'],
        ];
    }

    public function configForm(Form $form)
    {
        $form->text('rows', '列表显示条数');
    }

    public function adminForm(): Form
    {
        $form = new Form(new Post());

        $columns = collect(Column::selectOptions())->filter(function ($val, $key) {
            $column = Column::findColumn($key);

            return $column->type == $this->column->type;
        });

        $form->select('column_id', '所属栏目')->options($columns)
            ->default($this->column->id)
            ->required();

        $form->text('title', '文章标题')->required()->rules('required');
        $form->media('thumb', '封面图片')->required()->images();
        $form->text('author', '文章作者');
        $form->textarea('introduction', '文章概述');

        $form->editor('content', '内容')->required()->rules('required');

        $form->number('order', '排序')->default(0)
            ->help('倒序排列，数值越大越靠前');

        $column = $this->column;
        $form->saving(function (Form $form) use ($column) {

            $form->model()->setAttribute('user_id', user_id());

        });

        return $form;
    }

    public function templates(): array
    {
        return ['lists' => '列表', 'detail' => '详细'];
    }

    public function route(Router $router): void
    {
        $router->get('/', 'index');
        $router->get('/{id}', 'detail');
    }

    public function index()
    {
        return new Page('lists', $this->paginate($this->config('rows',20)));
    }

    public function paginate($size = 20, $columns = ['*'], $pageName = 'page', $pageNo = null): LengthAwarePaginator
    {
        $children = $this->column->children();
        $children->push($this->column);

        /** @var LengthAwarePaginator $list */
        $list = Post::whereIn('column_id', $children->pluck('id'))
            ->orderByDesc('order')
            ->orderByDesc('created_at')
            ->orderByDesc('id')
            ->paginate($size, '*', $pageName, $pageNo);

        return $list;
    }

    public function detail($id)
    {
        $post = $this->find($id);

        $post->view_num++;

        $post->save();

        return new Page('detail', $post);
    }

    /**
     * @param $id
     * @return Post
     */
    public function find($id): Model
    {
        return Post::find($id);
    }

}
