<?php
/**
 * Created by PhpStorm.
 * User: lian
 * Date: 2019/12/18
 * Time: 17:26
 */

namespace App\Lian\Content\Drivers;


use App\Admin\Components\Form;
use App\Lian\Content\Driver;
use App\Lian\Content\Router;
use App\Lian\Content\View as Page;
use App\Models\Post;
use Encore\Admin\Facades\Admin;

class EditAblePageDriver extends Driver
{
    protected $name = '可编辑页面';

    protected $slug = 'EDIT_ABLE_PAGE';

    protected $hasAdminConfig = false;

    public function templates(): array
    {
        return ['page' => '可编辑页面'];
    }

    public function route(Router $router): void
    {
        $router->get('/', 'index');
        $router->get('/edit', 'edit');
    }

    public function adminPage()
    {
        return view('drivers.editable.view', [
            'url' => $this->column->url . '/edit'
        ]);
    }

    public function adminForm(): Form
    {
        $form = new Form(new Post());

        $form->editor('content', '内容')->required();

        $form->edit($this->post()->id);

        $form->saved(function () {
            return response()->json(['status' => true, 'error' => 0, 'message' => '保存成功']);
        });

        return $form;
    }

    protected function post()
    {
        $post = Post::whereColumnId($this->column->id)->first();

        return $post ?? $this->createPost();
    }

    protected function createPost()
    {
        $post = new Post();
        $post->column_id = $this->column->id;
        $post->title = $this->column->name;
        $post->user_id = 0;
        $post->save();

        return $post;
    }

    public function index()
    {
        return new Page('page', $this->post()->content);
    }

    public function edit()
    {
        if (empty(Admin::user())) abort(404);

        $url = route('admin-column.create', [$this->column->slug]);
        $view = $this->column->url;

        $container = view('drivers.editable.editor', [
            'content' => $this->post()->content,
            'saveUrl' => $url,
            'viewUrl' => $view,
            'uploadUrl' => route('admin.ck-upload') . '?token=' . csrf_token()
        ]);

        return new Page('page', $container->render());
    }

}
