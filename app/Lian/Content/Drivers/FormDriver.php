<?php


namespace App\Lian\Content\Drivers;


use App\Admin\Components\Form;
use App\Admin\Components\Grid;
use App\Admin\ViewModel\Faker;
use App\Lian\Content\Driver;
use App\Lian\Content\Router;
use App\Lian\Content\View;
use App\Models\Post;
use Encore\Admin\Form\NestedForm;

class FormDriver extends Driver
{
    protected $slug = 'form';

    protected $name = '表单';

    protected $hasAdminConfig = true;

    public function adminPage()
    {
        $grid = new Grid(new Post);

        $grid->model()->where('column_id', $this->column->id);

        $fields = $this->config('fields');

        $grid->column('content', $this->column->name)->display(function ($content) use ($fields) {

            $data = json_decode($content, true);

            if (empty($data)) return '';

            $html = '';

            foreach ($fields as $field) {
                $name = $field['field_name'];

                $html .= $field['field_label'] . ' : ' . data_get($data, $name);

                $html .= '<br />';
            }

            return $html;
        });

        $grid->column('created_at', '时间')->date('Y.m.d H:i');

        return $grid;
    }

    public function configForm(Form $form)
    {
        $form->widget('fields', '字段', function (NestedForm $form) {

            $form->text('field_label', '字段名')->required();
            $form->text('field_name', '字段标识')->required();
            $form->text('field_type', '字段类型')->required();
            $form->textarea('filed_params', '规则')
                ->help('字典JSON,键代表调用的方法，值为设置的值');
        });
    }

    public function templates(): array
    {
        return [
            'page' => '表单页面',
        ];
    }

    public function route(Router $router): void
    {
        $router->get('/', 'index');
        $router->post('/', 'store');
    }

    public function index()
    {
        return new View('page', [
            'message' => session('form-message')
        ]);
    }

    public function store()
    {
        $fields = $this->config('fields');

        $form = new Form(new Faker);
        $data = request()->all();

        foreach ($fields as $field) {

            $name = $field['field_name'];

            $fd = $form->{$field['field_type']}($name, $field['field_label']);

            $params = json_decode($field['filed_params'], true);

            if (empty($params)) continue;

            foreach ($params as $method => $value) {
                call_user_func_array([$fd, $method], (array)$value);
            }

            if (empty($data[$name])) $data[$name] = '';
        }


        $form->saved(function (Form $form) {

            $model = $form->model();

            $post = new Post();
            $post->column_id = $this->column->id;
            $post->title = $this->column->name;
            $post->user_id = 0;
            $post->content = json_encode($model->getAttributes());

            $post->save();
        });

        $form->update(0, $data);

        if (session()->has('errors')) {
            $message = session('errors')->all();
        } else {
            $message = '操作成功';
        }

        return redirect($this->column->url)->with('form-message', $message);
    }


}