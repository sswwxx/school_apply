<?php
/**
 * Created by PhpStorm.
 * User: lian
 * Date: 2019/10/9
 * Time: 11:50
 */

namespace App\Lian\Content\Drivers;

use App\Admin\Components\Form;
use App\Lian\Content\Driver;
use App\Lian\Content\Router;
use App\Lian\Content\View;
use App\Models\Post;
use Encore\Admin\Form\Builder;
use Encore\Admin\Form\Tools;
use Illuminate\Database\Eloquent\Model;

class PageDriver extends Driver
{
    protected $slug = 'PAGE';

    protected $name = '单页内容';

    protected $hasAdminConfig = false;

    public function adminPage()
    {
        return $this->adminForm();
    }

    public function adminForm(): Form
    {
        $post = $this->post();

        $form = new Form($post);

        $form->setTitle(' ');

        $form->setAction(url()->current());

        $form->disableEditingCheck();
        $form->disableViewCheck();
        $form->disableCreatingCheck();

        $form->tools(function (Tools $tools) {
            $tools->disableDelete();
            $tools->disableList();
            $tools->disableView();
        });

        $form->saved(function () {

            admin_success('保存成功！');

            return back();
        });

        $form->editor('content', '栏目内容')->rules('required');

        $form = $form->edit($post->id);

        $form->builder()->setMode(Builder::MODE_CREATE);

        return $form;
    }

    protected function post(): Post
    {
        $post = Post::whereColumnId($this->column->id)->orderByDesc('created_at')->first();

        return $post ?: $this->initPost();
    }

    protected function initPost(): Post
    {
        $post = new Post();
        $post->title = $this->column->name;
        $post->content = '';
        $post->user_id = user_id();
        $post->column_id = $this->column->id;
        $post->save();

        return $post;
    }

    public function templates(): array
    {
        return ['single' => '单页'];
    }

    public function find($id): Model
    {
        return $this->post();
    }

    public function route(Router $router): void
    {
        $router->get('/', 'index');
    }

    public function index()
    {
        return new View('single', $this->post());
    }
}
