<?php
/**
 * Created by PhpStorm.
 * User: lian
 * Date: 2019/10/8
 * Time: 16:34
 */

namespace App\Lian\Content\Drivers;

use App\Admin\Components\Form;
use App\Admin\Components\Grid;
use App\Admin\ViewModel\Column;
use App\Lian\Content\Driver;
use App\Lian\Content\Router;
use App\Lian\Content\View as Page;
use App\Models\Post;
use Encore\Admin\Form\EmbeddedForm;
use Encore\Admin\Form\NestedForm;
use Encore\Admin\Grid\Filter;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;


class ProductsDriver extends Driver
{
    protected $slug = 'PRODUCT';

    protected $name = '产品内容';

    protected $hasAdminConfig = true;

    public function adminPage()
    {
        $grid = new Grid(new Post);

        $grid->disableExport();

        $grid->model()->orderByDesc('order');
        $grid->model()->orderByDesc('created_at');
        $grid->model()->orderByDesc('id');

        $grid->filter(function (Filter $filter) {
            $filter->like('title', '产品名称');
            $filter->between('created_at', '添加时间')->datetime();
        });

        $grid->model()->where('column_id', $this->column->id);

        $grid->quickSearch(function ($model, $keywords) {
            /** @var  Builder $model */
            $model->where('title', 'like', "%{$keywords}%");
        });


        $grid->column('id', '#');
        $grid->column('thumb', '产品封面')->image();
        $grid->column('title', '产品标题');
        $grid->column('column.name', '栏目');

        $grid->column('created_at', '添加时间')->date('Y.m.d H:i');

        $grid->column('view_num', '浏览量');

        return $grid;
    }

    public function configForm(Form $form)
    {
        $form->tags('attributes', '产品属性');
    }

    public function adminForm(): Form
    {
        $form = new Form(new Post());

        $form->tab('基础信息', function (Form $form) {

            $columns = collect(Column::selectOptions())->filter(function ($val, $key) {
                $column = Column::findColumn($key);

                return $column->type == $this->column->type;
            });

            $form->select('column_id', '所属栏目')->options($columns)
                ->default($this->column->id)
                ->required();


            $form->text('title', '产品名称')->required()->rules('required');
            $form->media('thumb', '产品封面')->required()->images();
            $form->media('images', '产品图册')->images()->multiple();

            $form->decimal('product.price', '产品价格')->required();
            $form->text('product.code', '产品编码')->required();
            $form->textarea('introduction', '产品概述');


            $attributes = explode(',', $this->config('attributes', ''));

            if ($attributes) {

                $form->embeds('attributes', '产品属性', function (EmbeddedForm $form) use ($attributes) {
                    foreach ($attributes as $attribute) $form->text($attribute,$attribute);
                });

            }


            $form->editor('content', '内容')->required()->rules('required');

            $form->number('order', '排序')->default(0)
                ->help('倒序排列，数值越大越靠前');
        });

        $column = $this->column;

        $form->saving(function (Form $form) use ($column) {

            $form->model()->setAttribute('user_id', user_id());

        });

        return $form;
    }

    public function templates(): array
    {
        return ['products' => '列表', 'products_detail' => '详情'];
    }

    public function detail($id)
    {
        return new Page('products_detail', $this->find($id));
    }

    public function find($id): Model
    {
        return Post::find($id);
    }

    public function route(Router $router): void
    {
        $router->get('/', 'index');
        $router->get('/json', 'json');
        $router->get('/{id}', 'detail');
    }

    public function json()
    {
        return \Response::json(['a' => 1]);
    }

    public function index()
    {
        return new Page('products', $this->paginate());
    }

    public function paginate($size = 20, $columns = ['*'], $pageName = 'page', $pageNo = null): LengthAwarePaginator
    {
        $children = $this->column->children();
        $children->push($this->column);

        /** @var LengthAwarePaginator $list */
        $list = Post::whereIn('column_id', $children->pluck('id'))
            ->orderByDesc('order')
            ->orderByDesc('updated_at')
            ->orderByDesc('id')
            ->paginate($size, '*', $pageName, $pageNo);

        return $list;
    }

}
