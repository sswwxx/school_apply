<?php
/**
 * Created by PhpStorm.
 * User: lian
 * Date: 2019/10/9
 * Time: 17:18
 */

namespace App\Lian\Content;


use App\Admin\Components\Form;
use App\Lian\Content\Contracts\Driver as Contract;
use App\Models\Column;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;

abstract class Driver implements Contract
{
    /** @var Column */
    protected $column;

    /** @var array */
    protected $config = [];

    protected $slug;

    protected $name;

    protected $hasAdminPage = true;

    protected $hasAdminConfig = true;

    /**
     * @param Column $column
     */
    public function setColumn(Column $column = null): void
    {
        $this->column = $column;
    }

    public function column()
    {
        return $this->column;
    }

    public function setConfig(array $config)
    {
        $this->config = $config;
    }

    public function hasAdminPage(): bool
    {
        return $this->hasAdminPage;
    }

    public function hasAdminConfig(): bool
    {
        return $this->hasAdminConfig;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function adminPage()
    {
        return null;
    }

    public function adminForm(): Form
    {
        throw new \Exception("Driver [{$this->getName()}] not support admin form");
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function configForm(Form $form)
    {
        throw new \Exception("Driver [{$this->getName()}] not support config form");
    }

    public function paginate($size = 20, $columns = ['*'], $pageName = 'page', $pageNo = null): LengthAwarePaginator
    {
        throw new \Exception("Driver [{$this->getName()}] not support paginate");
    }

    public function find($id)
    {
        throw new \Exception("Driver [{$this->getName()}] not support find");
    }

    public function config($key, $default = null)
    {
        return data_get($this->config, $key, $default);
    }

    public function schedule(Schedule $schedule)
    {
        throw new \Exception('content driver not support schedule');
    }
}
