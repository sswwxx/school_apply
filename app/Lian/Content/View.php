<?php
/**
 * Created by PhpStorm.
 * User: lian
 * Date: 2019/11/26
 * Time: 15:56
 */

namespace App\Lian\Content;

use App\Lian\Content\Contracts\ContentView as Contract;

class View implements Contract
{
    public $name;

    public $data;

    public function __construct($name, $data = [])
    {
        $this->name = $name;
        $this->data = $data;
    }

    public function getViewName(): string
    {
        return $this->name;
    }

    public function getViewData()
    {
        return $this->data;
    }
}
