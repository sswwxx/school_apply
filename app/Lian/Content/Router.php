<?php
/**
 * Created by PhpStorm.
 * User: lian
 * Date: 2019/11/28
 * Time: 11:59
 */

namespace App\Lian\Content;

use App\Models\Column;
use Illuminate\Routing\Router as IllRouter;

/**
 * Class Router
 * @package App\Lian\Web
 *
 * @see \Illuminate\Routing\Router
 */
class Router
{
    protected $router;

    /**
     * @var Column
     */
    protected $column;

    protected $controller;

    public function __construct(IllRouter $router, $controller)
    {
        $this->router = $router;
        $this->controller = $controller;
    }

    /**
     * @param Column $column
     */
    public function setColumn(Column $column): void
    {
        $this->column = $column;
    }

    public function get($uri, $action = null)
    {
        $this->router->get($this->buildUri($uri), $this->buildAction($action))
            ->name($this->buildName($action));
    }

    protected function buildUri($uri)
    {
        $uri = trim($uri, '/');

        if (empty($this->column->slug)) return $uri;

        return $this->column ? $this->column->slug . "/" . $uri : $uri;
    }

    protected function buildAction($action)
    {
        return $this->controller . '@' . $action;
    }

    protected function buildName($action)
    {
        return $this->column ? $this->column->slug . '.' . $action : $action;
    }

    public function post($uri, $action = null)
    {
        $this->router->post($this->buildUri($uri), $this->buildAction($action))
            ->name($this->buildName($action));
    }

    public function put($uri, $action = null)
    {
        $this->router->put($this->buildUri($uri), $this->buildAction($action))
            ->name($this->buildName($action));
    }

    public function delete($uri, $action = null)
    {
        $this->router->delete($this->buildUri($uri), $this->buildAction($action))
            ->name($this->buildName($action));
    }
}
