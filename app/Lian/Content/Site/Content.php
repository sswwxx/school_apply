<?php
/**
 * Created by PhpStorm.
 * User: lian
 * Date: 2019/11/26
 * Time: 16:15
 */

namespace App\Lian\Content\Site;


use App\Lian\Content\Drivers\StaticPageDriver;
use App\Lian\Content\Contracts\Dispatcher;
use App\Lian\Content\Contracts\Driver;
use App\Lian\Content\Contracts\WidgetRender;
use App\Lian\Content\Router;
use App\Lian\Content\View;
use App\Models\Column;
use App\Models\Option;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Routing\Router as LLRouter;

class Content implements Dispatcher
{
    protected $viewNamespace = 'site';

    /** @var Column */
    protected $column;

    /** @var Menu */
    protected $menus;

    /**
     * @param $slug
     * @param $method
     * @param array $params
     *
     * @return mixed
     * @throws
     *
     */
    public function handle($slug, $method, $params = [])
    {
        $this->column = $column = $this->findColumn($slug);

        if (empty($column)) {
            abort(404);
        }

        $driver = $this->getDriver($column);

        if (!is_callable([$driver, $method])) {
            abort(404);
        }

        $result = call_user_func([$driver, $method], ...$params);

        if ($result instanceof View) {

            $this->setColumnSeo($column);

            $this->menus = new Menu($column);

            return template()->render($this->column, $result);
        }

        return $result;
    }

    /**
     * @param $slug
     *
     * @return Column
     *
     */
    protected function findColumn($slug)
    {
        return Column::allColumns()->where('slug', $slug)->first();
    }

    /**
     * @param $column
     *
     * @return Driver
     *
     * @throws
     */
    protected function getDriver($column)
    {
        return drivers_manager()->columnDriver($column);
    }

    /**
     * 设置栏目SEO信息
     *
     * @param Column $column
     */
    protected function setColumnSeo(Column $column)
    {
        if ($column->seo_title) seo()->setTitle($column->seo_title);
        if ($column->seo_description) seo()->setDescription($column->seo_description);
        if ($column->seo_keywords) seo()->setKeywords($column->seo_keywords);
    }

    /**
     * 获取站点配置信息
     *
     * @param string|$key
     * @param null|string $default
     * @return mixed
     */
    public function option($key, $default = null)
    {
        return Option::get($key, $default);
    }

    /**
     * 返回菜单数组
     *
     * @return array
     */
    public function menus(): array
    {
        return $this->menus->menus();
    }

    /**
     * @param $id
     *
     * @return Column
     */
    public function column($id): Column
    {
        return Column::findColumn($id);
    }

    /**
     * @param LengthAwarePaginator $pager
     * @param array $params
     *
     * @return string
     *
     * @throws \Throwable
     */
    public function renderPager(LengthAwarePaginator $pager, $params = []): string
    {
        $pager->appends(request()->all());

        return view('site::pager', compact('pager', 'params'))->render();
    }

    /**
     * @param LLRouter $llRouter
     *
     * @throws
     */
    public function registerRoutes(LLRouter $llRouter): void
    {
        if (!$this->hasColumnsTable()) return;

        $controller = config('cms.route.web.controller');

        $router = new Router($llRouter, $controller);

        Column::allColumns()->each(function (Column $column) use ($router) {

            $router->setColumn($column);
            $this->getDriver($column)->route($router);
        });
    }

    protected function hasColumnsTable()
    {
        try {
            return \Schema::hasTable('columns');
        } catch (\Exception $exception) {
            return false;
        }
    }

    /**
     * @param $slug
     * @param array $variables
     *
     * @return string
     * @throws
     *
     */
    public function widget($slug, $variables = []): string
    {
        $widget = template()->theme()->widget($slug);

        /** @var WidgetRender $render */
        $render = resolve(WidgetRender::class);

        $data = (array)$this->column->metaValue("widget@{$slug}");

        $variables = array_merge($data, $variables);

        return $render->render($widget, $variables);
    }

    public function breadcrumbs(): array
    {
        return $this->buildBreadcrumbs($this->column);
    }

    protected function buildBreadcrumbs(Column $column)
    {
        $breadcrumbs = [$column->toArray()];

        if ($column->parent_id && $pc = Column::findColumn($column->parent_id)) {

            $breadcrumbs = array_merge($this->buildBreadcrumbs($pc), $breadcrumbs);

        }

        return $breadcrumbs;
    }

    protected function getDefaultDriver()
    {
        return resolve(StaticPageDriver::class);
    }

}
