<?php
/**
 * Created by PhpStorm.
 * User: lian
 * Date: 2019/10/22
 * Time: 11:52
 */

namespace App\Lian\Content\Site;

use App\Lian\Content\Contracts\SiteURLHelper;
use App\Models\Column;
use Illuminate\Support\Facades\Storage;

class URLHelper implements SiteURLHelper
{

    /**
     * @param Column $column
     * @param null $id
     * @return string
     *
     * @throws
     */
    public function columnUrl($column, $id = null): string
    {
        if (is_integer($column)) {
            $column = Column::findColumn($column);
        }

        if (!($column instanceof Column)) {
            throw new \Exception();
        }

        $uri = $id ? $column->slug . '/' . $id : $column->slug;

        return $this->url($uri);
    }

    public function url($uri = ''): string
    {
        if (empty($uri)) $uri = '/';

        $url = url($uri);

        return $url;
    }


    /**
     * 获取站点磁盘URL
     *
     * @param $path
     *
     * @return string
     */
    public function storageUrl($path): string
    {
        if (empty($path)) return '';

        if (url()->isValidUrl($path)) {
            return $path;
        }

        return Storage::disk(config('admin.upload.disk'))->url($path);
    }
}
