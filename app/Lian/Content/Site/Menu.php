<?php
/**
 * Created by PhpStorm.
 * User: lian
 * Date: 2019/10/16
 * Time: 15:08
 */

namespace App\Lian\Content\Site;

use App\Models\Column;
use Illuminate\Support\Collection;

class Menu
{
    protected $path;

    protected $menus;

    protected $breadcrumbs = [];

    protected $column;

    protected $active;

    public function __construct(Column $column)
    {
        $this->column = $column;
        $this->build();
    }

    protected function build()
    {
        $this->breadcrumbs = [];
        $this->menus = $this->buildNestedCollect();

        $this->breadcrumbs = array_reverse($this->breadcrumbs);
    }

    /**
     * Build Nested array.
     *
     * @param Collection $nodes
     * @param int $parentId
     *
     * @return array
     */
    protected function buildNestedCollect(Collection $nodes = null, $parentId = 0): array
    {
        $branch = [];

        if (empty($nodes)) {
            $nodes = $this->allColumns();
        }

        foreach ($nodes as $node) {

            if ($node->parent_id == $parentId) {

                $this->columnActive($node);

                $children = $this->buildNestedCollect($nodes, $node->id);

                $node['active'] = $this->active;

                if (!empty($children)) {
                    $node['children'] = $children;
                }

                $branch[] = $node;
            }
        }

        return $branch;
    }

    protected function allColumns()
    {
        return Column::allColumns()->where('status', 'SHOW');
    }

    protected function columnActive(Column $column)
    {
        $this->active = $this->column->slug === $column->slug;
    }

    public function menus()
    {
        return $this->menus;
    }
}
