<?php
/**
 * Created by PhpStorm.
 * User: lian
 * Date: 2019/11/26
 * Time: 16:16
 */

namespace App\Lian\Content\Site;


use App\Lian\Content\Contracts\SiteDataDispatcher;
use App\Models\Column;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;

class DataDispatcher implements SiteDataDispatcher
{
    /**
     * @param Column|int $cid
     * @param int $size
     * @param array $columns
     * @param string $pageName
     * @param null $pageNo
     *
     * @return LengthAwarePaginator
     *
     * @throws
     */
    public function lists($cid, $size = 20, $columns = ['*'], $pageName = 'page', $pageNo = null): LengthAwarePaginator
    {
        if ($cid instanceof Column) {
            $column = $cid;
        } else {
            $column = Column::findColumn($cid);
        }

        if (empty($column)) return new LengthAwarePaginator(collect(), 0, 1);

        return drivers_manager()->columnDriver($column)->paginate($size, $columns, $pageName, $pageNo);
    }

    /**
     * @param $cid
     * @param $id
     *
     * @return Model
     *
     * @throws
     */
    public function detail($cid, $id): Model
    {
        $column = Column::findColumn($cid);

        if (empty($column)) return null;

        return drivers_manager()->columnDriver($column)->find($id);
    }
}
