<?php
/**
 * Created by PhpStorm.
 * User: lian
 * Date: 2019/11/13
 * Time: 17:08
 */

namespace App\Lian\Content\Template;

use App\Lian\Content\Contracts\TemplatePage;
use App\Lian\Content\Contracts\TemplateTheme;
use App\Lian\Content\Contracts\TemplateWidget;
use App\Lian\Content\Site\Menu;
use App\Models\Option;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use TheSeer\Tokenizer\Exception;

class Theme implements TemplateTheme
{
    use HasWidgets;

    protected $name;

    protected $slug;

    protected $desc;

    protected $pages;

    protected $dir;

    /** @var Menu */
    protected $menus;


    /**
     * @var Collection|null
     */
    protected $config = null;

    public function __construct(Collection $config)
    {
        $this->setConfig($config);
    }

    protected function setConfig(Collection $config)
    {
        $this->config = $config;

        $this->name = $config->get('name', '');
        $this->slug = $config->get('slug', '');
        $this->desc = $config->get('desc', '');
    }

    public function name(): string
    {
        return $this->name;
    }

    public function desc(): string
    {
        return $this->desc;
    }

    public function saveWidgetData($data)
    {
        $data = Arr::only($data, array_keys($this->widgets()));

        Option::set($this->widgetsKey(), $data);
    }


    protected function widgetsKey()
    {
        return "themes.{$this->slug()}.widgets";
    }

    public function slug(): string
    {
        return $this->slug;
    }

    /**
     * @param $slug
     * @return TemplateWidget
     *
     * @throws Exception
     *
     */
    public function widget($slug): TemplateWidget
    {
        if (empty($this->widgets()[$slug])) {
            throw new Exception("Widget [$slug] not found.");
        }

        return $this->widgets()[$slug];
    }

    /**
     * @param string $name
     *
     * @return TemplatePage
     *
     * @throws
     */
    public function getPage($name): TemplatePage
    {
        $page = data_get($this->pages(), $name ?? '404');

        if (is_null($page)) throw new \Exception("Page [{$name}] not found.");

        return $page;
    }

    /**
     * @return TemplatePage[]
     */
    public function pages(): array
    {
        if ($this->pages) return $this->pages;

        $config = $this->config->get('pages', []);

        $pages = [];

        foreach ($config as $item) {

            $collect = collect($item);

            $slug = $collect->get('slug');

            if (!is_string($slug)) continue;

            $pages[$slug] = new Page($collect);
        }

        return $this->pages = $pages;
    }

    /**
     * @return array
     */
    public function getWidgetsData(): array
    {
        return Option::get($this->widgetsKey(), []);
    }
}
