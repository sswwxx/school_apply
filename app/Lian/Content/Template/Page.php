<?php
/**
 * Created by PhpStorm.
 * User: lian
 * Date: 2019/11/14
 * Time: 10:39
 */

namespace App\Lian\Content\Template;


use App\Lian\Content\Contracts\TemplatePage;
use Illuminate\Support\Collection;

class Page implements TemplatePage
{
    use HasWidgets;

    protected $name;

    protected $slug;

    /** @var Collection */
    protected $config;

    public function __construct(Collection $config)
    {
        $this->setConfig($config);
    }

    protected function setConfig($config)
    {
        $this->config = $config;

        $this->name = $this->config->get('name');
        $this->slug = $this->config->get('slug');
    }

    public function name(): string
    {
        return $this->name;
    }

    public function slug(): string
    {
        return $this->slug;
    }
}
