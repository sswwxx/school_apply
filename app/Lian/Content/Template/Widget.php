<?php
/**
 * Created by PhpStorm.
 * User: lian
 * Date: 2019/11/14
 * Time: 11:52
 */

namespace App\Lian\Content\Template;


use App\Admin\Components\Form;
use App\Lian\Content\Contracts\TemplateWidget;
use Encore\Admin\Form\EmbeddedForm;
use Encore\Admin\Form\NestedForm;
use Illuminate\Support\Collection;

class Widget implements TemplateWidget
{
    /** @var Collection */
    protected $config;

    protected $name;

    protected $slug;

    protected $view;

    public function __construct(Collection $config)
    {
        $this->setConfig($config);
    }

    protected function setConfig($config)
    {
        $this->config = $config;

        $this->name = $this->config->get('name');
        $this->slug = $this->config->get('slug');
        $this->view = $this->config->get('view');
    }

    public function view(): string
    {
        return $this->view;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function slug(): string
    {
        return $this->slug;
    }

    public function buildForm(Form $form, $data = [])
    {
        $config = $this->config->get('config');
        $type = $this->config->get('type');

        if ($type === 'many') {

            if (empty($data[$this->slug])) {
                $data[$this->slug] = [];
            }

            $form->widget($this->slug, '', function (NestedForm $form) use ($config) {
                $this->buildFields($form, $config);
            })->fill($data);

        } else {
            $form->embeds($this->slug, '', function (EmbeddedForm $form) use ($config) {
                $this->buildFields($form, $config);
            })->fill($data);
        }

    }

    /**
     * @param mixed $form
     * @param $params
     */
    protected function buildFields($form, $params)
    {
        foreach ($params as $param) $this->buildField($form, collect($param));
    }

    /**
     * @param Form|NestedForm $form
     * @param Collection $params
     */
    protected function buildField($form, $params)
    {
        $column = $params->get('column');
        $label = $params->get('label');
        $type = $params->get('type');

        $field = $form->{$type}($column, $label);

        $params = $params->get('params', []);

        foreach ($params as $method => $value) {
            if (!method_exists($field, $method)) continue;
            $field->{$method}($value);
        }
    }

    public static function createWidgets(array $config)
    {
        $widgets = [];

        foreach ($config as $item) {

            $collect = collect($item);

            $slug = $collect->get('slug');

            if (!is_string($slug)) continue;

            $widgets[$slug] = new static($collect);
        }

        return $widgets;
    }

}
