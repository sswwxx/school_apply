<?php
/**
 * Created by PhpStorm.
 * User: lian
 * Date: 2019/11/13
 * Time: 17:09
 */

namespace App\Lian\Content\Template;

use App\Lian\Content\Contracts\TemplateContract;
use App\Lian\Content\Contracts\TemplateTheme;
use App\Lian\Content\View;
use App\Models\Column;
use App\Models\Option;
use Illuminate\Support\Collection;

class Template implements TemplateContract
{
    public function allThemes(): array
    {
        $dirs = scandir($this->templateDir());

        $dirs = array_map(function ($dir) {

            if ($dir === '.' || $dir === '..') return false;

            if (!is_dir($this->getThemeDir($dir))) return false;

            if (!$this->isValidateTheme($dir)) return false;

            return $this->theme($dir);

        }, $dirs);

        $dirs = array_filter($dirs);

        return array_values($dirs);
    }

    /**
     * 模板文件夹
     *
     * @return string
     */
    protected function templateDir()
    {
        return base_path('template');
    }

    /**
     * 主题文件夹
     *
     * @param TemplateTheme | string $theme
     *
     * @return string
     */
    public function getThemeDir($theme): string
    {
        if ($theme instanceof TemplateTheme) {
            $theme = $theme->slug();
        }

        return $this->templateDir() . '/' . $theme;
    }

    /**
     * 验证主题与配置文件是否存在
     *
     * @param string $name
     *
     * @return bool
     */
    protected function isValidateTheme($name): bool
    {
        return file_exists($this->getThemeDir($name)) && file_exists($this->configFile($name));
    }

    /**
     * 主题配置文件路径
     *
     * @param string $name
     *
     * @return string
     */
    protected function configFile($name)
    {
        return $this->getThemeDir($name) . '/template.json';
    }

    /**
     * @param string $name
     *
     * @throws
     *
     * @return TemplateTheme
     */
    public function theme($name = null): TemplateTheme
    {
        if (is_null($name)) $name = $this->getSettingThemeName();

        if (!$this->isValidateTheme($name) || is_null($config = $this->loadConfig($name))) {

            throw new \Exception("Theme [{$name}] is not validate.");
        }

        return tap(new Theme($config), function ($theme) {
            $this->useTheme($theme);
        });
    }

    protected function getSettingThemeName()
    {
        return option('theme', env('THEME'));
    }

    /**
     * 读取主题配置文件
     *
     * @param string $name
     *
     * @return Collection
     */
    protected function loadConfig($name)
    {
        $config = file_get_contents($this->configFile($name));
        $config = @json_decode($config, true);

        if (empty($config)) return null;

        return collect($config);
    }

    protected function useTheme(TemplateTheme $theme)
    {
        $this->addViewNamespace($this->getThemeDir($theme));
    }

    protected function addViewNamespace($dir)
    {
        view()->addNamespace('site', $dir);
    }


    /**
     * @param Column $column
     * @param View $view
     *
     * @return string
     *
     * @throws
     */
    public function render(Column $column, View $view): string
    {
        $pageName = $view->getViewName();

        $template = $column ? $column->template : [];
        $pageName = data_get($template, $pageName);

        $theme = $this->theme();

        $page = $theme->getPage($pageName);

        if (empty($page)) abort(404);

        $widgetData = $theme->getWidgetsData();

        Option::merge($widgetData);

        $columnData = $column->metaValue($column->getWidgetMetaKey($view->getViewName()));

        Option::merge($columnData);

        view()->share('column', $column);

        return view('site::' . $page->slug(), [
            $page->slug() => $view->getViewData()
        ])->render();
    }
}
