<?php


namespace App\Lian\Content\Template;


trait HasWidgets
{
    protected $widgets;

    public function widgets(): array
    {
        if (!$this->widgets) {
            $config = $this->config->get('widgets', []);
            $this->widgets = Widget::createWidgets($config);
        }

        return $this->widgets;
    }
}