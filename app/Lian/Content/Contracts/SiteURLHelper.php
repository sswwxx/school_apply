<?php
/**
 * Created by PhpStorm.
 * User: lian
 * Date: 2019/11/26
 * Time: 16:20
 */

namespace App\Lian\Content\Contracts;


use App\Models\Column;

interface SiteURLHelper
{
    /**
     * @param string $uri
     * @return string
     */
    public function url($uri = ''): string;

    /**
     * @param Column | int $column
     * @param int | null $id
     * @return string
     */
    public function columnUrl($column, $id = null): string;

    /**
     * @param string $path
     * @return string
     */
    public function storageUrl($path): string;
}
