<?php
/**
 * Created by PhpStorm.
 * User: lian
 * Date: 2019/11/26
 * Time: 17:23
 */

namespace App\Lian\Content\Contracts;


use App\Lian\Content\View;
use App\Models\Column;

interface TemplateContract
{
    public function allThemes(): array;

    public function getThemeDir($theme): string;

    public function theme($name = null): TemplateTheme;

    public function render(Column $column, View $view): string;
}
