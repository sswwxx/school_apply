<?php
/**
 * Created by PhpStorm.
 * User: lian
 * Date: 2019/11/26
 * Time: 15:58
 */

namespace App\Lian\Content\Contracts;


use App\Models\Column;

interface Manager
{
    /**
     * @param Column $column
     *
     * @return Driver
     */
    public function columnDriver(Column $column): Driver;

    /**
     * @param $slug
     *
     * @return Driver
     */
    public function driver($slug): Driver;


    /**
     * @return Driver[]
     */
    public function drivers(): array;
}
