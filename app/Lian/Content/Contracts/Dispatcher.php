<?php
/**
 * Created by PhpStorm.
 * User: lian
 * Date: 2019/11/26
 * Time: 16:40
 */

namespace App\Lian\Content\Contracts;


use App\Models\Column;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Routing\Router;

/**
 * 站点内容接口
 *
 * Interface Dispatcher
 *
 * @package App\Lian\Web\Contracts
 */
interface Dispatcher
{
    /**
     * 菜单数组
     *
     * @return array
     */
    public function menus(): array;

    /**
     * 面包屑导航数组
     *
     * @return array
     */
    public function breadcrumbs(): array;

    /**
     * 获取配置数据
     *
     * @param string $key 配置名
     * @param null $default 默认值
     *
     * @return mixed
     */
    public function option($key, $default = null);

    /**
     * 获取栏目
     *
     * @param $id
     *
     * @return Column
     */
    public function column($id): Column;

    /**
     * 渲染分页
     *
     * @param LengthAwarePaginator $pager
     * @param array $params
     *
     * @return string
     */
    public function renderPager(LengthAwarePaginator $pager, $params = []): string;

    /**
     * 注册路由
     *
     * @param Router $router
     *
     */
    public function registerRoutes(Router $router): void;

    /**
     * 执行，显示内容
     *
     * @param string $slug 栏目标识
     * @param string $method 方法
     * @param array $params 参数
     *
     * @return mixed
     */
    public function handle($slug, $method, $params = []);


    public function widget($name, $variables = []): string;
}
