<?php
/**
 * Created by PhpStorm.
 * User: lian
 * Date: 2019/11/26
 * Time: 17:35
 */

namespace App\Lian\Content\Contracts;


use App\Admin\Components\Form;

interface TemplateWidget
{
    public function buildForm(Form $form, $data = []);

    public function name(): string;

    public function slug(): string;

    public function view(): string;
}
