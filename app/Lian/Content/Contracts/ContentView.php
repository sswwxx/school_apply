<?php
/**
 * Created by PhpStorm.
 * User: lian
 * Date: 2019/11/22
 * Time: 10:47
 */

namespace App\Lian\Content\Contracts;


/**
 * 站点内容视图 接口
 *
 * Interface ContentView
 *
 * @package App\Lian\Web\Contracts
 */
interface ContentView
{
    /**
     * 获取驱动中定义的视图名称
     *
     * @return string
     */
    public function getViewName(): string;

    /**
     * 获取数据
     *
     * @return mixed
     */
    public function getViewData();
}
