<?php
/**
 * Created by PhpStorm.
 * User: lian
 * Date: 2019/12/2
 * Time: 10:01
 */

namespace App\Lian\Content\Contracts;


interface WidgetRender
{
    public function render(TemplateWidget $widget, $variables = []);
}
