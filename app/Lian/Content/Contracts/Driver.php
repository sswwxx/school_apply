<?php
/**
 * Created by PhpStorm.
 * User: lian
 * Date: 2019/10/17
 * Time: 13:52
 */

namespace App\Lian\Content\Contracts;


use App\Lian\Module\Contracts\Module;
use App\Models\Column;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;

interface Driver extends Module
{
    /**
     *
     * @param int $size 页面大小
     * @param array $columns 字段名称
     * @param string $pageName 页码字段名称
     * @param int $pageNo 当前页
     *
     * @return LengthAwarePaginator
     *
     */
    public function paginate($size = 20, $columns = ['*'], $pageName = 'page', $pageNo = null): LengthAwarePaginator;


    /**
     * @param $id
     *
     * @return Model
     */
    public function find($id);


    /**
     * @param Column $column
     */
    public function setColumn(Column $column): void;

}
