<?php
/**
 * Created by PhpStorm.
 * User: lian
 * Date: 2019/11/26
 * Time: 16:10
 */

namespace App\Lian\Content\Contracts;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;

interface SiteDataDispatcher
{
    public function lists($cid, $size = null, $columns = null, $pageName = null, $pageNo = null): LengthAwarePaginator;

    public function detail($cid, $id): Model;
}
