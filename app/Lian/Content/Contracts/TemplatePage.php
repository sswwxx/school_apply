<?php
/**
 * Created by PhpStorm.
 * User: lian
 * Date: 2019/11/26
 * Time: 17:31
 */

namespace App\Lian\Content\Contracts;


interface TemplatePage
{
    /**
     * @return TemplateWidget[]
     */
    public function widgets(): array;

    public function name(): string;

    public function slug(): string;
}
