<?php
/**
 * Created by PhpStorm.
 * User: lian
 * Date: 2019/11/26
 * Time: 17:25
 */

namespace App\Lian\Content\Contracts;


use App\Lian\Content\View;
use App\Models\Column;

interface TemplateTheme
{
    public function name(): string;

    public function desc(): string;

    public function slug(): string;

    public function saveWidgetData($data);

    /**
     * @return TemplateWidget[]
     */
    public function widgets(): array;

    public function getWidgetsData(): array;

    /**
     * @return TemplatePage[]
     */
    public function pages(): array;


    /**
     * @param string $name
     *
     * @return TemplatePage
     */
    public function getPage($name): TemplatePage;


    public function widget($slug): TemplateWidget;
}
