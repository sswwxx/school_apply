<?php
/**
 * Created by PhpStorm.
 * User: lian
 * Date: 2019/11/28
 * Time: 10:22
 */

namespace App\Lian\Content\Contracts;

/**
 * Interface SeoContract
 *
 * @package App\Lian\Web\Contracts
 *
 */
interface SeoContract
{
    /**
     * 获取SEO标题
     *
     * @return string
     */
    public function getTitle(): string;

    /**
     * 获取SEO描述
     *
     * @return string
     */
    public function getDescription(): string;

    /**
     * 获取SEO关键词
     *
     * @return string
     */
    public function getKeywords(): string;

    /**
     * 设置SEO标题
     *
     * @param $title
     */
    public function setTitle($title): void;

    /**
     * 设置SEO描述
     *
     * @param $description
     */
    public function setDescription($description): void;

    /**
     * 设置SEO关键词
     *
     * @param $keywords
     */
    public function setKeywords($keywords): void;

}
