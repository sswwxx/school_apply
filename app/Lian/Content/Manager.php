<?php
/**
 * Created by PhpStorm.
 * User: lian
 * Date: 2019/10/9
 * Time: 11:07
 */

namespace App\Lian\Content;


use App\Lian\Content\Contracts\Manager as Contract;
use App\Lian\Content\Contracts\Driver;
use App\Models\Column;

class Manager implements Contract
{
    protected static $drivers = [];

    /**
     * @param $driver
     *
     * @deprecated
     *
     * @throws
     */
    public static function extend($driver)
    {
        self::pushDriver(resolve($driver));
    }

    /**
     * @param $driver
     *
     * @throws
     */
    public static function register($driver)
    {
        self::pushDriver(resolve($driver));
    }

    protected static function pushDriver(Driver $driver)
    {
        self::$drivers[$driver->getSlug()] = $driver;
    }

    /**
     * @param Column $column
     *
     * @return Driver
     *
     * @throws
     */
    public function columnDriver(Column $column): Driver
    {
        $slug = $column->type;

        return tap($this->driver($slug), function (Driver $driver) use ($column) {

            $config = $column->metaValue($column->getDriverMetaKey()) ?: [];

            $driver->setColumn($column);
            $driver->setConfig($config);

        });
    }

    /**
     * @param $slug
     *
     * @return Driver
     *
     * @throws
     */
    public function driver($slug): Driver
    {
        if (isset(self::$drivers[$slug])) {
            return self::$drivers[$slug];
        }

        throw new \Exception("Column [{$slug}] Not Found.");
    }

    public function drivers(): array
    {
        return self::$drivers;
    }

}
