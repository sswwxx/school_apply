<?php
/**
 * Created by PhpStorm.
 * User: lian
 * Date: 2019/11/28
 * Time: 10:26
 */

namespace App\Lian\Content;

use App\Lian\Content\Contracts\SeoContract;

class Seo implements SeoContract
{
    protected $title;

    protected $description;

    protected $keywords;

    public function getTitle(): string
    {
        return $this->title ?: option('seo_title', '');
    }

    public function setTitle($title): void
    {
        $this->title = $title;
    }

    public function getDescription(): string
    {
        return $this->description ?: option('seo_description', '');
    }

    public function setDescription($description): void
    {
        $this->description = $description;
    }

    public function getKeywords(): string
    {
        return $this->keywords ?: option('seo_keywords', '');
    }

    public function setKeywords($keywords): void
    {
        $this->keywords = $keywords;
    }

}
