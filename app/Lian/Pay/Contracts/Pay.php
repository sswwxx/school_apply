<?php
/**
 * Created by PhpStorm.
 * User: lian
 * Date: 2020/1/2
 * Time: 17:35
 */

namespace App\Lian\Pay\Contracts;


use App\Models\Order;

interface Pay
{
    public function pay(Order $order);

    public function refund(Order $order);

    public function find(Order $order);
}
