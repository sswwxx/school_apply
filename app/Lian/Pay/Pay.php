<?php
/**
 * Created by PhpStorm.
 * User: lian
 * Date: 2020/1/2
 * Time: 17:34
 */

namespace App\Lian\Pay;


use Illuminate\Support\Manager;

class Pay extends Manager
{
    public function getDefaultDriver()
    {
        throw new \Exception('Driver not found.');
    }

    public function createAliPayDriver()
    {

    }

    public function createWechatPayDriver()
    {

    }

}
