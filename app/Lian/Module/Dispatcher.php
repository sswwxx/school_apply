<?php
/**
 * Created by PhpStorm.
 * User: lian
 * Date: 2019/12/25
 * Time: 14:58
 */

namespace App\Lian\Module;

use App\Lian\Content\Router;
use App\Lian\Content\View;
use App\Lian\Helper\OptionKey;
use App\Lian\Module\Contracts\Dispatcher as Contract;
use App\Models\Column;
use App\Models\Option;
use Illuminate\Routing\Router as SystemRouter;

class Dispatcher implements Contract
{
    public function handle($slug, $method, $params = [])
    {
        $module = modules_manager()->module($slug);

        if (!is_callable([$module, $method])) {
            abort(404);
        }

        $result = call_user_func([$module, $method], ...$params);

        if ($result instanceof View) {

            $column = $this->fakerColumn();
            $column->template = Option::get(OptionKey::moduleTemplate($module), []);

            return template()->render($column, $result);
        }

        return $result;
    }

    protected function fakerColumn()
    {
        return new Column();
    }

    public function registerRoutes(SystemRouter $router): void
    {
        if (!$this->tableReady()) return;

        $enabled = option('modules_enabled', []);

        $controller = config('cms.route.module.controller');

        $router = new Router($router, $controller);
        $modules = modules_manager()->modules();
        $column = $this->fakerColumn();

        foreach ($modules as $module) {

            $column->slug = $module->getSlug();

            if (!in_array($column->slug, $enabled)) continue;

            $router->setColumn($column);
            $module->route($router);
        }
    }

    protected function tableReady()
    {
        try {
            return \Schema::hasTable('columns');
        } catch (\Exception $exception) {
            return false;
        }
    }
}
