<?php
/**
 * Created by PhpStorm.
 * User: lian
 * Date: 2019/12/25
 * Time: 14:21
 */

namespace App\Lian\Module;

use App\Admin\Components\Form;
use App\Lian\Helper\OptionKey;
use App\Lian\Module\Contracts\Module as Contract;
use App\Models\Option;
use Illuminate\Console\Scheduling\Schedule;

abstract class Module implements Contract
{
    protected $name;

    protected $slug;

    protected $hasAdminConfig = true;

    protected $hasAdminPage = false;

    protected $config;

    public function getName(): string
    {
        return $this->name;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function hasAdminConfig(): bool
    {
        return $this->hasAdminConfig;
    }

    public function setConfig(array $config)
    {
        $this->config = $config;
    }

    public function hasAdminPage(): bool
    {
        return $this->hasAdminPage;
    }

    public function adminForm(): Form
    {
        throw new \Exception("Module [$this->name] has not admin form");
    }

    public function adminPage()
    {
        throw new \Exception("Module [$this->name] has not admin page");
    }

    public function config($key, $default = null)
    {
        if (empty($this->config)) {
            $this->config = Option::get(OptionKey::moduleConfig($this), []);
        }

        return data_get($this->config, $key, $default);
    }

    public function schedule(Schedule $schedule)
    {

    }


}
