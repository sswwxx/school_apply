<?php
/**
 * Created by PhpStorm.
 * User: lian
 * Date: 2019/12/25
 * Time: 14:19
 */

namespace App\Lian\Module;

use App\Lian\Module\Contracts\Manager as Contract;
use App\Lian\Module\Contracts\Module;

class Manager implements Contract
{
    protected static $modules = [];

    /**
     * @param $driver
     *
     * @deprecated
     *
     * @throws
     */
    public static function extend($driver)
    {
        self::pushDriver(resolve($driver));
    }

    /**
     * @param $driver
     */
    public static function register($driver)
    {
        self::pushDriver(resolve($driver));
    }

    protected static function pushDriver(Module $driver)
    {
        self::$modules[$driver->getSlug()] = $driver;
    }

    /**
     * @param $slug
     *
     * @return Module
     *
     * @throws
     */
    public function module($slug): Module
    {
        if (isset(self::$modules[$slug])) {
            return self::$modules[$slug];
        }

        throw new \Exception("Module [{$slug}] Not Found.");
    }

    public function modules(): array
    {
        return self::$modules;
    }

}
