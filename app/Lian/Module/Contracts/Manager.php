<?php
/**
 * Created by PhpStorm.
 * User: lian
 * Date: 2019/12/25
 * Time: 14:31
 */

namespace App\Lian\Module\Contracts;


interface Manager
{
    public function module($slug): Module;

    /**
     * @return Module[]
     */
    public function modules(): array;
}
