<?php
/**
 * Created by PhpStorm.
 * User: lian
 * Date: 2019/12/25
 * Time: 14:47
 */

namespace App\Lian\Module\Contracts;


use Illuminate\Routing\Router;

interface Dispatcher
{
    /**
     * @param string $slug
     * @param string $method
     * @param array $params
     * @return mixed
     */
    public function handle($slug, $method, $params = []);

    /**
     * 注册路由
     *
     * @param Router $router
     */
    public function registerRoutes(Router $router): void;
}
