<?php
/**
 * Created by PhpStorm.
 * User: lian
 * Date: 2019/12/25
 * Time: 14:20
 */

namespace App\Lian\Module\Contracts;


use App\Admin\Components\Form;
use App\Lian\Content\Router;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Contracts\Support\Renderable;

interface Module
{
    /**
     * @return array ['slug' => 'page_name']
     */
    public function templates(): array;

    /**
     * @param Router $router
     */
    public function route(Router $router): void;

    /**
     *
     * @return string
     */
    public function getName(): string;

    /**
     * @return string
     */
    public function getSlug(): string;

    /**
     * @param Form $form
     */
    public function configForm(Form $form);

    /**
     * @param array $config
     * @return mixed
     */
    public function setConfig(array $config);


    #===== Admin use

    /**
     * @return string|Renderable
     */
    public function adminPage();

    /**
     * @return Form
     */
    public function adminForm(): Form;

    /**
     * @return bool
     */
    public function hasAdminPage(): bool;

    /**
     * @return bool
     */
    public function hasAdminConfig(): bool;


    # config
    public function config($key,$default = null);

    # schedule
    public function schedule(Schedule $schedule);

}
