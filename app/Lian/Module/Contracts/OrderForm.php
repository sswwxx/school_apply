<?php


namespace App\Lian\Module\Contracts;


use App\Models\User\User;

interface OrderForm
{
    public function user(): User;

    public function tid();

    public function type();

    public function subject();

    public function totalFee();

    public function discountFee();


}