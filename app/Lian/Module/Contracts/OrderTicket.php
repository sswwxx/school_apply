<?php


namespace App\Lian\Module\Contracts;


interface OrderTicket extends OrderForm
{
    public function paid(): bool;

    public function refunded(): bool;

    public function payType(): string;

    public function payMethod(): string;

    public function createdAt();

    public function paidAt();

    public function refundedAt();

    public function paidFee();

    public function refundedFee();

    public function id();

    public function tradeNo();
}