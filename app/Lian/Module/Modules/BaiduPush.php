<?php


namespace App\Lian\Module\Modules;


use App\Admin\Components\Form;
use App\Jobs\BaiduRank;
use App\Jobs\PushBaiduCount;
use App\Lian\Content\Router;
use App\Lian\Module\Module;
use App\Models\Baidu\Keyword;
use Illuminate\Console\Scheduling\Schedule;
use App\QueryRules\Baidu;
use Illuminate\Support\Collection;
use QL\QueryList;

class BaiduPush extends Module
{
    protected $hasAdminConfig = true;

    protected $hasAdminPage = true;

    protected $slug = 'baiduPush';

    protected $name = '百度主动推送';

    public function templates(): array
    {
        return [];
    }

    public function route(Router $router): void
    {

    }

    public function configForm(Form $form)
    {
        $form->text('token', '百度主动推送准入秘钥');
        $form->tags('keywords', '关键字排行监听');
    }

    public function schedule(Schedule $schedule)
    {
        $schedule->job(new PushBaiduCount($this))->everyTenMinutes();
        $schedule->job(new BaiduRank($this))->dailyAt('4');
    }

}
