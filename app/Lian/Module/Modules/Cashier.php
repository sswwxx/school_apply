<?php
/**
 * Created by PhpStorm.
 * User: lian
 * Date: 2020/1/2
 * Time: 17:14
 */

namespace App\Lian\Module\Modules;


use App\Admin\Components\Form;
use App\Events\OrderPaid;
use App\Lian\Content\Router;
use App\Lian\Content\View;
use App\Lian\Module\Contracts\OrderForm;
use App\Lian\Module\Contracts\OrderTicket;
use App\Lian\Module\Module;
use App\Models\Order;
use Illuminate\Database\Eloquent\Model;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Yansongda\Pay\Pay;

class Cashier extends Module
{
    protected $slug = 'cashier';

    protected $name = '收银台';

    protected $hasAdminPage = true;

    protected $hasAdminConfig = true;

    public static function settle(OrderForm $form): OrderTicket
    {
        return modules_manager()->module('cashier')->createOrder($form);
    }

    public static function url(OrderTicket $ticket)
    {
        return module_url('cashier', 'pay', ['id' => $ticket->tradeNo()]);
    }

    public static function payUrl(OrderTicket $ticket, $redirect = null)
    {
        return module_url('cashier', 'pay', ['id' => $ticket->tradeNo(), 'redirect' => $redirect]);
    }

    /**
     * @param string $trade
     * @return OrderTicket
     */
    public static function ticket($trade)
    {
        return modules_manager()->module('cashier')->getTicket($trade);
    }

    public static function refund($tid, $type, $fee): OrderTicket
    {
        return modules_manager()->module('cashier')->orderRefund($tid, $type, $fee);
    }

    /**
     * 创建订单
     *
     * @param OrderForm $form
     * @return OrderTicket
     */
    public function createOrder(OrderForm $form): OrderTicket
    {
        $order = new Order();

        $order->tid = $form->tid();
        $order->user_id = $form->user()->id;
        $order->order_type = $form->type();
        $order->subject = $form->subject();
        $order->total_fee = $form->totalFee();
        $order->discount_fee = $form->discountFee();
        $order->status = Order::STATUS_CREATED;
        $order->trade_no = $this->createTradeNo();

        $order->save();

        return $order;
    }

    /**
     * 创建不重复的订单号
     *
     * @return string
     */
    protected function createUniqueTradeNo()
    {
        $tradeNo = $this->createTradeNo();

        if (Order::whereTradeNo($tradeNo)->exists()) {
            return $this->createTradeNo();
        }

        return $tradeNo;
    }

    /**
     * 创建订单号
     *
     * @return string
     */
    protected function createTradeNo()
    {
        return ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'][date('Y') % 10] .
            strtoupper(dechex(date('m'))) . date('d') .
            substr(time(), -5) . substr(microtime(), 2, 5) .
            sprintf('%02d', rand(0, 99));
    }

    protected function client($type)
    {
        $config = [];
        switch ($type) {
            case 'wechat':
                //微信支付
                $config = [
                    'app_id' => $this->config('wechat_pay_app_id'),
                    'mch_id' => $this->config('wechat_pay_mch_id'),
                    'key' => $this->config('wechat_pay_key'),
                    'cert_path' => $this->config('wechat_pay_cert_client'),
                    'key_path' => $this->config('wechat_pay_cert_key'),
                    'notify_url' => url('cashier/notify_url/wechat'),     // 你也可以在下单时单独设置来想覆盖它
                    'mode' => 'dev', // optional,设置此参数，将进入沙箱模式
                ];
                break;
            case 'alipay':
                //支付宝支付
                $config = [
                    'app_id' => $this->config('alipay_app_id'),
                    'notify_url' => url('cashier/notify_url/alipay'),
                    'return_url' => request()->session()->get('redirect'),
                    'ali_public_key' => $this->config('alipay_ali_public_key'),
                    'private_key' => $this->config('alipay_private_key'),
                    'mode' => 'dev', // optional,设置此参数，将进入沙箱模式
                ];
                break;
        }
        return $config;
    }

    /**
     * 获取订单
     *
     * @param string $trade
     * @return mixed|OrderTicket
     */
    public function getTicket($trade)
    {
        return Order::where(['trade_no' => $trade])->first();
    }

    /**
     * 支付订单
     *
     * @param string $no
     *
     * @return mixed
     */
    public function pay($no)
    {
        request()->session()->put('redirect', request()->get('redirect'));
        return new View('pay', $no);
    }

    public function buy($type, $no)
    {
        $order = Order::whereTradeNo($no)->firstOrFail();
        //实际支付金额通过计算获得
        $order->pay_fee = bcsub($order->total_fee, $order->discount_fee, 2);

        $qrcode = '';
        switch ($type) {
            case 'wechat':
                /* https://pay.yanda.net.cn/docs/2.x/wechat/pay 接口文档*/
                $wechatPay = Pay::wechat($this->client($type))->scan([
                    'body' => $order->subject,
                    'out_trade_no' => $order->trade_no,
                    'total_fee' => bcmul(100, $order->pay_fee, 0), //微信支付以分为单位
                ]);

                $qrcode = $wechatPay->code_url;
                break;
            case 'alipay':
                //支付宝支付
                $aliPay = Pay::alipay($this->client($type))->scan([
                    'out_trade_no' => $order->trade_no,
                    'total_amount' => $order->pay_fee, //支付宝支付单位为元
                    'subject' => $order->subject,
                ]);
                $qrcode = $aliPay->qr_code;
                break;
        }
        $data = [
            'qrcode' => QrCode::size(500)->generate($qrcode),
            'no' => $no
        ];
        return new View('buy', $data);
    }

    public function status($no)
    {
        $order = Order::whereTradeNo($no)->firstOrFail();
        if ($order && $order->status === Order::STATUS_PAID) {
            return response()->json(['status' => true]);
        }
    }

    public function notify_url($type)
    {
        //支付回调
        switch ($type) {

            case 'wechat':
                $payClient = Pay::wechat($this->client($type));
                $wechat = $payClient->verify();
                $order = Order::whereTradeNo($wechat->out_trade_no)->firstOrFail();
                if (!$order || $order->status == Order::STATUS_PAID) { // 如果订单不存在 或者 订单已经支付过了
//                    return $payClient->success();
                }
                if ($wechat->return_code === 'SUCCESS') { // return_code 表示通信状态，不代表支付状态
                    $order->paid_at = date('Y-m-d H:i:s', time()); // 更新支付时间为当前时间
                    $order->status = Order::STATUS_PAID;
                    $this->triggerPaidEvent($order);
                }
                return $payClient->success();
                break;

            case 'alipay':
                $payClient = Pay::wechat($this->client($type));
                $alipay = $payClient->verify();
                $order = Order::whereTradeNo($alipay->out_trade_no)->firstOrFail();
                if (!$order || $order->status === Order::STATUS_PAID) { // 如果订单不存在 或者 订单已经支付过了
//                    return $payClient->success();
                }
                if ($alipay->trade_status === 'TRADE_SUCCESS') { // return_code 表示通信状态，不代表支付状态
                    $order->paid_at = date('Y-m-d H:i:s', time()); // 更新支付时间为当前时间
                    $order->status = Order::STATUS_PAID;
                    $this->triggerPaidEvent($order);
                }
                return $payClient->success();
                break;
        }

    }

    public function orderRefund($tid, $type, $fee)
    {

    }


    public function templates(): array
    {
        return [
            'pay' => '支付页面',
            'buy' => '付款页面',
        ];
    }

    public function route(Router $router): void
    {
        $router->get('/pay/{id}', 'pay');
        $router->get('/buy/{type}/{id}', 'buy');
        $router->get('/status/{id}', 'status');

        $router->post('/notify_url/{type}', "notify_url");
    }

    public function configForm(Form $form)
    {
        $form->tab('微信支付', function (Form $form) {

            $form->text('wechat_pay_appid', 'APP APPID');
            $form->text('wechat_pay_app_id', '公众号 APPID');
            $form->text('wechat_pay_miniapp_id', '小程序 APPID');
            $form->text('wechat_pay_mch_id', '商户 ID');
            $form->text('wechat_pay_key', '商户 KEY');
            $form->textarea('wechat_pay_cert_client', '商户证书');
            $form->textarea('wechat_pay_cert_key', '商户证书KEY');

        });

        $form->tab('支付宝支付', function (Form $form) {

            $form->text('alipay_app_id', 'APPID');
            $form->textarea('alipay_ali_public_key', '支付宝公钥');
            $form->textarea('alipay_private_key', '支付宝私钥');

        });
    }

    protected function triggerPaidEvent(OrderTicket $ticket)
    {
        event(new OrderPaid($ticket));
    }

}
