<?php
/**
 * Created by PhpStorm.
 * User: lian
 * Date: 2019/12/25
 * Time: 15:41
 */

namespace App\Lian\Module\Modules;


use App\Admin\Components\Form;
use App\Admin\Components\Grid;
use App\Lian\Content\Router;
use App\Lian\Module\Module;
use App\Models\User\User;
use App\Models\User\Wechat;
use EasyWeChat\Factory;
use EasyWeChat\OfficialAccount\Application;

class WechatAuth extends Module
{
    protected $name = '微信H5登录';

    protected $slug = 'wechat_auth';

    /** @var Application */
    protected $app;

    protected $hasAdminPage = true;

    public function adminPage()
    {
        $grid = new Grid(new Wechat);

        $grid->column('avatar', '头像')->image(null, 50, 50);

        $grid->column('nickname', '昵称');
        $grid->column('country', '国家');
        $grid->column('province', '省份');
        $grid->column('city', '城市');

        $grid->column('created_at', '注册时间')->date('Y-m-d');

        $grid->disableCreateButton();
        $grid->disableRowSelector();
        $grid->disableActions();

        $grid->model()->orderByDesc('id');

        return $grid;
    }

    public function templates(): array
    {
        return [
            'login' => '登录页面',
            'registered' => '注册页面',
        ];
    }

    public function route(Router $router): void
    {
        $router->get('/login', "login");
        $router->get('/login/result', "loginResult");
    }

    public function login()
    {
        $redirect = url('/');

        session('auth_redirect', $redirect);

        if (auth()->check()) return redirect($redirect);

        return $this->app()->oauth->scopes(['snsapi_userinfo'])
            ->redirect(module_url($this->slug, 'loginResult'));
    }

    protected function app()
    {
        if ($this->app) return $this->app;

        $config = [
            'app_id' => $this->config('wx_app_id'),
            'secret' => $this->config('wx_app_secret'),

            'response_type' => 'object',
        ];

        if (empty($config['app_id']) || empty($config['secret'])) {
            throw new \Exception('APPID与APP Secret 不能为空');
        }

        return $this->app = Factory::officialAccount($config);
    }

    public function loginResult()
    {
        $user = $this->app()->oauth->user();

        $wechatUser = Wechat::whereOpenid($user->getAttribute('id'))->first();

        if (is_null($wechatUser)) {

            $wechatUser = new Wechat();

            $wechatUser->openid = $user->getAttribute('id');
            $wechatUser->user_id = 0;
        }

        $wechatUser->name = $user->getAttribute('name', '');
        $wechatUser->nickname = $user->getAttribute('nickname', '');
        $wechatUser->avatar = $user->getAttribute('avatar', '');
        $origin = $user->getOriginal();

        $wechatUser->sex = data_get($origin, 'sex', 0);
        $wechatUser->country = data_get($origin, 'country', '');
        $wechatUser->province = data_get($origin, 'province', '');
        $wechatUser->city = data_get($origin, 'city', '');

        $wechatUser->save();

        if (empty($user = $wechatUser->user)) {

            $user = new User();
            $user->name = $wechatUser->nickname;
            $user->avatar = $wechatUser->avatar;
            $user->is_admin = 0;
            $user->username = 'WechatUser_' . $wechatUser->id;
            $user->password = '';

            $user->save();

            $wechatUser->user_id = $user->id;

            $wechatUser->save();
        }

        auth()->login($user);

        $url = $this->config('redirect_url', url('/'));

        return redirect(session('auth_redirect', $url));
    }

    public function configForm(Form $form)
    {
        $form->tab('微信公众号登录', function (Form $form) {

            $form->display('route', '地址')->default(module_url('wechat_auth', 'login'));

            $form->text('wx_app_id', '公众号ID')->required();
            $form->text('wx_app_secret', '公众号秘钥')->required();

            $form->url('redirect_url', '登录后跳转');
        });
    }
}
