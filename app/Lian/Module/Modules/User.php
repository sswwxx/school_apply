<?php


namespace App\Lian\Module\Modules;


use App\Admin\Components\Form;
use App\Lian\Content\Router;
use App\Lian\Content\View;
use App\Lian\Module\Module;
use Illuminate\Support\ViewErrorBag;

class User extends Module
{
    protected $hasAdminPage = false;

    protected $slug = 'user';

    protected $name = '用户模块';

    public function templates(): array
    {
        return [
            'login' => '登录页'
        ];
    }

    public function route(Router $router): void
    {
        $router->get('login', 'loginPage');
        $router->post('login', 'doLogin');

    }

    public function loginPage()
    {
        $this->rememberRedirect(request('redirect'));

        $errors = null;

        if (session()->has('errors')) {

            /** @var ViewErrorBag $errors */
            $errors = session()->get('errors');
            $errors = $errors->getMessages();

            $errors = collect($errors)->map(function ($error) {
                return $error[0];
            })->implode('');

        }

        if (auth()->check()) return $this->redirect();

        return new View('login', [
            'login_url' => module_url($this->slug, 'doLogin'),
            'errors' => $errors
        ]);
    }

    public function doLogin()
    {
        $username = request('username');
        $password = request('password');

        if (auth()->attempt(['username' => $username, 'password' => $password], true) ||
            auth()->attempt(['mobile' => $username, 'password' => $password], true)) {

            return $this->redirect();
        }

        return back()->withErrors('用户名或密码错误');
    }

    protected function redirect()
    {
        $url = $this->rememberRedirect();

        if ($url && $this->config('with_token', false)) {

            $token = auth()->user()->getRememberToken();

            $url = strpos($url, '?') === false ? $url . '?token=' . $token : $url . '&token=' . $token;
        }

        return redirect($url ?: url('/'));
    }

    protected function rememberRedirect($url = null)
    {
        if ($url) session()->flash('LoginRememberRedirect', $url);

        return session('LoginRememberRedirect');
    }

    public function configForm(Form $form)
    {
        $form->switch('with_token', '重定向时附带token')->default(0);
    }

}