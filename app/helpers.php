<?php
/**
 * Created by PhpStorm.
 * User: lian
 * Date: 2019/10/8
 * Time: 18:04
 */

use App\Lian\Content\Contracts;
use App\Lian\Content\Template\Template;
use App\Lian\Module\Contracts\Manager;
use App\Models\User\User;
use Illuminate\Contracts\Auth\Authenticatable;

if (!function_exists('user_id')) {
    /**
     * 获取当前登录用户的ID
     *
     * @return int
     */
    function user_id()
    {
        return ($user = user()) ? $user->id : 0;
    }
}

if (!function_exists('user')) {

    /**
     * 获取当前登录的用户
     *
     * @return User|Authenticatable|null
     */
    function user()
    {
        return \Encore\Admin\Facades\Admin::user();
    }
}

if (!function_exists('site_config')) {

    /**
     * 获取系统设置
     *
     * @param $key
     * @param null $default
     * @return mixed
     *
     * @deprecated
     */
    function site_config($key, $default = null)
    {
        return option($key, $default);
    }

}

if (!function_exists('storage_url')) {

    /**
     * 获取文件URL
     *
     * @param $path
     * @return string
     */
    function site_storage($path)
    {
        return app(Contracts\SiteURLHelper::class)->storageUrl($path);
    }

}


if (!function_exists('site_nav')) {

    /**
     * 获取菜单
     *
     * @return array
     */
    function site_menus()
    {
        return app(Contracts\Dispatcher::class)->menus();
    }

}

if (!function_exists('site_breadcrumbs')) {

    /**
     * 获取面包屑导航
     *
     * @return array
     */
    function site_breadcrumbs()
    {
        return app(Contracts\Dispatcher::class)->breadcrumbs();
    }

}


if (!function_exists('site_url')) {

    /**
     * 获取完整的URL地址
     *
     * @param string $uri
     * @return \Illuminate\Contracts\Routing\UrlGenerator|string
     */
    function site_url($uri = '')
    {
        return app(Contracts\SiteURLHelper::class)->url($uri);
    }

}


if (!function_exists('site_lists')) {

    /**
     * 获取列表数据
     *
     * @param int $id 栏目|模块 ID
     * @param int $size 每页数据条数
     * @param array $columns 需要获取的字段
     * @param string $pageName 当前页码
     * @param int $pageNo 分页字段名称
     *
     * @return array
     */
    function site_lists($id, $size = 20, $columns = ['*'], $pageName = 'page', $pageNo = 1)
    {
        return app(Contracts\SiteDataDispatcher::class)->lists($id, $size, $columns, $pageName, $pageNo);
    }
}

if (!function_exists('site_detail')) {

    /**
     * 获取栏目内容详细数据
     *
     * @param int $cid 栏目|模型 ID
     * @param int $id 内容ID
     * @return array
     */
    function site_detail($cid, $id)
    {
        return app(Contracts\SiteDataDispatcher::class)->detail($cid, $id);
    }
}

if (!function_exists('site_column_url')) {

    /**
     * 获取栏目的URL
     *
     * @param \App\Models\Column |int $column 栏目|模型 ID
     *
     * @param int $id
     *
     * @return string
     *
     * @throws
     */
    function site_column_url($column, $id = null)
    {
        return app(Contracts\SiteURLHelper::class)->columnUrl($column, $id);
    }

}


if (!function_exists('site_column')) {

    /**
     * 获取栏目信息
     *
     * @param int $cid 栏目ID
     * @return \App\Models\Column
     */
    function site_column($cid)
    {
        return app(Contracts\Dispatcher::class)->column($cid);
    }

}

if (!function_exists('site_pager')) {

    /**
     * @param \Illuminate\Pagination\LengthAwarePaginator $page
     * @param array $params
     *
     * @return string
     *
     * @throws
     */
    function site_pager(\Illuminate\Pagination\LengthAwarePaginator $page, $params = [])
    {
        return app(Contracts\Dispatcher::class)->renderPager($page, $params);
    }

}

if (!function_exists('option')) {

    /**
     * @param $key
     * @param null $default
     * @return mixed
     */
    function option($key, $default = null)
    {
        return app(Contracts\Dispatcher::class)->option($key, $default);
    }

}


if (!function_exists('drivers_manager')) {

    /**
     * 返回栏目调度者
     *
     * @return \App\Lian\Content\Manager
     */
    function drivers_manager()
    {
        return resolve(Contracts\Manager::class);
    }

}


if (!function_exists('template')) {

    /**
     * 返回模板
     *
     * @return  Template
     */
    function template()
    {
        return resolve(Contracts\TemplateContract::class);
    }
}


if (!function_exists('seo')) {

    /**
     * 返回SEO
     *
     * @return Contracts\SeoContract
     */
    function seo()
    {
        return resolve(Contracts\SeoContract::class);
    }

}


if (!function_exists('site_content')) {

    /**
     * @return Contracts\Dispatcher
     */
    function site_content()
    {
        return resolve(Contracts\Dispatcher::class);
    }

}

if (!function_exists('modules_manager')) {

    /**
     * 返回模块管理
     *
     * @return Manager
     */
    function modules_manager()
    {
        return resolve(Manager::class);
    }

}


if (!function_exists('module_url')) {

    function module_url($slug, $action,$params = [])
    {
        return route($slug . '.' . $action,$params);
    }

}
