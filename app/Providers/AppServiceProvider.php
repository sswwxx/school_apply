<?php

namespace App\Providers;

use App\Lian\Content\Drivers\EditAblePageDriver;
use App\Lian\Content\Drivers\LinkDriver;
use App\Lian\Content\Drivers\PageDriver;
use App\Lian\Content\Drivers\PostsDriver;
use App\Lian\Content\Drivers\ProductsDriver;
use App\Lian\Content\Drivers\StaticPageDriver;
use App\Lian\Content\Contracts\Manager as DriverDispatcherContract;
use App\Lian\Content\Contracts\SeoContract;
use App\Lian\Content\Contracts\Dispatcher;
use App\Lian\Content\Contracts\SiteDataDispatcher;
use App\Lian\Content\Contracts\SiteURLHelper;
use App\Lian\Content\Contracts\TemplateContract;
use App\Lian\Content\Manager as DriverDispatcher;
use App\Lian\Content\Seo;
use App\Lian\Content\Site\Content;
use App\Lian\Content\Site\DataDispatcher;
use App\Lian\Content\Site\URLHelper;
use App\Lian\Content\Template\Template;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap any application services.
     *
     * @throws
     *
     * @return void
     */
    public function boot()
    {

    }
}
