<?php
/**
 * Created by PhpStorm.
 * User: lian
 * Date: 2019/12/25
 * Time: 14:53
 */

namespace App\Providers;

use App\Lian\Content\Drivers\EditAblePageDriver;
use App\Lian\Content\Drivers\FormDriver;
use App\Lian\Content\Drivers\LinkDriver;
use App\Lian\Content\Drivers\PageDriver;
use App\Lian\Content\Drivers\PostsDriver;
use App\Lian\Content\Drivers\ProductsDriver;
use App\Lian\Content\Drivers\StaticPageDriver;
use App\Lian\Content\Contracts\Dispatcher;
use App\Lian\Content\Contracts\Manager as DriverManagerContract;
use App\Lian\Content\Contracts\SeoContract;
use App\Lian\Content\Contracts\SiteDataDispatcher;
use App\Lian\Content\Contracts\SiteURLHelper;
use App\Lian\Content\Contracts\TemplateContract;
use App\Lian\Content\Manager as DriverManager;
use App\Lian\Content\Seo;
use App\Lian\Content\Site\Content;
use App\Lian\Content\Site\DataDispatcher;
use App\Lian\Content\Site\URLHelper;
use App\Lian\Content\Template\Template;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class ContentServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton(TemplateContract::class, Template::class);

        $this->app->singleton(DriverManagerContract::class, DriverManager::class);

        $this->app->singleton(Dispatcher::class, Content::class);

        $this->app->singleton(SiteDataDispatcher::class, DataDispatcher::class);

        $this->app->singleton(SiteURLHelper::class, URLHelper::class);

        $this->app->singleton(SeoContract::class, Seo::class);
    }

    public function boot()
    {
        DriverManager::register(PostsDriver::class);
        DriverManager::register(PageDriver::class);
        DriverManager::register(ProductsDriver::class);
        DriverManager::register(StaticPageDriver::class);
        DriverManager::register(LinkDriver::class);
        DriverManager::register(EditAblePageDriver::class);
        DriverManager::register(FormDriver::class);

        $this->bladeDirective();
    }

    protected function bladeDirective()
    {
        Blade::directive('widget', function ($expression) {
            return "<?php echo site_content()->widget($expression); ?>";
        });
    }


}
