<?php
/**
 * Created by PhpStorm.
 * User: lian
 * Date: 2019/12/25
 * Time: 14:55
 */

namespace App\Providers;


use App\Lian\Module;
use Illuminate\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton(Module\Contracts\Dispatcher::class, Module\Dispatcher::class);

        $this->app->singleton(Module\Contracts\Manager::class, Module\Manager::class);
    }

    public function boot()
    {
        Module\Manager::register(Module\Modules\WechatAuth::class);
        Module\Manager::register(Module\Modules\BaiduPush::class);
        Module\Manager::register(Module\Modules\User::class);
        Module\Manager::register(Module\Modules\Cashier::class);
    }
}
