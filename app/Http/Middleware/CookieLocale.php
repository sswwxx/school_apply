<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Support\Responsable;

class CookieLocale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!($locale = cookie('locale')->getValue())) {
            if ($locale) dd($locale);
            $locale = config('app.locale');
        }

        app()->setLocale($locale);

        $next = $next($request);

        if ($next instanceof Responsable) {
            $next->cookie('locale', $locale);
        }

        return $next;
    }
}
