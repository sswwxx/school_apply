<?php

namespace App\Http\Middleware;

use App\Models\Visit;
use Closure;
use Illuminate\Http\Request;
use itbdw\Ip\IpLocation;
use Jenssegers\Agent\Agent;

class UserVisitHistory
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $this->addHistory($request);

        return $next($request);
    }

    protected function addHistory(Request $request)
    {
        $path = trim($request->path(), '/');

        if ($this->shouldSkip($path)) return;

        $visit = new Visit();

        $visit->path = $path;


        $visit->ip = $request->ip();

        $info = IpLocation::getLocation($visit->ip);

        $visit->country = data_get($info, 'country', '局域网');
        $visit->province = data_get($info, 'province', '');
        $visit->city = data_get($info, 'city', '');
        $visit->isp = data_get($info, 'isp', '');
        $visit->area = data_get($info, 'area', '');

        $agent = new Agent();

        $visit->agent = $agent->getUserAgent() ?: '';
        $visit->browser = $agent->browser();
        $visit->version = $agent->version($visit->browser);
        $visit->device = $agent->device();
        $visit->desktop = $agent->isDesktop();

        $visit->created_at = date('Y-m-d H:i:s');

        $visit->user_id = auth()->check() ? auth()->user()->getAuthIdentifier() : 0;

        $visit->save();
    }

    protected function shouldSkip($path)
    {
        return strpos($path, config('admin.route.prefix')) === 0;
    }

}
