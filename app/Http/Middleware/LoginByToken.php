<?php

namespace App\Http\Middleware;

use App\Models\User\User;
use Closure;

class LoginByToken
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @param bool $redirect
     *
     * @return mixed
     */
    public function handle($request, Closure $next, $redirect = false)
    {
        if (!auth()->check() && !$this->login($request) && $redirect) {

            return redirect(module_url('user','loginPage'));
        }

        return $next($request);
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return bool
     */
    protected function login($request)
    {
        $token = $request->input('token');

        $user = User::whereRememberToken($token)->first();

        if (empty($user)) return false;

        auth()->login($user);

        return true;
    }

}
