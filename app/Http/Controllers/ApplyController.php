<?php


namespace App\Http\Controllers;


use App\Admin\Components\Form;
use App\Models\Apply;
use App\Models\Role;
use App\Models\User\User;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Form\EmbeddedForm;
use Encore\Admin\Form\Field;
use Illuminate\Database\Eloquent\Builder;

class ApplyController extends Controller
{
    protected $step = 'base';

    protected $steps = [
        'base' => '基础信息',
        'test' => '报考信息',
        'school' => '原学历信息',
        'images' => '资料上传',
    ];

    public function __construct()
    {
        Admin::disablePjax();
        Form::extend('simpleFile', Form\Field\SimpleFile::class);
    }

    public function index()
    {
        if (empty($this->userId())) {
            return redirect(url('wechat_auth/login '));
        }

        $this->step = request('step', 'base');

        if ($this->step == 'success') {
            return view('apply.success', [
                'title' => '完成',
            ]);
        }

        if (empty($this->steps[$this->step])) abort(404);

        $fields = $this->form()->builder()->fields();
        $data = $this->history();

        foreach ($fields as $field) {
            /** @var Field $field */
            $field->fill($data);
        }

        return view('apply.apply', array_merge([
            'fields' => $fields,
            'title' => $this->steps[$this->step],
        ], $this->nextPrev()));
    }

    protected function history()
    {
        $apply = Apply::whereUserId($this->userId())->first();

        return $apply ? $apply->toArray() : [];
    }

    protected function nextPrev()
    {
        $keys = array_keys($this->steps);

        $index = array_search($this->step, $keys);

        return [
            'next' => data_get($keys, $index + 1),
            'prev' => data_get($keys, $index - 1),
        ];
    }

    protected function form()
    {
        $form = new Form(new Apply);

        $form->setAction(url()->current());


        if ($this->step == 'base') {


            if ($tid = request('tid')) {

                $teacher = User::whereHas('roles', function (Builder $query) {
                    $query->where('slug', Role::ZS_TEACHER);
                })->where('id', $tid)->first();

                if (empty($teacher)) abort(404);

                $form->display('teacher', '招生老师')->default($teacher->name);

                $form->hidden('teacher_id')->default($teacher->id);

            } else {

                $options = User::whereHas('roles', function (Builder $query) {
                    $query->where('slug', Role::ZS_TEACHER);
                })->get()->pluck('name', 'id');

                $form->select('teacher_id', '招生老师')->options($options)->required();
            }


            $form->text('name', '姓名')->required();
            $form->select('gender', '性别')
                ->options([
                    'male' => '男',
                    'female' => '女',
                ])
                ->rules('required')->required();

            $form->text('nation', '民族')->rules('required')->required();

            $form->text('hometown', '籍贯')->rules('required')->required();

            $form->text('edu_level', '文化程度')->rules('required')->required();

            $options = [
                '群众',
                '团员',
                '党员',
                '预备党员',
                '其他',
            ];
            $form->select('political_status', '政治面貌')
                ->rules('required')->required()
                ->options(array_combine($options, $options));


            $form->text('id_number', '身份证号')->rules('required')->required();

            $form->date('birthday', '生日')
                ->rules('required')->required()
                ->style('width', '100%');

            $form->text('address', '户籍所在地址')->rules('required')->required();

            $form->select('hk_type', '户口性质')->options([
                '城镇' => '城镇',
                '农村' => '农村',
            ])->rules('required')->required();

            $form->text('company', '工作单位')->rules('required')->required();

            $form->date('company_date', '入职时间')
                ->rules('required')
                ->style('width', '100%')
                ->required();

            $form->text('mobile', '联系电话')->rules('required')->required();

            $form->text('email', '邮箱')->rules('required')->required();

            $form->text('em_name', '紧急联系人')->rules('required')->required();

            $form->text('em_mobile', '紧急联系人电话')->rules('required')->required();

        } else if ($this->step == 'test') {


            $form->select('apply_level', '报考层次')->options([
                '高起专' => '高起专',
                '专升本' => '专升本',
            ])->rules('required')->required();

            $form->text('apply_school', '预报名院校')->rules('required')->required();

            $form->text('apply_ind', '预报考专业')->rules('required')->required();

            $form->select('apply_type', '报考类别')->options([
                '统考' => '统考',
                '校企' => '校企',
                '全日制' => '全日制',
            ])->rules('required')->required();

        } else if ($this->step == 'school') {

            $form->text('org_school', '原毕业院校');

            $form->text('org_number', '原毕业证书编号');

        } else if ($this->step == 'images') {

            $form->embeds('ex', '', function (EmbeddedForm $form) {
                $form->simpleFile('blue_id_img', '本人2寸蓝底证件照')->attribute('accept','image/*');;
                $form->simpleFile('id_img_zm', '身份证正面')->attribute('accept','image/*');;
                $form->simpleFile('id_img_fm', '身份证反面')->attribute('accept','image/*');;
                $form->simpleFile('id_img_sc', '手持身份证照片')->attribute('accept','image/*');;
                $form->simpleFile('certificate', '缴费凭证截图')->attribute('accept','image/*');;
                $form->simpleFile('xl_photo', '学历照片')->attribute('accept','image/*');;
                $form->simpleFile('reg_img', '电子注册备案表')->attribute('accept','image/*');;
            });
        }

        $form->hidden('step')->default($this->step);
        $form->ignore('step');

        $form->saving(function (Form $form) {
            $form->model()->setAttribute('user_id', $this->userId());

            $next = data_get($this->nextPrev(), 'next');

            $status = $next ? Role::STUDENT : Role::FINANCE;

            $form->model()->setAttribute('status', $status);
        });

        $form->saved(function () {

            $next = data_get($this->nextPrev(), 'next') ?: 'success';

            return redirect(url()->current() . '?step=' . $next);

        });

        return $form;
    }


    public function store()
    {
        if (empty($this->userId())) abort(404);

        $this->step = request('step', 'base');

        if ($id = data_get($this->history(), 'id')) {
            $res = $this->form()->update($id);
        } else {
            $res = $this->form()->store();
        }

        if (session()->has('errors')) {
            dd(session('errors')->all());
        }

        return $res;
    }


    protected function userId()
    {
        return auth()->id();
    }
}