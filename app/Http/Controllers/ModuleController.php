<?php
/**
 * Created by PhpStorm.
 * User: lian
 * Date: 2019/10/9
 * Time: 18:19
 */

namespace App\Http\Controllers;

use App\Lian\Module\Contracts\Dispatcher;

class ModuleController extends Controller
{
    public function __call($method, $parameters)
    {
        $slug = request()->segment(1);

        $content = resolve(Dispatcher::class);

        return $content->handle($slug, $method, $parameters);
    }
}
