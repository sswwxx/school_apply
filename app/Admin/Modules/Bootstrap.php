<?php
/**
 * Created by PhpStorm.
 * User: lian
 * Date: 2019/11/26
 * Time: 15:03
 */

namespace App\Admin\Modules;


class Bootstrap
{
    protected $config = [
        'admin_name' => 'admin.name',
        'admin_logo' => 'admin.logo',
        'admin_logo_mini' => 'admin.logo-mini',
    ];

    public static function boot()
    {
        (new  static())->handle();
    }

    public function handle()
    {
        $this->migrateConfig('admin_name', 'admin.name');
        $this->migrateConfig('admin_name', 'admin.title');
        $this->migrateConfig('admin_login_bg', 'admin.login_background_image');

        $this->migrateConfig('admin_logo', 'admin.logo', function ($img) {
            return "<img height='40' src='{$img}'>";
        });

        $this->migrateConfig('admin_logo_mini', 'admin.logo-mini', function ($img) {
            return "<img height='40' src='{$img}'>";
        });

        $this->migrateConfig('admin_layout', 'admin.layout');
        $this->migrateConfig('admin_skin', 'admin.skin');
    }



    protected function migrateConfig($optionName, $confName, \Closure $callback = null)
    {
        $value = option($optionName);

        if (!is_null($value) && $callback) $value = $callback($value);

        if (is_null($value)) $value = config($confName);

        config([$confName => $value]);
    }

}
