<?php

/**
 * Laravel-admin - admin builder based on Laravel.
 * @author z-song <https://github.com/z-song>
 *
 * Bootstraper for Admin.
 *
 * Here you can remove builtin form field:
 * Encore\Admin\Form::forget(['map', 'editor']);
 *
 * Or extend custom form field:
 * Encore\Admin\Form::extend('php', PHPEditor::class);
 *
 * Or require js and css assets:
 * Admin::css('/packages/prettydocs/css/styles.css');
 * Admin::js('/packages/prettydocs/js/main.js');
 *
 */

use App\Admin\Components\Form\Field;

Encore\Admin\Form::forget(['map', 'editor']);
Encore\Admin\Form::extend('image', Field\Image::class);
Encore\Admin\Form::extend('editor', Field\UEditor::class);
Encore\Admin\Form::extend('media', Field\Media::class);
Encore\Admin\Form::extend('widget', Field\Widget::class);
Encore\Admin\Form::extend('blank', Field\Blank::class);

\View::prependNamespace('admin', resource_path('views/admin'));

view()->replaceNamespace('laravel-admin-media', resource_path('views/media'));

Encore\Admin\Facades\Admin::css('vendor/admin.css?v=1.1');

\App\Admin\Modules\Bootstrap::boot();

$script = <<<EOT
$(document).on('keyup', '.select2-selection--multiple .select2-search__field', function(event){
	if(event.keyCode == 13){
		var that = $(this);
		var optionText = that.val();
		//如果没有就添加
		if(optionText != "" && that.find("option[value='" + optionText + "']").length === 0){
			//我还不知道怎么优雅的定位到input对应的select
			var select = that.parents('.select2-container').prev("select");
			var newOption = new Option(optionText, optionText, true, true);
			select.append(newOption).trigger('change');
			that.val('');
		}
	}
});
EOT;

\Encore\Admin\Facades\Admin::script($script);
