<?php
/**
 * Created by PhpStorm.
 * User: lian
 * Date: 2019/11/20
 * Time: 16:42
 */

namespace App\Admin\ViewModel;

use App\Models\Column as Model;
use Encore\Admin\Traits\ModelTree;
use Encore\Admin\Tree;
use Illuminate\Pagination\LengthAwarePaginator;

class Column extends Model
{
    use ModelTree;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->setTitleColumn('name');
    }

    public static function tree()
    {
        return new Tree(new static());
    }

    public static function paginate($perPage = null, $columns = [], $pageName = 'page', $page = null)
    {
        $options = (new static())->selectOptions();

        unset($options[0]);

        $options = collect($options)->map(function ($name, $id) {
            $column = Column::find($id);

            $column->name = $name;

            return $column;

        });

        return new LengthAwarePaginator($options, count($options), 1);
    }

    /**
     * Get options for Select field in form.
     *
     * @param \Closure|null $closure
     * @param string $rootText
     *
     * @return array
     */
    public static function selectOptions(\Closure $closure = null, $rootText = '')
    {
        $options = (new static())->withQuery($closure)->buildSelectOptions([], 0, ' ');

        return $rootText ? collect($options)->prepend($rootText, 0)->all() : $options;
    }

    /**
     * Build options of select field in form.
     *
     * @param array $nodes
     * @param int $parentId
     * @param string $prefix
     * @param string $space
     *
     * @return array
     */
    protected function buildSelectOptions(array $nodes = [], $parentId = 0, $prefix = '', $space = '&nbsp;')
    {
        $prefix = trim($prefix ?: '┝' . $space);

        $options = [];

        if (empty($nodes)) {
            $nodes = $this->allNodes();
        }

        foreach ($nodes as $index => $node) {
            if ($node[$this->parentColumn] == $parentId) {
                $node[$this->titleColumn] = $prefix . $space . $node[$this->titleColumn];

                $childrenPrefix = str_replace('┝', str_repeat($space, 6), $prefix) . '┝' . str_replace(['┝', $space], '', $prefix);

                $children = $this->buildSelectOptions($nodes, $node[$this->getKeyName()], $childrenPrefix);

                $options[$node[$this->getKeyName()]] = $node[$this->titleColumn];

                if ($children) {
                    $options += $children;
                }
            }
        }

        return $options;
    }

    public static function with($relations)
    {
        return new static;
    }

    public function toArray()
    {
        $arr = parent::toArray();

        $arr['fields'] = $this->fields->meta_value;

        return $arr;
    }
}
