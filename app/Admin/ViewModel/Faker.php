<?php
/**
 * Created by PhpStorm.
 * User: lian
 * Date: 2019/12/5
 * Time: 15:38
 */

namespace App\Admin\ViewModel;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;

class Faker extends Model
{
    protected static $items = [];

    protected $table = ' ';

    public static function item($item)
    {
        self::$items = $item;
    }

    public static function with($relations)
    {
        return new static();
    }

    public static function paginate()
    {
        return new LengthAwarePaginator(self::$items, count(self::$items), 1);
    }

    public function findOrFail()
    {
        return $this;
    }

    public function save(array $options = [])
    {

        return false;
    }
}
