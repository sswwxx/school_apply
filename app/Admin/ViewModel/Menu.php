<?php
/**
 * Created by PhpStorm.
 * User: lian
 * Date: 2019/11/20
 * Time: 14:54
 */

namespace App\Admin\ViewModel;

use App\Lian\Module\Contracts\Module;
use Encore\Admin\Auth\Database\Menu as Model;

class Menu extends Model
{
    public function allNodes(): array
    {
        $contents = $this->contentMenus();
        $modules = $this->modulesMenus();
        return collect()
            ->merge(parent::allNodes())
            ->filter(function ($item) use ($contents, $modules) {

                return ($item['id'] == 12 && $contents->count() > 0)
                    || ($item['id'] == 17 && $modules->count() > 0)
                    || (!in_array($item['id'], [12, 17]));
            })
            ->merge($contents)
            ->merge($modules)
            ->toArray();
    }

    protected function contentMenus()
    {
        return collect((new Column)->allNodes())->filter(function ($item) {

            return drivers_manager()->driver($item['type'])->hasAdminPage();

        })->map(function ($item) {

            $number = config('cms.admin_column_add_number');

            $item['id'] += $number;

            $item['parent_id'] = $item['parent_id'] ? $item['parent_id'] + $number : 12;

            $item['uri'] = 'columns/' . $item['slug'];

            $item['title'] = $item['name'];

            return $item;

        });
    }

    protected function modulesMenus()
    {
        $enabled = option('modules_enabled', []);

        return collect(modules_manager()->modules())->filter(function (Module $module) use ($enabled) {

            return in_array($module->getSlug(), $enabled) && $module->hasAdminPage();

        })->map(function (Module $module) {

            $number = config('cms.admin_column_add_number');

            $item = [];

            $item['id'] = mt_rand(999, 9999) + $number;

            $item['parent_id'] = 17;

            $item['icon'] = '';

            $item['uri'] = 'mod/' . $module->getSlug();

            $item['title'] = $module->getName();

            return $item;

        });
    }


}
