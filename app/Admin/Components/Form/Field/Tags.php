<?php


namespace App\Admin\Components\Form\Field;

use Encore\Admin\Form\Field\Tags as Field;

class Tags extends Field
{
    /**
     * {@inheritdoc}
     */
    public function render()
    {
        $this->script = "$(\"{$this->getElementClassSelector()}\").select2({
            tags: true,
            tokenSeparators: [','],
            createTag: function(params) {//解决部分浏览器开启 tags: true 后无法输入中文的BUG 
		         if (/[,;，； ]/.test(params.term)) {//支持【逗号】【分号】【空格】结尾生成tags
			        var str = params.term.trim().replace(/[,;，；]*$/, '');
			        return { id: str, text: str }
		         } else {
			        return null;
		        }
	        }
        });";

        if ($this->keyAsValue) {
            $options = $this->value + $this->options;
        } else {
            $options = array_unique(array_merge($this->value, $this->options));
        }

        return parent::render()->with([
            'options'    => $options,
            'keyAsValue' => $this->keyAsValue,
        ]);
    }
}