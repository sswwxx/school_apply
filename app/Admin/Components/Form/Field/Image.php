<?php
/**
 * Created by PhpStorm.
 * User: lian
 * Date: 2019/10/21
 * Time: 11:56
 */

namespace App\Admin\Components\Form\Field;

use Encore\Admin\Form\Field\Image as Field;

class Image extends Field
{
    public function render()
    {
        if ($default = $this->getDefault()) {
            $this->value = $default;
        }

        return parent::render();
    }
}
