<?php


namespace App\Admin\Components\Form\Field;


use Encore\Admin\Form\Field\File;

class SimpleFile extends File
{
    protected $view = 'form.simpleFile';
}