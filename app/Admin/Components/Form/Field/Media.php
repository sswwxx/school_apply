<?php
/**
 * Created by PhpStorm.
 * User: lian
 * Date: 2019/11/4
 * Time: 11:25
 */

namespace App\Admin\Components\Form\Field;


use Encore\Admin\Facades\Admin;
use Encore\Admin\Form\Field;
use Illuminate\Support\Facades\URL;

class Media extends Field
{
    protected static $js = [
        'vendor/sortable/sortable.js',
    ];

    protected $mimes = null;

    protected $view = 'form.media';

    protected $multiple = false;

    protected $preview = [
        'jpg|png|jpeg|bmp' => '<img src="__SRC__">',
        'mp4' => '<video controls="controls" src="__SRC__">',
        'pdf' => '<span class="file-icon"><i class="fa fa-file-pdf-o"></i></span>',
        'zip' => '<span class="file-icon"><i class="fa fa-file-zip-o"></i></span>',
        'word' => '<span class="file-icon"><i class="fa fa-file-word-o"></i></span>',
        'ppt' => '<span class="file-icon"><i class="fa fa-file-powerpoint-o"></i></span>',
        'xls' => '<span class="file-icon"><i class="fa fa-file-excel-o"></i></span>',
        'txt' => '<span class="file-icon"><i class="fa fa-file-text-o"></i></span>',
    ];

    public function images()
    {
        $this->mimes = 'jpg|png|jpeg|bmp';

        return $this;
    }

    public function mimes($mimes)
    {
        $this->mimes = $mimes;
    }

    public function multiple()
    {
        $this->multiple = true;

        return $this;
    }

    public function render()
    {
        if (empty($this->value)) {
            $this->value = $this->default;
        }

        if (is_string($this->value) && !URL::isValidUrl($this->value)) {
            $this->value = \Storage::disk(config('admin.upload.disk'))->url($this->value);
        }

        if (empty($this->value)) {
            $this->value = [];
        }

        if (is_string($this->value)) {
            $this->value = [$this->value];
        }

        $this->variables['mimes'] = $this->mimes;
        $this->variables['multiple'] = $this->multiple ? 1 :0;

        Admin::script($this->script());

        return parent::render();
    }

    protected function script()
    {
        $url = route('media-picker');
        $multiple = $this->multiple ? 'true' : 'false';
        $preview = json_encode($this->preview);
        $name = $this->id;

        return <<<EOT
        
        $(document).off('click','.media-remove');
        $(document).off('click','.media-tools');
        
        $(document).on('click','.media-remove',function(){
            $(this).closest('.media-thumbnail').remove();
        });
        
        var preview = function(src){
            var mime = src.split('.').pop();
            var previews = {$preview};
            for(var mimes in previews){
                if(mimes.indexOf(mime) === -1 ) continue;
                
                var html = previews[mimes];
                
                return html.replace('__SRC__',src);
            }
            
            return '<span class="file-icon"><i class="fa fa-file"></i></span>';
        };
        
        
        $('.media-thumbnail[data-src]').each(function(){
            var src = $(this).data('src');
            $(this).find('.preview').html(preview(src));
        });
        
        
        $(document).on('click','.media-tools',function(){
         
            var that = $(this);
            var box = that.closest('.media-picker');
            var template = box.find('template');
            var multiple = box.attr('data-multiple');
            
            var html = template.html();
            
            var rows = box.find('.media-rows');
            var mimes = that.find('input').val();
            
            console.log(multiple);
                        
            window.mediaPicked = function (list) {
                
                if (win) win.close();

                $(list).each(function (idx, src) {

                    var el = $(html);
                    
                    var name = src.split('/').pop();
                    
                    el.find('.preview').html(preview(src));
                    el.find('input').val(src);
                    el.find('.media-basename').html(name);
                    el.find('.media-show').attr('href',src);
                    
                    if(multiple == 0) rows.html(' ');
                    
                    rows.append(el);
                    
                    console.log(multiple);
                });
            };
            
            var url = '{$url}?multiple=' + multiple + '&mimes='+ mimes;
            win = window.open(url, "", "width=1012,height=600,top=100");
                
        });
        
        
        
        
        
        var create = function(bar){
            Sortable.create(bar, {
                group: "{$name}-rows",
                opacity: 0.5,//拖动的透明度
                revert: true, //缓冲效果
                cursor: 'move', //拖动的时候鼠标样式
                animation: 150, // ms, number 单位：ms，定义排序动画的时间
                disabled: false, // boolean 定义是否此sortable对象是否可用，为true时sortable对象不能拖放排序等功能，为false时为可以进行排序，相当于一个开关；
            });
        }
        
        var timer = function(){
            setTimeout(function(){
        
                var bar = document.getElementById("{$name}-rows");
                
                if(!bar) return timer();
                
                create(bar);
                
            },400);
        };
    
EOT;
    }

    public function prepare($value)
    {
        $value = array_filter($value);

        if ($this->multiple) return $value;

        return array_shift($value);
    }
}
