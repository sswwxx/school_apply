<?php
/**
 * Created by PhpStorm.
 * User: lian
 * Date: 2018/11/29
 * Time: 16:24
 */

namespace App\Admin\Components\Form\Field;

use Encore\Admin\Form\Field;

class Blank extends Field
{
    protected $view = 'form.blank';

    protected $url = null;

    protected $params = [];

    protected $listenField;

    public function listen($field, $sourceUrl, $params = [])
    {
        $this->listenField = $field;
        $this->url = $sourceUrl;
        $this->params = $params;

        return $this;
    }

    public function setParams($params)
    {
        $this->params = array_merge($this->params, $params);

        return $this;
    }


    public function render()
    {
        if (empty($this->listenField)) return null;

        $params = empty($this->params) ? "{}" : json_encode($this->params);

        $this->script .= <<<TEXT
        $(document).on('change','.{$this->listenField}',function() {
            var val = $(this).val();
            var params = {$params};
            params['q'] = val;
            $.get('{$this->url}',params,function(html){
                $('#{$this->id}').html(html);
            });        
        });
        var params = {$params};
        params['q'] = $('.{$this->listenField}').val();
        $.get('{$this->url}', params ,function(html){
            $('#{$this->id}').html(html);
        });
        
        
TEXT;

        $this->attribute('id', $this->id);
        $this->attribute('class', $this->id);

        return parent::render();
    }
}
