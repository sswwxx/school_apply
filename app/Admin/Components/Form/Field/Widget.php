<?php
/**
 * Created by PhpStorm.
 * User: lian
 * Date: 2019/11/15
 * Time: 11:39
 */

namespace App\Admin\Components\Form\Field;

use Encore\Admin\Admin;
use Encore\Admin\Form\Field;
use Encore\Admin\Form\NestedForm;

class Widget extends Field\HasMany
{
    /**
     * @var string
     */
    protected $viewMode = 'widget';

    /**
     * Table constructor.
     *
     * @param string $column
     * @param array $arguments
     */
    public function __construct($column, $arguments = [])
    {
        $this->column = $column;

        if (count($arguments) == 1) {
            $this->label = $this->formatLabel();
            $this->builder = $arguments[0];
        }

        if (count($arguments) == 2) {
            list($this->label, $this->builder) = $arguments;
        }

        $this->views['widget'] = 'form.widget';
    }

    public function prepare($input)
    {
        $form = $this->buildNestedForm($this->column, $this->builder);

        $prepare = $form->prepare($input);

        return collect($prepare)->reject(function ($item) {
            return $item[NestedForm::REMOVE_FLAG_NAME] == 1;
        })->map(function ($item) {
            unset($item[NestedForm::REMOVE_FLAG_NAME]);

            return $item;
        })->values()->toArray();
    }

    protected function buildNestedForm($column, \Closure $builder, $key = null)
    {
        $form = new NestedForm($column);

        $form->setForm($this->form)
            ->setKey($key);

        call_user_func($builder, $form);

        $form->hidden(NestedForm::REMOVE_FLAG_NAME)->default(0)->addElementClass(NestedForm::REMOVE_FLAG_CLASS);

        return $form;
    }

    public function render()
    {
        return $this->renderTable();
    }

    protected function renderTable()
    {
        $headers = [];
        $fields = [];
        $hidden = [];
        $scripts = [];

        /* @var Field $field */
        foreach ($this->buildNestedForm($this->column, $this->builder)->fields() as $field) {
            if (is_a($field, Field\Hidden::class)) {
                $hidden[] = $field->render();
            } else {
                /* Hide label and set field width 100% */
//                $field->setLabelClass(['hidden']);
                $field->setWidth(8, 2);
                $fields[] = $field->render();
                $headers[] = $field->label();
            }

            /*
             * Get and remove the last script of Admin::$script stack.
             */
            if ($field->getScript()) {
                $scripts[] = array_pop(\Encore\Admin\Admin::$script);
            }
        }

        /* Build row elements */
        $template = array_reduce($fields, function ($all, $field) {
            $all .= "<td>{$field}</td>";

            return $all;
        }, '');

        /* Build cell with hidden elements */
        $template .= '<td class="hidden">' . implode('', $hidden) . '</td>';

        $this->setupScript(implode("\r\n", $scripts));

        // specify a view to render.
        $this->view = $this->views[$this->viewMode];

        return parent::render()->with([
            'headers' => $headers,
            'forms' => $this->buildRelatedForms(),
            'template' => $template,
            'relationName' => $this->relationName,
            'options' => $this->options,
        ]);
    }

    /**
     * @return array
     */
    protected function buildRelatedForms()
    {
        $forms = [];

        if ($values = old($this->column)) {
            foreach ($values as $key => $data) {
                if ($data[NestedForm::REMOVE_FLAG_NAME] == 1) {
                    continue;
                }

                $forms[$key] = $this->buildNestedForm($this->column, $this->builder, $key)->fill($data);
            }
        } else {
            foreach ($this->value as $key => $data) {
                if (isset($data['pivot'])) {
                    $data = array_merge($data, $data['pivot']);
                }
                $forms[$key] = $this->buildNestedForm($this->column, $this->builder, $key)->fill($data);
            }
        }

        return $forms;
    }

    protected function getKeyName()
    {
        if (is_null($this->form)) {
            return null;
        }

        return 'id';
    }

    protected function setupScriptForWidgetView($templateScript)
    {
        parent::setupScriptForDefaultView($templateScript);

        $script = <<<EOT
        
        $('.has-many-{$this->column}').on('click','.moveUp',function(){
            var box = $(this).closest('.fields-group');
            box.prev().before(box);
        });
        
        $('.has-many-{$this->column}').on('click','.moveDown',function(){
            var box = $(this).closest('.fields-group');
            box.next().after(box);
        });
        
        
EOT;
        Admin::script($script);
    }

    public function fill($data)
    {
        $data[$this->column] = data_get($data,$this->column,[]);

        parent::fill($data);
    }

}
