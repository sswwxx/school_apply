<?php
/**
 * Created by PhpStorm.
 * User: lian
 * Date: 2019/10/31
 * Time: 18:09
 */

namespace App\Admin\Components\Form\Field;

use Codingyu\Ueditor\Editor as Field;

class UEditor extends Field
{
    protected static $js = [
        'vendor/ueditor/ueditor.config.js',
        'vendor/ueditor/ueditor.all.js',
    ];

    public function render()
    {
        $mediaPickerUrl = route('media-picker');

        $script = <<<EOT
window.UEDITOR_CONFIG.MEDIA_URL = '{$mediaPickerUrl}';
EOT;

        \Admin::script($script);

        return parent::render();
    }
}
