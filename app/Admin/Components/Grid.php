<?php
/**
 * Created by PhpStorm.
 * User: lian
 * Date: 2019/10/9
 * Time: 10:38
 */

namespace App\Admin\Components;

use Encore\Admin\Grid as BaseGrid;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Grid
 * @package App\Admin\Components
 *
 * @method Model|Builder model();
 */
class Grid extends BaseGrid
{

}
