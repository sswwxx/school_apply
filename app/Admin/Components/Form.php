<?php
/**
 * Created by PhpStorm.
 * User: lian
 * Date: 2019/10/8
 * Time: 16:56
 */

namespace App\Admin\Components;

use App\Admin\Components\Form\Field\Blank;
use App\Admin\Components\Form\Field\Media;
use App\Admin\Components\Form\Field\Widget;
use Encore\Admin\Form as BaseForm;

/**
 * Class Form
 * @package App\Admin\Components
 *
 * @method BaseForm\Field UEditor($name, $label = '')
 * @method BaseForm\Field editor($name, $label = '')
 * @method Media media($name, $label = '')
 * @method Widget widget($name, $label = '', $builder = null)
 * @method Blank blank($name, $label = '')
 */
class Form extends BaseForm
{

}
