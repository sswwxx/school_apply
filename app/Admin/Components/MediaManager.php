<?php
/**
 * Created by PhpStorm.
 * User: lian
 * Date: 2019/10/31
 * Time: 11:20
 */

namespace App\Admin\Components;

use Encore\Admin\Exception\Handler;
use Encore\Admin\Media\MediaManager as Manager;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use League\Flysystem\Adapter\Local;

class MediaManager extends Manager
{
    protected $routeIndexName = 'media-index';

    protected $allowMimes = null;

    /**
     * MediaManager constructor.
     *
     * @param string $path
     */
    public function __construct($path = '/')
    {
        $this->path = $path;

        $this->initStorage();
    }

    private function initStorage()
    {
        $disk = config('admin.extensions.media-manager.disk');

        $this->storage = Storage::disk($disk);

        if (!$this->storage->getDriver()->getAdapter() instanceof Local) {
            Handler::error('Error', '[laravel-admin-ext/media-manager] only works for local storage.');
        }
    }

    /**
     * @param string $routeIndexName
     */
    public function setRouteIndexName(string $routeIndexName): void
    {
        $this->routeIndexName = $routeIndexName;
    }

    /**
     * @param null $allowMimes
     */
    public function allowMimes($allowMimes): void
    {
        $allowMimes = trim($allowMimes);
        $this->allowMimes = $allowMimes;
    }

    /**
     * @return array
     */
    public function urls()
    {
        return [
            'path' => $this->path,
            'index' => route('media-picker'),
            'move' => route('media.move'),
            'delete' => route('media.delete'),
            'upload' => route('media.upload'),
            'new-folder' => route('media.new-folder'),
        ];
    }

    public function navigation()
    {
        $folders = explode('/', $this->path);

        $folders = array_filter($folders);

        $path = '';

        $navigation = [];

        foreach ($folders as $folder) {
            $path = rtrim($path, '/') . '/' . $folder;

            $navigation[] = [
                'name' => $folder,
                'url' => route($this->routeIndexName, ['path' => $path]),
            ];
        }

        return $navigation;
    }

    public function ls()
    {
        if (!$this->exists()) {
            Handler::error('Error', "File or directory [$this->path] not exists");

            return [];
        }

        $files = $this->storage->files($this->path);

        $files = $this->filesFilter($files);

        $directories = $this->storage->directories($this->path);

        return $this->formatDirectories($directories)
            ->merge($this->formatFiles($files))
            ->sort(function ($item) {
                return $item['name'];
            })->all();
    }

    public function formatFiles($files = [])
    {
        $files = array_map(function ($file) {
            return [
                'download'  => route('media-download', compact('file')),
                'icon'      => '',
                'name'      => $file,
                'preview'   => $this->getFilePreview($file),
                'isDir'     => false,
                'size'      => $this->getFilesize($file),
                'link'      => route('media-download', compact('file')),
                'url'       => $this->storage->url($file),
                'time'      => $this->getFileChangeTime($file),
            ];
        }, $files);

        return collect($files);
    }


    protected function filesFilter($files)
    {
        $mimes = $this->allowMimes ? explode('|', $this->allowMimes) : null;

        return array_filter($files, function ($path) use ($mimes) {

            if (Str::endsWith($path, ['.gitignore', '.DS_Store'])) return false;


            if (empty($mimes)) return true;

            if (Str::endsWith($path, $mimes)) return true;


            return false;
        });
    }

    public function formatDirectories($dirs = [])
    {
        $url = route($this->routeIndexName, ['path' => '__path__', 'view' => request('view')]);

        $preview = "<a href=\"$url\"><span class=\"file-icon text-aqua\"><i class=\"fa fa-folder\"></i></span></a>";

        $dirs = array_map(function ($dir) use ($preview) {
            return [
                'download' => '',
                'icon' => '',
                'name' => $dir,
                'preview' => str_replace('__path__', $dir, $preview),
                'isDir' => true,
                'size' => '',
                'link' => route($this->routeIndexName, ['path' => '/' . trim($dir, '/'), 'view' => request('view')]),
                'url' => $this->storage->url($dir),
                'time' => $this->getFileChangeTime($dir),
            ];
        }, $dirs);

        return collect($dirs);
    }

    /**
     * @param UploadedFile[] $files
     *
     * @return bool
     */
    public function upload($files = [])
    {
        foreach ($files as $file) {
            $this->putFile($file);
        }

        return true;
    }

    protected function putFile(UploadedFile $file)
    {
        if ($this->storage->exists($this->path . '/' . $file->getClientOriginalName())) {
            return $this->storage->putFile($this->path, $file);
        }

        return $this->storage->putFileAs($this->path, $file, $file->getClientOriginalName());
    }
}
