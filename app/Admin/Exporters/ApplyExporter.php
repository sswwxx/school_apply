<?php


namespace App\Admin\Exporters;


use Encore\Admin\Grid\Exporters\ExcelExporter;
use Maatwebsite\Excel\Concerns\WithMapping;

class ApplyExporter extends ExcelExporter implements WithMapping
{
    protected $fileName = '报名数据表.xlsx';

    protected $columns = [
        'name' => '姓名',
        'gender' => '性别',
        'nation' => '名族',
        'hometown' => '籍贯',
        'edu_level' => '文化程度',
        'political_status' => '政治面貌',
        'id_number' => '身份证号',
        'birthday' => '生日',
        'address' => '户籍所在地址',
        'hk_type' => '户籍性质',
        'company' => '工作单位',
        'company_date' => '入职时间',
        'mobile' => '联系电话',
        'email' => 'QQ邮箱',
        'em_name' => '紧急联系人',
        'em_mobile' => '紧急联系人电话',
        'apply_level' => '报考层次',
        'apply_school' => '预报名院校',
        'apply_ind' => '预报考专业',
        'apply_type' => '报考类别',
        'org_school' => '原毕业院校',
        'org_number' => '原毕业证书编号',
    ];

    public function map($apply): array
    {
        $apply = $apply->toArray();

        return array_merge($apply, [
            'gender' => data_get(['male' => '男', 'female' => '女',], $apply['gender']),
        ]);
    }

}