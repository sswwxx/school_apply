<?php


namespace App\Admin\Details;


use App\Models\Apply;
use Encore\Admin\Show;
use Illuminate\Contracts\Support\Renderable;

class ApplyDetail implements Renderable
{
    protected $id;

    public static function detail($id)
    {
        return (new static($id))->build();
    }

    public function __construct($id)
    {
        $this->id = $id;
    }

    protected function build(): Show
    {
        $model = Apply::findOrFail($this->id);
        $show = new Show($model);


        $show->field('name', '姓名');
        $show->field('gender', '性别')->using([
            'male' => '男',
            'female' => '女',
        ]);


        $show->field('nation', '名族');
        $show->field('hometown', '籍贯');
        $show->field('edu_level', '文化程度');
        $show->field('political_status', '政治面貌');
        $show->field('id_number', '身份证号');
        $show->field('birthday', '生日');
        $show->field('address', '户籍所在地址');
        $show->field('hk_type', '户籍性质');
        $show->field('company', '工作单位');
        $show->field('company_date', '入职时间');
        $show->field('mobile', '联系电话');
        $show->field('email', 'QQ邮箱');
        $show->field('em_name', '紧急联系人');
        $show->field('em_mobile', '紧急联系人电话');

        $show->divider();

        $show->field('apply_level', '报考层次');
        $show->field('apply_school', '预报名院校');
        $show->field('apply_ind', '预报考专业');
        $show->field('apply_type', '报考类别');

        $show->divider();

        $show->field('org_school', '原毕业院校');
        $show->field('org_number', '原毕业证书编');

        $show->divider();

        $show->field('ex', '报名资料')->as(function ($content) use ($model) {

            $lists = [
                'blue_id_img' => '本人2寸蓝底证件照',
                'id_img_zm' => '身份证正面',
                'id_img_fm' => '身份证反面',
                'id_img_sc' => '手持身份证照片',
                'certificate' => '缴费凭证截图',
                'xl_photo' => '学历照片',
                'reg_img' => '电子注册备案表',
            ];

            return view('show.images', [
                'lists' => $lists,
                'content' => $content,
                'apply' => $model,
            ]);

        })->unescape();


        $show->panel()->tools(function (Show\Tools $tools) {
            $tools->disableDelete();
            $tools->disableEdit();
            $tools->disableList();
        });

        $show->panel()->title('申请详细');

        return $show;
    }

    public function render()
    {
        return $this->build()->render();
    }


}