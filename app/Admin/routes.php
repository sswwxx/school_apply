<?php

use Illuminate\Routing\Router;

Admin::routes();


Route::group(config('admin.route'), function (Router $router) {

    $router->get('/', 'HomeController@index')->name('admin.home');

    $router->resource('/auth/options', 'Auth\OptionController')->names('admin-options');

    $router->get('/auth/columns/template', 'Auth\ColumnController@content')->name('column-content');

    $router->get('/auth/columns/sort', 'Auth\ColumnController@sort')->name('columns-sort');

    $router->get('/auth/columns/pages/{column}/{page}', 'Auth\ColumnPageController@index')->name('columns-pages.edit');
    $router->put('/auth/columns/pages/{column}/{page}', 'Auth\ColumnPageController@update')->name('columns-pages.update');

    $router->get('/auth/columns/driver/{column}', 'Auth\ColumnDriverController@index')->name('columns-drivers.edit');
    $router->put('/auth/columns/driver/{column}', 'Auth\ColumnDriverController@update')->name('columns-drivers.update');

    $router->resource('/auth/columns', 'Auth\ColumnController')->names('columns');

    $router->resource('/auth/menu', 'Auth\MenuController');


    $router->get('medias', 'MediaController@index')->name('media.index');
    $router->get('medias/download', 'MediaController@download')->name('media.download');
    $router->delete('medias/delete', 'MediaController@delete')->name('media.delete');
    $router->put('medias/move', 'MediaController@move')->name('media.move');
    $router->post('medias/upload', 'MediaController@upload')->name('media.upload');
    $router->post('medias/folder', 'MediaController@newFolder')->name('media.new-folder');

    $router->get('media/picker', 'MediaController@picker')->name('media-picker');


    $router->post('themes/change', 'ThemeController@change')->name('theme-change');
    $router->resource('themes', 'ThemeController');

    $router->get("columns/{column}", 'Column\DispatchController@index');
    $router->get("columns/{column}/create", 'Column\DispatchController@create');
    $router->get("columns/{column}/{id}/edit", 'Column\DispatchController@edit');
    $router->get("columns/{column}/{id}", 'Column\DispatchController@show');
    $router->post("columns/{column}", 'Column\DispatchController@store')->name('admin-column.create');
    $router->put("columns/{column}/{id}", 'Column\DispatchController@update')->name('admin-column.update');
    $router->delete("columns/{column}/{id}", 'Column\DispatchController@destroy');


    $router->get("mod/{column}", 'Module\DispatchController@index');
    $router->get("mod/{column}/create", 'Module\DispatchController@create');
    $router->get("mod/{column}/{id}/edit", 'Module\DispatchController@edit');
    $router->get("mod/{column}/{id}", 'Module\DispatchController@show');
    $router->post("mod/{column}", 'Module\DispatchController@store')->name('admin-module.create');
    $router->put("mod/{column}/{id}", 'Module\DispatchController@update')->name('admin-module.update');
    $router->delete("mod/{column}/{id}", 'Module\DispatchController@destroy');

    $router->resource('modules', 'Module\ModuleController')
        ->name('show', 'module.template')
        ->name('index', 'modules');
    $router->resource('modules/{module}/conf', 'Module\ConfController')
        ->name('index', 'module.config')
        ->name('post', 'module.config.save');

    $router->post('ck/upload', 'CKEditController@upload')->name('admin.ck-upload');


    $router->resource('senate','User\SenateController');
    $router->resource('teachers','User\TeacherController');
    $router->resource('finances','User\FianceController');
    $router->resource('students','User\StudentController');
    $router->resource('students','User\StudentController');
    $router->resource('reports','User\ApplyController');

});
