<?php


namespace App\Admin\Forms;


use App\Admin\Components\Form;
use App\Models\Role;
use App\Models\User\User;
use Illuminate\Contracts\Support\Renderable;

class TeacherForm implements Renderable
{
    protected $role;

    protected $form;

    public static function form($role)
    {
        return (new static($role))->build();
    }

    public function __construct($role)
    {
        $this->role = $role;
    }

    protected function build(): Form
    {
        User::scopeOfRole($this->role);

        $form = new Form(new User);

        $userTable = config('admin.database.users_table');
        $connection = config('admin.database.connection');

        $form->display('id', 'ID');

        $form->text('username', '用户名')
            ->creationRules(['required', "unique:{$connection}.{$userTable}"])
            ->updateRules(['required', "unique:{$connection}.{$userTable},username,{{id}}"])
            ->required();

        $form->text('name', '姓名')->rules('required')->required();
        $form->text('mobile', '手机号码')->rules('required')->required();
        $form->text('hometown', '籍贯');
        $form->select('gender', '性别')->options([
            'male' => '男',
            'female' => '女',
        ]);

        $form->password('password', '后台密码')->rules('required|confirmed')->required();
        $form->password('password_confirmation', trans('admin.password_confirmation'))->rules('required')->default(function ($form) {
            return $form->model()->password;
        })->required();

        $form->ignore(['password_confirmation']);


        $form->display('created_at', trans('admin.created_at'));
        $form->display('updated_at', trans('admin.updated_at'));

        $form->saving(function (Form $form) {
            if ($form->password && $form->model()->password != $form->password) {
                $form->password = bcrypt($form->password);
            }
        });

        $form->saved(function (Form $form) {
            /** @var User $user */
            $user = $form->model();

            if ($user->isRole($this->role)) return;

            $user->roles()->save(Role::whereSlug($this->role)->firstOrFail());

        });

        return $form;
    }

    public function render()
    {
        return $this->build()->render();
    }


}