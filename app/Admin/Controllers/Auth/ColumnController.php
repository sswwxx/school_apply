<?php
/**
 * Created by PhpStorm.
 * User: lian
 * Date: 2019/10/8
 * Time: 13:16
 */

namespace App\Admin\Controllers\Auth;

use App\Admin\Components\Form;
use App\Admin\Components\Grid;
use App\Admin\ViewModel\Column;
use App\Lian\Content\Contracts\Driver;
use App\Lian\Content\Contracts\TemplatePage;
use App\Models\ColumnMeta;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Controllers\MenuController;
use Encore\Admin\Form\EmbeddedForm;
use Encore\Admin\Grid\Tools;
use Encore\Admin\Layout\Content;
use Encore\Admin\Layout\Row;
use Encore\Admin\Tree;
use Illuminate\Http\Request;

class ColumnController extends MenuController
{
    use HasResourceActions;

    protected $title = '栏目';

    /**
     * Index interface.
     *
     * @param Content $content
     *
     * @return Content
     */
    public function index(Content $content)
    {
        $content->title($this->title);
        $content->description(trans('admin.list'));

        $content->row($this->grid());

        return $content;
    }

    protected function grid()
    {
        $grid = new Grid(new Column);

        $grid->disableRowSelector();
        $grid->disableFilter();
        $grid->disablePagination();

        $grid->column('id', '#');
        $grid->column('name', '栏目名称')->display(function ($name) {
            return $name;
        });

        $grid->column('url', '栏目网址')->copyable();

        $grid->column('status', '状态')->switch($this->statusSwitch())->width(100);

        $pages = collect(template()->theme()->pages())->map(function (TemplatePage $page) {

            return ['slug' => $page->slug(), 'name' => $page->name()];

        })->pluck('name', 'slug');


        $grid->column('type', '配置')->display(function () use ($pages) {

            $html = '';

            $driver = drivers_manager()->columnDriver($this);

            if ($driver->hasAdminConfig()) {

                $html .= '<a href="' . route('columns-drivers.edit', $this->id) . '">[' . $driver->getName() . ']</a><br />';
            }

            $templates = $this->template;

            $theme = template()->theme();

            $html .= collect($templates)->map(function ($page, $driver) use ($pages, $theme) {

                if ($page === '404') return '';

                $widgets = $theme->getPage($page)->widgets();

                if (empty($widgets)) return '';

                $url = route('columns-pages.edit', [$this->id, $driver]);

                return '<a href="' . $url . '">[' . data_get($pages, $page) . ']</a><br />';

            })->implode('');

            return $html;
        });

        $grid->tools(function (Tools $tools) {
            $url = route('columns-sort');
            $tools->append("<a href='{$url}' class='btn btn-info btn-sm'>排序</a>");
        });

        return $grid;
    }

    protected function statusSwitch()
    {
        return [
            'off' => ['value' => 'HIDE', 'text' => '隐藏', 'color' => 'danger'],
            'on' => ['value' => 'SHOW', 'text' => '显示', 'color' => 'success'],
        ];
    }

    public function sort(Content $content)
    {
        $content->title($this->title);
        $content->description(trans('admin.list'));
        $content->row(function (Row $row) {
            $row->column(10, $this->treeView()->render());
        });

        return $content;
    }

    /**
     * @return \Encore\Admin\Tree
     */
    protected function treeView()
    {
        return new Tree(new Column, function (Tree $tree) {
            $tree->path = route('columns.store');
            $tree->branch(function ($branch) {
                return view('tree.branch.column', compact('branch'));
            });
        });
    }

    /**
     * Redirect to edit page.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function show($id)
    {
        return redirect(Column::findOrFail($id)->url);
    }

    /**
     * Edit interface.
     *
     * @param string $id
     * @param Content $content
     *
     * @return Content
     */
    public function edit($id, Content $content)
    {
        $content->title($this->title);

        $content->description(trans('admin.edit'));

        $content->row($this->form()->edit($id));

        return $content;
    }

    protected function useOptionFields(Form $form)
    {
        $fields = option('columns_fields', []);

        foreach ($fields as $field) {

            $label = data_get($field, 'field_label');
            $name = data_get($field, 'field_name');
            $type = data_get($field, 'field_type');
            $params = json_decode(data_get($field, 'filed_params'), true) ?: [];

            if (empty($label) || empty($name) || empty($type)) continue;

            $field = $form->{$type}('fields.' . $name, $label);

            foreach ($params as $method => $param) {

                if (!method_exists($field, $method)) continue;

                $field->{$method}($param);
            }
        }
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    public function form()
    {
        $form = new Form(new Column());

        $form->tab('基础信息', function (Form $form) {

            $form->text('name', '栏目名称')->rules('required')->required();

            $form->text('slug', '栏目标识')->help('请输入不包含正反斜杠(/,\)的英文或者拼音路径');

            $form->icon('icon', '菜单图标')->default('fa-bars');

            $form->select('parent_id', '父级栏目')->options(Column::selectOptions(null, '无'));

            $form->media('thumb', '栏目封面')->images();

            $form->switch('status', '状态')->states($this->statusSwitch())->default('SHOW');

            $form->textarea('introduction', '栏目简介');

            $this->useOptionFields($form);

            $options = collect(drivers_manager()->drivers())->map(function (Driver $driver) {

                return ['name' => $driver->getName(), 'slug' => $driver->getSlug()];

            })->pluck('name', 'slug')->toArray();

            $form->select('type', '栏目类型')->options($options)
                ->rules('required')->required();

            $blank = $form->blank('template-content', '模板内容')
                ->listen('type', route('column-content'));

            $form->editing(function (Form $form) use ($blank) {
                $blank->setParams(['id' => $form->model()->getKey()]);
            });

        })->tab('SEO信息', function (Form $form) {

            $form->text('seo_title', 'SEO标题');
            $form->textarea('seo_description', 'SEO描述');
            $form->textarea('seo_keywords', 'SEO关键词');

        });


        $form->saving(function (Form $form) {

            $id = $form->model()->getKey();

            if (Column::where('id', '!=', $id)->where('name', $form->input('name'))->exists()) {

                $name = $form->input('name');
                admin_error("栏目[$name]已经存在.");

                return back();
            }

            $template = $form->input('template');
            $form->model()->setAttribute('template', $template);
        });

        return $form;
    }

    public function create(Content $content)
    {
        $content->title($this->title);

        $content->description(trans('admin.create'));

        $content->row($this->form());

        return $content;
    }

    /**
     * @param Request $request
     *
     * @return string
     * @throws
     *
     */
    public function content(Request $request)
    {
        $id = $request->get('id');
        $name = $request->get('q');

        $driver = drivers_manager()->driver($name);

        $column = Column::find($id);
        $template = $column ? $column->template : [];

        $pages = $driver->templates();


        $options = collect(template()->theme()->pages())->map(function (TemplatePage $page) {

            return ['slug' => $page->slug(), 'name' => $page->name()];

        })->prepend(['slug' => '404', 'name' => '禁用'])->pluck('name', 'slug');

        $form = new EmbeddedForm('template');

        $html = '';
        $script = '';

        foreach ($pages as $slug => $label) {

            $field = $form->select($slug, $label)->options($options)->default('404')->required();

            $field->fill($template);

            $html .= $field->render();
            $script .= $field->getScript();
        }

        $script = "<script>$(function() { {$script} })</script>";

        return $html . $script;
    }
}
