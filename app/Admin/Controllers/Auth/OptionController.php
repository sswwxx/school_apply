<?php
/**
 * Created by PhpStorm.
 * User: lian
 * Date: 2019/10/17
 * Time: 16:44
 */

namespace App\Admin\Controllers\Auth;


use App\Admin\Components\Form;
use App\Admin\Components\Grid;
use App\Admin\Controllers\Controller;
use App\Lian\Module\Contracts\Module;
use App\Models\Option;
use Encore\Admin\Form\Builder;
use Encore\Admin\Form\Field;
use Encore\Admin\Form\NestedForm;
use Encore\Admin\Form\Tools;
use Encore\Admin\Layout\Content;

class OptionController extends Controller
{
    protected $title = '站点设置';

    public function index(Content $content)
    {
        $content->header($this->title);

        $content->row($this->form());

        return $content;
    }

    public function form()
    {
        $form = new Form(new \App\Admin\ViewModel\Option);

        $form->tools(function (Tools $tools) {
            $tools->disableList();
            $tools->disableView();
            $tools->disableDelete();
        });

        $form->setTitle(' ');

        $form->setAction(url()->current() . '/0');

        $form->builder()->setMode(Builder::MODE_EDIT);

        $form->disableViewCheck();
        $form->disableCreatingCheck();
        $form->disableEditingCheck();

        $form->tab('站点设置', function (Form $form) {

            $form->media('site_logo', 'Logo')->images();
            $form->text('site_name', '网站名称');
            $form->media('favicon', '网站ICON');

            $form->text('site_icp', '备案信息');
            $form->text('site_copyright', '版权信息');

        })->tab('SEO设置', function (Form $form) {

            $form->text('seo_title', 'SEO标题');
            $form->textarea('seo_description', 'SEO描述');
            $form->textarea('seo_keywords', 'SEO关键词');

        })->tab('栏目扩展字段', function (Form $form) {

            $form->widget('columns_fields', '', function (NestedForm $form) {
                $form->text('field_label', '字段名')->required();
                $form->text('field_name', '字段标识')->required();
                $form->text('field_type', '字段类型')->required();
                $form->textarea('filed_params', '规则')
                ->help('字典JSON,键代表调用的方法，值为设置的值');
            });

        })->tab('后台设置', function (Form $form) {

            $form->text('admin_name', '名称')->default(config('admin.name'));

            $form->media('admin_logo', 'logo')->images();
            $form->media('admin_logo_mini', '迷你logo')->images();

            $form->media('admin_login_bg', '登录背景图');

            $form->multipleSelect('admin_layout', '布局')->options([
                "fixed" => '固定',
                "layout-boxed" => '盒装版式',
                "layout-top-nav" => '版面导航',
                "sidebar-collapse" => '侧边栏折叠',
                "sidebar-mini" => '侧边栏迷你',
            ])->default(config('admin.layout'));

            $form->select('admin_skin', '皮肤')->options([
                "skin-blue" => '蓝色',
                "skin-blue-light" => '蓝色高亮',
                "skin-yellow" => '黄色',
                "skin-yellow-light" => '黄色高亮',
                "skin-green" => '绿色',
                "skin-green-light" => '绿色高亮',
                "skin-purple" => '紫色',
                "skin-purple-light" => '紫色高亮',
                "skin-red" => '红色',
                "skin-red-light" => '红色高亮',
                "skin-black" => '黑色',
                "skin-black-light" => '黑色高亮'
            ])->default(config('admin.skin'));

        })->tab('模块启停', function (Form $form) {

            $modules = collect(modules_manager()->modules())->map(function (Module $module) {
                return $module->getName();
            });

            $form->listbox('modules_enabled', '模块启用')->options($modules);

        });


        $data = Option::options();

        $form->builder()->fields()->each(function (Field $field) use ($data) {
            $field->fill($data);
        });

        $form->saved(function (Form $form) {

            # 从表单Model中获取所有的数据
            $attributes = $form->model()->getAttributes();
            # 保存数据
            $this->saveListConfig($attributes);
            # 输出保存成功信息
            admin_success(trans('admin.save_succeeded'));

            return redirect($form->resource(-1));
        });

        return $form;
    }

    protected function saveListConfig($attributes)
    {
        Option::set($attributes);
    }

    public function create(Content $content)
    {
        throw new \Exception('no create page');
    }

    public function update()
    {
        return $this->form()->store();
    }

    public function show()
    {
        return redirect(route('admin-options.index'));
    }

    protected function grid()
    {
        $grid = new Grid(new Option);


        return $grid;
    }

}
