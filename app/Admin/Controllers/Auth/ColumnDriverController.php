<?php
/**
 * Created by PhpStorm.
 * User: lian
 * Date: 2019/12/5
 * Time: 15:02
 */

namespace App\Admin\Controllers\Auth;


use App\Admin\Components\Form;
use App\Admin\Controllers\Controller;
use App\Admin\ViewModel\Column;
use App\Admin\ViewModel\Faker;
use Encore\Admin\Form\Field;
use Encore\Admin\Form\Tools;
use Encore\Admin\Layout\Content;

class ColumnDriverController extends Controller
{
    public function index(Content $content, $id)
    {
        /** @var Column $column */
        $column = Column::findOrFail($id);

        $content->title($column->name);
        $content->description('模块配置');

        $form = $this->form($column);

        $form->edit($id);

        $data = $column->metaValue($column->getDriverMetaKey()) ?: [];

        $form->builder()->fields()->each(function (Field $field) use ($data) {
            $field->fill($data);
        });

        $content->row($form);


        return $content;
    }


    protected function form(Column $column)
    {
        $form = new Form(new Faker);

        $action = url()->current();
        $name = $column->getDriverMetaKey();

        $form->tools(function (Tools $tools) {
            $tools->disableList();
            $tools->disableView();
            $tools->disableDelete();
        });

        $form->setTitle(' ');
        $form->setAction($action);

        $driver = drivers_manager()->columnDriver($column);

        $driver->configForm($form);

        $form->saved(function (Form $form) use ($name, $action, $column) {

            $model = $form->model();
            $data = $model->getAttributes();

            $column->saveMeta($name, $data);

            admin_success('保存成功！');

            if (request('after-save')) {
                return redirect($action);
            }

            return redirect(route('columns.index'));
        });

        $form->disableCreatingCheck();
        $form->disableViewCheck();


        return $form;
    }

    public function update($id)
    {
        $column = Column::findOrFail($id);

        return $this->form($column)->update($id);
    }
}
