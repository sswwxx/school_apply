<?php
/**
 * Created by PhpStorm.
 * User: lian
 * Date: 2019/11/14
 * Time: 16:09
 */

namespace App\Admin\Controllers\Auth;


use App\Admin\Components\Form;
use App\Admin\Controllers\Controller;
use App\Admin\ViewModel\Column;
use App\Admin\ViewModel\Option;
use App\Lian\Content\Contracts\TemplateWidget;
use Encore\Admin\Form\Builder;
use Encore\Admin\Form\Tools;
use Encore\Admin\Layout\Content;

class ColumnPageController extends Controller
{

    /**
     * @param Content $content
     * @param $cid
     * @param $driver
     *
     * @return Content
     *
     */
    public function index(Content $content, $cid, $driver)
    {
        $content->header('栏目样式');

        $form = $this->form($cid, $driver);
        $form->builder()->setMode(Builder::MODE_EDIT);

        $content->row($form);

        return $content;
    }

    /**
     * @param $id
     * @param $driver
     *
     * @return Form
     *
     * @throws
     */
    protected function form($id, $driver)
    {
        $form = new Form(new Option);

        /** @var Column $column */
        $column = Column::findOrFail($id);

        if (empty($column->template[$driver])) {
            abort(404);
        }

        $form->disableCreatingCheck();
        $form->disableViewCheck();

        $form->tools(function (Tools $tools) {
            $tools->disableDelete();
            $tools->disableView();
            $tools->disableList();
        });

        $name = $column->getWidgetMetaKey($driver);

        $theme = template()->theme();

        $action = route('columns-pages.update', [$id, $driver]);

        $form->setTitle($column->name);
        $form->setAction($action);

        $page = $theme->getPage($column->template[$driver]);
        $widgets = $page->widgets();

        $data = $column->metaValue($name);

        /** @var TemplateWidget $widget */
        foreach ($widgets as $widget) {

            $form->tab($widget->name(), function (Form $form) use ($widget, $data) {
                $form->hidden('updated_at');
                $widget->buildForm($form, $data);
            });
        }


        $form->saved(function (Form $form) use ($column, $name, $action) {

            $model = $form->model();
            $data = $model->getAttributes();

            $column->saveMeta($name, $data);

            admin_success('保存成功！');

            if (request('after-save')) {
                return redirect($action);
            }

            return redirect(route('columns.index'));
        });


        return $form;
    }

    /**
     * @param $cid
     * @param $driver
     *
     * @return mixed
     *
     * @throws
     */
    public function update($cid, $driver)
    {
        return $this->form($cid, $driver)->store();
    }

    public function show()
    {
        return redirect(route('columns.index'));
    }

}
