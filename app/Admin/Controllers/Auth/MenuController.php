<?php
/**
 * Created by PhpStorm.
 * User: lian
 * Date: 2019/11/20
 * Time: 17:01
 */

namespace App\Admin\Controllers\Auth;

use Encore\Admin\Auth\Database\Menu;
use Encore\Admin\Controllers\MenuController as Controller;

class MenuController extends Controller
{
    public function __construct()
    {
        config(['admin.database.menu_model' => Menu::class]);
    }
}
