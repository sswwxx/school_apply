<?php
/**
 * Created by PhpStorm.
 * User: lian
 * Date: 2019/10/31
 * Time: 10:47
 */

namespace App\Admin\Controllers;

use App\Admin\Components\MediaManager;
use Encore\Admin\Layout\Content;
use Encore\Admin\Media\MediaController as Controller;
use Illuminate\Http\Request;

class MediaController extends Controller
{
    public function index(Request $request)
    {
        $content = resolve(Content::class);

        $content->header(trans('admin.Media manager'));

        $path = $request->get('path', '/');
        $view = $request->get('view', 'list');

        $manager = new MediaManager($path);

        $content->body(view("laravel-admin-media::$view", [
            'list' => $manager->ls(),
            'nav' => $manager->navigation(),
            'url' => $manager->urls(),
        ]));

        return $content;
    }

    public function picker(Content $content, Request $request)
    {
        $path = $request->get('path', '/');

        $manager = new MediaManager($path);

        $manager->setRouteIndexName('media-picker');
        $manager->allowMimes($this->mediaSessionConf($request, 'mimes'));


        $multiple = $this->mediaSessionConf($request, 'multiple');

        $content->row(view("laravel-admin-media::picker.list", [
            'list' => $manager->ls(),
            'nav' => $manager->navigation(),
            'url' => $manager->urls(),
            'multiple' => $multiple,
            "paginator" => ''
        ]));

        return $content;
    }

    protected function mediaSessionConf(Request $request, $name)
    {
        if ($request->exists($name)) {
            $mimes = $request->get($name);
            session()->put('media-' . $name, $mimes);
        }

        return session()->get('media-' . $name);
    }

    public function upload(Request $request)
    {
        $files = $request->file('files');
        $dir = $request->get('dir', '/');

        $manager = new MediaManager($dir);

        try {
            if ($manager->upload($files)) {
                admin_toastr(trans('admin.upload_succeeded'));
            }
        } catch (\Exception $e) {
            admin_toastr($e->getMessage(), 'error');
        }

        return back();
    }
}
