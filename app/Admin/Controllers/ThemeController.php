<?php
/**
 * Created by PhpStorm.
 * User: lian
 * Date: 2019/11/14
 * Time: 16:09
 */

namespace App\Admin\Controllers;


use App\Admin\Components\Form;
use App\Admin\ViewModel\Option;
use App\Lian\Content\Contracts\TemplateWidget;
use Encore\Admin\Form\Builder;
use Encore\Admin\Form\Tools;
use Encore\Admin\Layout\Content;
use Illuminate\Http\Request;

class ThemeController extends Controller
{
    public function index(Content $content)
    {
        if ($theme = env('THEME')) {

            return redirect(url()->current() . '/' . $theme . '/edit');
        }

        $content->header(trans('admin.theme'));

        $content->row($this->listForm());

        return $content;
    }


    protected function listForm()
    {
        $themes = template()->allThemes();

        $current = Option::get('theme', '');

        $list = view('theme.list', compact('themes', 'current'));

        return $list;
    }

    public function change(Request $request)
    {
        $name = $request->get('name');

        \App\Models\Option::set('theme', $name);

        return redirect()->back();
    }

    /**
     * @param Content $content
     * @param $id
     *
     * @return Content
     *
     */
    public function edit(Content $content, $id)
    {
        $content->header('主题设置');

        $form = $this->themeForm($id);
        $form->builder()->setMode(Builder::MODE_EDIT);

        $content->row($form);

        return $content;
    }

    /**
     * @param $id
     *
     * @return Form
     *
     * @throws
     */
    protected function themeForm($id)
    {
        $form = new Form(new Option);

        $form->disableCreatingCheck();
        $form->disableViewCheck();

        $form->tools(function (Tools $tools) {
            $tools->disableDelete();
            $tools->disableView();
            $tools->disableList();
        });


        if (empty($id)) $id = request('id');


        $theme = template()->theme($id);

        $form->setTitle($theme->name());
        $form->setAction(route('themes.update', $theme->slug()));

        $widgets = $theme->widgets();

        $data = $theme->getWidgetsData();

        /** @var TemplateWidget $widget */
        foreach ($widgets as $widget) {

            $form->tab($widget->name(), function (Form $form) use ($widget, $data) {
                $form->hidden('updated_at');
                $widget->buildForm($form, $data);
            });
        }

        $form->saved(function (Form $form) use ($theme) {

            $model = $form->model();
            $data = $model->getAttributes();

            $theme->saveWidgetData($data);

            admin_success('保存成功！');

            $url = route('themes.index');
            if ($theme = env('THEME')) {
                $url = $url . '/' . $theme . '/edit';
            }
            return redirect($url);
        });


        return $form;
    }

    /**
     * @param $id
     *
     * @return mixed
     *
     * @throws
     */
    public function update($id)
    {
        return $this->themeForm($id)->store();
    }

    public function show($id)
    {
        return redirect(route('themes.edit', $id));
    }

}
