<?php
/**
 * Created by PhpStorm.
 * User: lian
 * Date: 2019/11/20
 * Time: 15:18
 */

namespace App\Admin\Controllers\Column;


use App\Admin\Components\Form;
use App\Admin\Controllers\Controller;
use App\Lian\Content\Contracts\Driver;
use App\Lian\Content\Manager;
use App\Models\Column;
use Encore\Admin\Layout\Content;

class DispatchController extends Controller
{
    protected $manager;

    /** @var Column */
    protected $column;

    public function __construct(Manager $manager)
    {
        $this->manager = $manager;
    }

    public function create(Content $content, $slug)
    {
        $this->setColumnBySlug($slug);

        $content->header($this->column->name);
        $content->description(trans('admin.create'));

        $content->row($this->form());

        return $content;
    }

    protected function setColumnBySlug($slug)
    {
        $this->column = Column::whereSlug($slug)->firstOrFail();
    }

    protected function form(): Form
    {
        return $this->getDriver()->adminForm();
    }

    protected function getDriver(): Driver
    {
        return $this->manager->columnDriver($this->column);
    }

    public function edit(Content $content, $slug, $id)
    {
        $this->setColumnBySlug($slug);

        $content->header($this->column->name);
        $content->description(trans('admin.edit'));

        $content->row($this->form()->edit($id));

        return $content;
    }

    public function index(Content $content, $slug)
    {
        $this->setColumnBySlug($slug);

        $content->header($this->column->name);

        $content->row($this->getDriver()->adminPage());

        return $content;
    }

    public function store($slug)
    {
        $this->setColumnBySlug($slug);

        return $this->form()->store();
    }

    public function destroy($slug, $id)
    {
        $this->setColumnBySlug($slug);

        return $this->form()->destroy($id);
    }

    public function show($slug, $id)
    {
        $this->setColumnBySlug($slug);

        return redirect(site_column_url($this->column, $id));
    }

    public function update($slug, $id)
    {
        $this->setColumnBySlug($slug);
        return $this->form()->update($id);
    }
}
