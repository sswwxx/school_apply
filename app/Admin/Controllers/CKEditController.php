<?php
/**
 * Created by PhpStorm.
 * User: lian
 * Date: 2019/12/20
 * Time: 10:51
 */

namespace App\Admin\Controllers;


use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;

class CKEditController extends Controller
{
    public function upload(Request $request)
    {
        /** @var UploadedFile $file */
        $file = collect($request->allFiles())->first();

        $name = $file->store('images/' . date('Y-m'), config('admin.upload.disk'));
        $url = \Storage::disk(config('admin.upload.disk'))->url($name);

        return [
            'uploaded' => 1,
            'fileName' => basename($url),
            'url' => $url
        ];
    }
}
