<?php
/**
 * Created by PhpStorm.
 * User: lian
 * Date: 2019/12/26
 * Time: 14:14
 */

namespace App\Admin\Controllers\Module;


use App\Admin\Components\Form;
use App\Admin\Controllers\Controller;
use App\Admin\ViewModel\Faker;
use App\Lian\Helper\OptionKey;
use App\Lian\Module\Contracts\Module;
use App\Models\Option;
use Encore\Admin\Form\Field;
use Encore\Admin\Form\Tools;
use Encore\Admin\Layout\Content;

class ConfController extends Controller
{
    public function index(Content $content, $slug)
    {
        $module = modules_manager()->module($slug);

        $content->title($module->getName());
        $content->description('模块配置');

        $form = $this->form($module);

        $data = Option::get(OptionKey::moduleConfig($module), []);

        $form->builder()->fields()->each(function (Field $field) use ($data) {
            $field->fill($data);
        });

        $content->row($form);


        return $content;
    }


    protected function form(Module $module)
    {
        $form = new Form(new Faker());

        $action = url()->current();

        $form->tools(function (Tools $tools) {
            $tools->disableList();
            $tools->disableView();
            $tools->disableDelete();

            $url = route('modules');
            $btn = <<<HTML
<a href="{$url}" class="btn btn-sm btn-default" title="返回"><i class="fa fa-angle-double-left"></i><span class="hidden-xs">&nbsp;返回</span></a>
HTML;
            $tools->append($btn);

        });

        $form->setTitle(' ');
        $form->setAction($action);

        $module->configForm($form);

        $form->saved(function (Form $form) use ($action, $module) {

            $data = $form->model()->getAttributes();

            Option::set(OptionKey::moduleConfig($module), $data);

            admin_success('保存成功！');

            return redirect(route('modules'));
        });

        $form->disableCreatingCheck();
        $form->disableViewCheck();
        $form->disableEditingCheck();


        return $form;
    }

    public function store($id)
    {
        $module = modules_manager()->module($id);

        return $this->form($module)->update($id);
    }

}
