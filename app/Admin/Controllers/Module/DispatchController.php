<?php
/**
 * Created by PhpStorm.
 * User: lian
 * Date: 2019/11/20
 * Time: 15:18
 */

namespace App\Admin\Controllers\Module;


use App\Admin\Components\Form;
use App\Admin\Controllers\Controller;
use App\Lian\Module\Contracts\Module;
use App\Lian\Module\Manager;
use App\Models\Column;
use Encore\Admin\Layout\Content;

class DispatchController extends Controller
{
    protected $manager;

    /** @var Module */
    protected $module;

    public function __construct(Manager $manager)
    {
        $this->manager = $manager;
    }

    public function create(Content $content, $slug)
    {
        $this->setModuleBySlug($slug);

        $content->header($this->module->getName());
        $content->description(trans('admin.create'));

        $content->row($this->form());

        return $content;
    }

    protected function setModuleBySlug($slug)
    {
        $this->module = $this->manager->module($slug);
    }

    protected function form(): Form
    {
        return $this->module->adminForm();
    }

    public function edit(Content $content, $slug, $id)
    {
        $this->setModuleBySlug($slug);

        $content->header($this->module->getName());
        $content->description(trans('admin.edit'));

        $content->row($this->form()->edit($id));

        return $content;
    }

    public function index(Content $content, $slug)
    {
        $this->setModuleBySlug($slug);

        $content->header($this->module->getName());

        $content->row($this->module->adminPage());

        return $content;
    }

    public function store($slug)
    {
        $this->setModuleBySlug($slug);

        return $this->form()->store();
    }

    public function destroy($slug, $id)
    {
        $this->setModuleBySlug($slug);

        return $this->form()->destroy($id);
    }

    public function show($slug)
    {
        return redirect(admin_url('mod/' . $slug));
    }

    public function update($slug, $id)
    {
        $this->setModuleBySlug($slug);
        return $this->form()->update($id);
    }
}
