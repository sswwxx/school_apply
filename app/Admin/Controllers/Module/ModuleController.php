<?php
/**
 * Created by PhpStorm.
 * User: lian
 * Date: 2019/12/26
 * Time: 14:13
 */

namespace App\Admin\Controllers\Module;


use App\Admin\Components\Form;
use App\Admin\Components\Grid;
use App\Admin\Controllers\Controller;
use App\Admin\ViewModel\Faker;
use App\Lian\Content\Contracts\TemplatePage;
use App\Lian\Helper\OptionKey;
use App\Lian\Module\Contracts\Module;
use App\Models\Option;
use Encore\Admin\Form\Builder;
use Encore\Admin\Form\Field;
use Encore\Admin\Form\Tools;
use Encore\Admin\Layout\Content;

class ModuleController extends Controller
{
    public function index(Content $content)
    {
        $content->header('模块管理');
        $content->description('已用模块');


        $content->row($this->grid());

        return $content;
    }


    protected function grid()
    {
        $grid = new Grid(new Faker());

        $enabled = option('modules_enabled', []);

        $modules = collect(modules_manager()->modules())->map(function (Module $module) use ($enabled) {

            if (!in_array($module->getSlug(), $enabled)) return false;

            $faker = new Faker();
            $faker->slug = $module->getSlug();
            $faker->name = $module->getName();
            $faker->slug2 = $module->getSlug();

            return $faker;

        })->filter();

        Faker::item($modules);

        $grid->column('name', '模块名称');
        $grid->column('slug', '模块标识');
        $grid->column('slug2', '操作')->display(function ($slug) {

            $module = modules_manager()->module($slug);
            
            $link = '';
            if (!empty($module->templates())) {
                $uri = route('module.template', $module->getSlug());
                $link .= "<a href='{$uri}'> 模板配置 </a>";
            }
            if ($module->hasAdminConfig()) {
                $uri = route('module.config', $module->getSlug());
                $link .= "<a href='{$uri}'> 参数配置 </a>";
            }

            return $link;
        });

        $grid->disableCreateButton();
        $grid->disableRowSelector();

        $grid->disableActions();
        $grid->disableFilter();
        $grid->disablePagination();

        return $grid;
    }

    public function show(Content $content, $slug)
    {
        $module = modules_manager()->module($slug);

        $content->header($module->getName());
        $content->description('模板配置');

        $data = option(OptionKey::moduleTemplate($module), []);

        $form = $this->form($module);

        $form->builder()->fields()->each(function (Field $field) use ($data) {
            $field->fill($data);
        });

        $content->row($form);

        return $content;
    }

    protected function form(Module $module)
    {
        $form = new Form(new Faker());

        $form->setTitle(' ');
        $form->disableViewCheck();
        $form->disableEditingCheck();
        $form->disableCreatingCheck();

        $form->tools(function (Tools $tools) {
            $tools->disableDelete();
            $tools->disableView();
            $tools->disableList();

            $url = route('modules');
            $btn = <<<HTML
<a href="{$url}" class="btn btn-sm btn-default" title="返回"><i class="fa fa-angle-double-left"></i><span class="hidden-xs">&nbsp;返回</span></a>
HTML;
            $tools->append($btn);
        });

        $form->builder()->setMode(Builder::MODE_EDIT);
        $form->setAction(url()->current());

        $templates = $module->templates();

        $options = collect(template()->theme()->pages())->map(function (TemplatePage $page) {

            return ['slug' => $page->slug(), 'name' => $page->name()];

        })->prepend(['slug' => '404', 'name' => '禁用'])->pluck('name', 'slug');

        foreach ($templates as $name => $label) {
            $form->select($name, $label)->options($options)->default(404)->required();
        }

        $form->saved(function (Form $form) use ($module) {

            $data = $form->model()->getAttributes();

            Option::set(OptionKey::moduleTemplate($module), $data);

            admin_success('保存成功！');

            return redirect(route('modules'));
        });

        return $form;
    }

    public function update($slug)
    {
        return $this->form(modules_manager()->module($slug))->store();
    }
}
