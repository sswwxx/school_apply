<?php


namespace App\Admin\Controllers\User;


use App\Admin\Controllers\Controller;
use App\Admin\Details\ApplyDetail;
use App\Admin\Exporters\ApplyExporter;
use App\Admin\Grids\ApplyGrid;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Layout\Content;

class StudentController extends Controller
{
    use HasResourceActions;

    protected $title = '学生';

    protected function grid()
    {
        $grid = ApplyGrid::grid();

        $grid->model()->where('status', 'success');

        $grid->column('show', '查看')->display(function () {
            $url = url()->current() . '/' . $this->id;
            return "<a href='{$url}'>查看</a>";
        });

        $grid->disableExport(false);
        $grid->exporter(new ApplyExporter());

        return $grid;
    }


    public function index(Content $content)
    {
        $content->header($this->title . '管理');
        $content->description('列表');

        $content->row($this->grid());

        return $content;
    }

    protected function detail($id)
    {
        return ApplyDetail::detail($id);
    }

    public function show(Content $content, $id)
    {
        $content->header('报名详情');
        $content->description('详情');

        $content->row($this->detail($id));

        return $content;
    }
}