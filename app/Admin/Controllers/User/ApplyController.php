<?php


namespace App\Admin\Controllers\User;


use App\Admin\Components\Form;
use App\Admin\Controllers\Controller;
use App\Admin\Details\ApplyDetail;
use App\Admin\Grids\ApplyGrid;
use App\Models\Apply;
use App\Models\Role;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Form\Tools;
use Encore\Admin\Layout\Content;
use Encore\Admin\Layout\Row;

class ApplyController extends Controller
{
    protected $status = [
        Role::FINANCE => Role::SENATE,
        Role::SENATE => 'success',
    ];

    public function index(Content $content)
    {
        $content->header('报名审核');
        $content->description('列表');

        $content->row($this->grid());

        return $content;
    }


    public function grid()
    {
        $grid = ApplyGrid::grid();

        $grid->column('show', '审核/查看')->display(function () {
            $url = url()->current() . '/' . $this->id . '/edit';
            return "<a href='{$url}'>审核/查看</a>";
        });

        if (Admin::user()->isRole(Role::FINANCE)) {
            $grid->model()->where('status', Role::FINANCE);
        } else if (Admin::user()->isRole(Role::SENATE)) {
            $grid->model()->where('status', Role::SENATE);
        }


        return $grid;
    }


    public function edit(Content $content, $id)
    {
        $content->header('报名审核');
        $content->description('审核');

        $content->row(function (Row $row) use ($id) {


            $apply = Apply::findOrFail($id);

            if (in_array($apply->status, ['success', 'refuse'])) {
                $row->column(12, $this->detail($id));
            } else {
                $row->column(7, $this->detail($id));
                $row->column(5, $this->form($id)->edit($id));
            }
        });

        return $content;
    }

    protected function form($id = 0)
    {
        $form = new Form(new Apply);

        $apply = Apply::findOrFail($id);

        $next = data_get($this->status, $apply->status);

        $states = [
            'on' => ['value' => $next, 'text' => '通过', 'color' => 'primary'],
            'off' => ['value' => 'refuse', 'text' => '不通过', 'color' => 'default'],
        ];

        $form->switch('status', '审核')->states($states);

        $options = [
            Role::FINANCE => '财务审核',
            Role::SENATE => '教务审核',
            'success' => '审核通过',
            'refuse' => '审核拒绝',
        ];


        $form->setTitle(data_get($options, $apply->status));

        $form->disableCreatingCheck();
        $form->disableViewCheck();
        $form->disableEditingCheck();
        $form->builder()->getFooter()->disableReset();

        $form->tools(function (Tools $tools) {
            $tools->disableDelete();
            $tools->disableView();
            $tools->disableList();
        });

        $form->saving(function (Form $form) use ($next) {

            $data = [
                $next . '_id' => Admin::user()->id,
                $next . '_time' => time(),
            ];

            $form->model()->setAttribute('ex', $data);
        });

        return $form;
    }

    protected function detail($id)
    {
        return ApplyDetail::detail($id);
    }

    public function update($id)
    {
        return $this->form($id)->update($id);
    }

}