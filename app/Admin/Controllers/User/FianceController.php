<?php


namespace App\Admin\Controllers\User;


use App\Admin\Forms\TeacherForm;
use App\Admin\Grids\TeacherGrid;
use App\Models\Role;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Layout\Content;

class FianceController
{
    use HasResourceActions;

    protected $title = '财务';

    protected function grid()
    {
        return TeacherGrid::grid(Role::FINANCE);
    }


    protected function form()
    {
        return TeacherForm::form(Role::FINANCE);
    }

    public function index(Content $content)
    {
        $content->header($this->title . '管理');
        $content->description('列表');

        $content->row($this->grid());

        return $content;
    }

    public function create(Content $content)
    {
        $content->header('新增' . $this->title);
        $content->description('新增');

        $content->row($this->form());

        return $content;
    }

    public function edit(Content $content, $id)
    {
        $content->header('编辑' . $this->title);
        $content->description('编辑');

        $content->row($this->form()->edit($id));

        return $content;
    }
}