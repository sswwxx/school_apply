<?php


namespace App\Admin\Grids;


use App\Admin\Components\Grid;
use App\Models\Apply;
use Illuminate\Contracts\Support\Renderable;

class ApplyGrid implements Renderable
{
    public static function grid()
    {
        return (new static())->build();
    }

    protected function build(): Grid
    {
        $grid = new Grid(new Apply);

        $grid->disableCreateButton();
        $grid->disableActions();
        $grid->disableRowSelector();
        $grid->disableBatchActions();

        $grid->column('id', 'ID');
        $grid->column('user.name', '姓名');
        $grid->column('teacher.name', '老师');
        $grid->column('apply_level', '报考层次');
        $grid->column('apply_school', '预报名院校');
        $grid->column('apply_ind', '预报考专业');
        $grid->column('apply_type', '报考类别');
        $grid->column('created_at', '报名时间')->date('Y.m.d H:i');

        return $grid;
    }

    public function render()
    {
        return $this->build()->render();
    }


}