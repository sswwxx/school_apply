<?php


namespace App\Admin\Grids;


use App\Admin\Components\Grid;
use App\Models\User\User;
use Illuminate\Contracts\Support\Renderable;

class TeacherGrid implements Renderable
{
    protected $role;

    public static function grid($role)
    {
        return (new static($role))->build();
    }

    public function __construct($role)
    {
        $this->role = $role;
    }

    public function build(): Grid
    {
        User::scopeOfRole($this->role);

        $grid = new Grid(new User);
        
        $grid->column('name', '姓名');
        $grid->column('mobile', '手机号码');
        $grid->column('gender', '性别')->display(function ($gender) {
            return data_get(['male' => '男', 'female' => '女',], $gender);
        });


        return $grid;
    }

    public function render()
    {
        return $this->build()->render();
    }


}