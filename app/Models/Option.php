<?php
/**
 * Created by PhpStorm.
 * User: lian
 * Date: 2019/10/17
 * Time: 16:41
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;


/**
 * App\Models\Option
 *
 * @property int $id
 * @property string $name
 * @property array|null $value
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Option newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Option newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Option query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Option whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Option whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Option whereValue($value)
 * @mixin \Eloquent
 */
class Option extends Model
{
    /**
     * @var array
     */
    protected static $options = [];

    public $timestamps = false;

    protected $guarded = [];

    protected $casts = [
        'value' => 'json'
    ];

    /**
     * 获取参数
     *
     * @param $name
     * @param null $default
     * @return bool|mixed
     */
    public static function get($name, $default = null)
    {
        return data_get(self::options(), $name, $default);
    }

    /**
     * @return array
     */
    public static function options()
    {
        if (!empty(self::$options)) return self::$options;

        $result = static::all()->pluck('value', 'name');

        $options = [];

        foreach ($result as $key => &$item) {
            data_set($options, $key, $item);
        }


        return self::$options = $options;
    }

    /**
     * 设置参数
     *
     * 如果 name 为数组则设置多个参数
     *
     * @param string|array $name
     * @param null $value
     *
     * @return bool
     *
     */
    public static function set($name, $value = null)
    {
        if (is_array($name)) {

            foreach ($name as $key => $value) static::set($key, $value);

            return true;
        }

        $option = static::firstOrNew(['name' => $name]);

        $option->value = $value;

        return $option->save();
    }

    /**
     * 把数据合并到参数中
     *
     * @param $data
     */
    public static function merge($data)
    {
        self::$options = collect(self::options())->merge($data);
    }

}
