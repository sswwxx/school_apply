<?php
/**
 * Created by PhpStorm.
 * User: lian
 * Date: 2019/11/20
 * Time: 14:56
 */

namespace App\Models;


use App\Models\Traits\HasColumnCache;
use App\Models\Traits\HasFamilyColumns;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Str;

/**
 * App\Models\Column
 *
 * @property int $id
 * @property string|null $slug
 * @property string $name
 * @property int $order
 * @property string|null $thumb
 * @property string|null $icon
 * @property int $parent_id
 * @property string|null $introduction
 * @property string|null $template
 * @property string $type
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Column newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Column newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Column query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Column whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Column whereIcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Column whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Column whereIntroduction($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Column whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Column whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Column whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Column whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Column whereTemplate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Column whereThumb($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Column whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Column whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Column[] $children
 * @property-read int|null $children_count
 * @property-read \App\Models\Column $parent
 * @property string|null $status
 * @property-read mixed $url
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Column whereStatus($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ColumnMeta[] $metas
 * @property-read int|null $metas_count
 * @property string|null $seo_title
 * @property string|null $seo_keywords
 * @property string|null $seo_description
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Column whereSeoDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Column whereSeoKeywords($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Column whereSeoTitle($value)
 * @property-read \App\Models\ColumnMeta $fields
 */
class Column extends Model
{
    use HasFamilyColumns;
    use HasColumnCache;

    protected $table = 'columns';

    protected $appends = [
        'url'
    ];

    protected $casts = [
        'template' => 'json'
    ];


    /**
     * 获取栏目的URL地址
     *
     * @return string
     */
    public function getUrlAttribute()
    {
        return site_column_url($this);
    }

    public function save(array $options = [])
    {
        /**
         * 将输入的标识字段进行转义
         */
        $this->slug = $this->formatSlug();

        return parent::save($options);
    }


    protected function formatSlug()
    {
        $slug = Str::slug($this->slug) ?: '';

        if (Column::where('slug', $slug)->where('id', '!=', $this->id)->exists()) {
            $slug = str_ireplace('/', '-', $this->name);
        }

        return $slug;
    }


    public function metaValue($key)
    {
        /** @var ColumnMeta $meta */
        $meta = $this->meta($key);

        return $meta ? $meta->meta_value : null;
    }

    public function meta($key)
    {
        return $this->metas()->where('meta_key', $key)->first();
    }

    public function fields(): HasOne
    {
        return $this->hasOne(ColumnMeta::class, 'column_id')
            ->where('meta_key', 'fields')
            ->withDefault(function () {
                return new ColumnMeta([
                    'meta_key' => 'fields',
                    'column_id' => $this->getKey()
                ]);
            });
    }

    public function metas(): HasMany
    {
        return $this->hasMany(ColumnMeta::class, 'column_id', 'id');
    }

    public function saveMeta($key, $value)
    {
        /** @var ColumnMeta $meta */
        $meta = $this->metas()->firstOrNew(['meta_key' => $key]);

        $meta->meta_value = $value;

        return $meta->save();
    }

    public function getWidgetMetaKey($driver)
    {
        return strtolower("templates.{$this->type}.{$driver}.{$this->template[$driver]}");
    }

    public function getDriverMetaKey()
    {
        return strtolower("driver.{$this->type}");
    }

    public function migrateAttributes($data)
    {
        if (!is_array($data)) $data = [];
        
        $this->attributes = array_merge($this->attributes, $data);
    }

}
