<?php
/**
 * Created by PhpStorm.
 * User: lian
 * Date: 2019/10/22
 * Time: 10:13
 */

namespace App\Models\Traits;


trait HasFamilyColumns
{
    public function brothers()
    {
        return static::allColumns()->where('parent_id', $this->parent_id);
    }

    public function ancestor()
    {
        if (empty($this->parent_id)) {
            return $this;
        }

        return $this->_findAncestor($this->parent_id);
    }

    

    private function _findAncestor($pid, $nodes = [])
    {
        if (empty($nodes)) {
            $nodes = static::allColumns();
        }

        foreach ($nodes as $node) {

            if ($node->id != $pid) continue;

            if (empty($node->parent_id)) return $node;

            return $this->_findAncestor($node->parent_id, $nodes);
        }

        return $this;
    }

    public function children()
    {
        return $this->_findChildren($this->id);
    }

    private function _findChildren($cid, $nodes = [])
    {
        $columns = collect();

        if (empty($nodes)) {
            $nodes = static::allColumns();
        }

        foreach ($nodes as $column) {

            if ($column['parent_id'] != $cid) continue;

            $columns->push($column);
            $columns->merge($this->_findChildren($column['id'], $nodes));
        }

        return $columns;
    }
}
