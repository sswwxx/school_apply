<?php
/**
 * Created by PhpStorm.
 * User: lian
 * Date: 2019/10/22
 * Time: 14:12
 */

namespace App\Models\Traits;


use App\Models\Column;
use App\Models\Menu;
use Illuminate\Database\Eloquent\Collection;


trait HasColumnCache
{
    /** @var null|Collection */
    protected static $all = null;

    /**
     * @param $id
     * @return static|null
     */
    public static function findColumn($id)
    {
        return static::allColumns()->where('id', $id)->first();
    }

    /**
     * @return Collection
     */
    public static function allColumns()
    {
        if (empty(self::$all)) {
            self::$all = static::orderByRaw('`order`=0,`order`')->get();
            self::$all->load('fields')->map(function (Column $column) {
                $fields = $column->fields->meta_value;
                $column->migrateAttributes($fields);
            });
        }

        return self::$all;
    }


}
