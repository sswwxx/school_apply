<?php
/**
 * Created by PhpStorm.
 * User: lian
 * Date: 2019/10/8
 * Time: 16:35
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

/**
 * App\Models\Post
 *
 * @property int $id
 * @property string $title
 * @property string|null $introduction
 * @property string|null $thumb
 * @property string|null $author 作者
 * @property string|null $url 来源地址
 * @property int|null $order
 * @property int $column_id
 * @property int $user_id
 * @property string|null $content
 * @property int $view_num
 * @property int|null $visible 可见
 * @property int|null $top 置顶
 * @property int|null $hot 热门
 * @property int|null $recommend 热门
 * @property string $publish_time
 * @property array|null $extra
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \App\Models\Column $column
 * @property-read \App\Models\Product $product
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Post onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereAuthor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereColumnId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereExtra($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereHot($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereIntroduction($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post wherePublishTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereRecommend($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereThumb($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereTop($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereViewNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereVisible($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Post withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Post withoutTrashed()
 * @mixin \Eloquent
 * @property array|null $images 图集
 * @property array|null $attributes 属性
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereAttributes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Post whereImages($value)
 */
class Post extends Model
{
    use SoftDeletes;

    protected $table = 'posts';

    protected $casts = [
        'extra' => 'json',
        'attributes' => 'json',
        'images' => 'json',
    ];

    public function getExtraAttribute()
    {
        $fields = $this->attributes['extra'];

        return $fields ? json_decode($fields, true) : [];
    }

    public function column(): BelongsTo
    {
        return $this->belongsTo(Column::class, 'column_id', 'id');
    }

    public function save(array $options = [])
    {
        if (empty($this->publish_time)) {
            $this->publish_time = $this->created_at ?: date('Y-m-d H:i:s');
        }

        if (empty($this->content)) {
            $this->content = '';
        }

        return parent::save($options);
    }

    public function next()
    {
        return static::where('id', '>', $this->id)
            ->where('column_id', $this->column_id)
            ->orderBy('id')->first();
    }

    public function pre()
    {
        return static::where('id', '<', $this->id)
            ->where('column_id', $this->column_id)
            ->orderBy('id')->first();
    }

    public function getUrlAttribute()
    {
        return site_column_url($this->column, $this->id);
    }

    public function product()
    {
        return $this->hasOne(Product::class, 'post_id', 'id');
    }
}
