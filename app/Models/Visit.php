<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Visit
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Visit newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Visit newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Visit query()
 * @mixin \Eloquent
 * @property int $id
 * @property int $user_id
 * @property string $path
 * @property string $ip
 * @property string $country
 * @property string $province
 * @property string $city
 * @property string $isp
 * @property string $area
 * @property string $agent
 * @property string $version
 * @property string $browser
 * @property string $device
 * @property int $desktop
 * @property \Illuminate\Support\Carbon $created_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Visit whereAgent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Visit whereArea($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Visit whereBrowser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Visit whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Visit whereCountry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Visit whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Visit whereDesktop($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Visit whereDevice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Visit whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Visit whereIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Visit whereIsp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Visit wherePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Visit whereProvince($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Visit whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Visit whereVersion($value)
 */
class Visit extends Model
{
    protected $table = 'visits';

    public $timestamps = false;
}