<?php
/**
 * Created by PhpStorm.
 * User: lian
 * Date: 2019/12/2
 * Time: 10:16
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ColumnMeta
 *
 * @property int $id
 * @property int $column_id
 * @property string $meta_key
 * @property array|null $meta_value
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ColumnMeta newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ColumnMeta newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ColumnMeta query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ColumnMeta whereColumnId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ColumnMeta whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ColumnMeta whereMetaKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ColumnMeta whereMetaValue($value)
 * @mixin \Eloquent
 */
class ColumnMeta extends Model
{
    public $timestamps = false;

    protected $table = 'columns_metas';

    protected $guarded = [];

    protected $casts = [
        'meta_value' => 'json',
    ];

    public function setAttribute($key, $value)
    {
        if ($key == 'meta_value' || $key == 'meta_key' || $key == 'column_id') {
            return parent::setAttribute($key, $value);
        }

        $val = empty($this->attributes['meta_value']) ? [] : json_decode($this->attributes['meta_value'], true);

        $val[$key] = $value;

        $this->attributes['meta_value'] = json_encode($val);

        return $this;
    }
}
