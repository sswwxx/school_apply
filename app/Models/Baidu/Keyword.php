<?php


namespace App\Models\Baidu;


use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Baidu\Keyword
 *
 * @property int $id
 * @property string $keyword
 * @property int $rank
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Baidu\Keyword newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Baidu\Keyword newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Baidu\Keyword query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Baidu\Keyword whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Baidu\Keyword whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Baidu\Keyword whereKeyword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Baidu\Keyword whereRank($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Baidu\Keyword whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Keyword extends Model
{
    protected $table = 'baidu_keywords';

    protected $guarded = [];


}