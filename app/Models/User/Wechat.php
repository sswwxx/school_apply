<?php
/**
 * Created by PhpStorm.
 * User: lian
 * Date: 2020/1/2
 * Time: 10:46
 */

namespace App\Models\User;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;


/**
 * App\Models\User\Wechat
 *
 * @property int $id
 * @property string $openid
 * @property string $name
 * @property string $nickname
 * @property string $avatar
 * @property int $sex
 * @property string $country
 * @property string $province
 * @property string $city
 * @property int $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Wechat newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Wechat newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Wechat query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Wechat whereAvatar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Wechat whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Wechat whereCountry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Wechat whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Wechat whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Wechat whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Wechat whereNickname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Wechat whereOpenid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Wechat whereProvince($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Wechat whereSex($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Wechat whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User\Wechat whereUserId($value)
 * @mixin \Eloquent
 * @property-read \App\Models\User\User $user
 */
class Wechat extends Model
{
    protected $table = 'users_wechat';

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

}
