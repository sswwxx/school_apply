<?php


namespace App\Models;


use App\Models\User\User;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Apply
 *
 * @property int $id
 * @property int $user_id
 * @property int $teacher_id
 * @property string $name 姓名
 * @property string $gender 性别
 * @property string $nation 名族
 * @property string $hometown 籍贯
 * @property string $edu_level 文化程度
 * @property string $political_status 政治面貌
 * @property string $id_number 身份证号
 * @property string $birthday 生日
 * @property string $address 户籍所在地址
 * @property string $hk_type 户籍性质
 * @property string $company 工作单位
 * @property string $company_date 入职时间
 * @property string $mobile 联系电话
 * @property string $email QQ邮箱
 * @property string $em_name 紧急联系人
 * @property string $em_mobile 紧急联系人电话
 * @property string $apply_level 报考层次
 * @property string $apply_school 预报名院校
 * @property string $apply_ind 预报考专业
 * @property string $apply_type 报考类别
 * @property string $org_school 原毕业院校
 * @property string $org_number 原毕业证书编号
 * @property string|null $ex
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Apply newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Apply newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Apply query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Apply whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Apply whereApplyInd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Apply whereApplyLevel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Apply whereApplySchool($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Apply whereApplyType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Apply whereBirthday($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Apply whereCompany($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Apply whereCompanyDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Apply whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Apply whereEduLevel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Apply whereEmMobile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Apply whereEmName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Apply whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Apply whereEx($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Apply whereGender($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Apply whereHkType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Apply whereHometown($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Apply whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Apply whereIdNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Apply whereMobile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Apply whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Apply whereNation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Apply whereOrgNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Apply whereOrgSchool($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Apply wherePoliticalStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Apply whereTeacherId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Apply whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Apply whereUserId($value)
 * @mixin \Eloquent
 */
class Apply extends Model
{
    protected $table = 'applies';


    protected $casts = [
        'ex' => 'json',
    ];


    public function setExAttribute($val)
    {
        $ex = (array)$this->ex;

        $this->attributes['ex'] = json_encode(array_merge($ex, $val));
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function teacher()
    {
        return $this->belongsTo(User::class, 'teacher_id');
    }
}