<?php
/**
 * Created by PhpStorm.
 * User: lian
 * Date: 2020/1/2
 * Time: 17:41
 */

namespace App\Models;


use App\Lian\Module\Contracts\OrderTicket;
use App\Models\User\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;


/**
 * App\Models\Order
 *
 * @property int $id
 * @property int $user_id
 * @property int $tid 类型ID
 * @property string $order_type 订单类型
 * @property string $trade_no 订单号
 * @property float|null $discount_fee 优惠金额
 * @property float $total_fee 订单金额
 * @property float $pay_fee 支付金额
 * @property string $subject 主题
 * @property string|null $pay_type 支付类型。支付宝，微信支付
 * @property string|null $pay_method 支付方式。
 * @property string|null $paid_at 支付时间
 * @property string $status 订单状态
 * @property float $refunded_fee 退款金额
 * @property string|null $refunded_at 退款时间
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\User\User $member
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereDiscountFee($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereOrderType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order wherePaidAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order wherePayFee($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order wherePayMethod($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order wherePayType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereRefundedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereRefundedFee($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereSubject($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereTid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereTotalFee($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereTradeNo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereUserId($value)
 * @mixin \Eloquent
 * @property float $paid_fee 支付金额
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order wherePaidFee($value)
 */
class Order extends Model implements OrderTicket
{
    const STATUS_CREATED = 'CREATED';

    const STATUS_PAID = 'PAID';

    const STATUS_REFUND = 'REFUND';

    protected $table = 'orders';

    public function member(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function user(): User
    {
        return $this->member;
    }

    public function tid()
    {
        return $this->tid;
    }

    public function type()
    {
        return $this->order_type;
    }

    public function subject()
    {
        return $this->subject;
    }

    public function totalFee()
    {
        return $this->total_fee;
    }

    public function discountFee()
    {
        return $this->discount_fee;
    }

    public function paid(): bool
    {
        return $this->status === self::STATUS_PAID;
    }

    public function refunded(): bool
    {
        return $this->refunded_at > 0;
    }

    public function payType(): string
    {
        // TODO: Implement payType() method.
    }

    public function payMethod(): string
    {
        // TODO: Implement payMethod() method.
    }

    public function createdAt()
    {
        // TODO: Implement createdAt() method.
    }

    public function paidAt()
    {
        // TODO: Implement paidAt() method.
    }

    public function refundedAt()
    {
        return $this->refunded_at;
    }

    public function paidFee()
    {
        return $this->paid_fee;
    }

    public function refundedFee()
    {
        // TODO: Implement refundedFee() method.
    }

    public function id()
    {
        return $this->id;
    }

    public function tradeNo()
    {
        return $this->trade_no;
    }

}
