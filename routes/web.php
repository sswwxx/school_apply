<?php

/** @var \Illuminate\Routing\Router $router */
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

$router->resource('apply', 'ApplyController')->only(['index', 'store']);


app(\App\Lian\Content\Contracts\Dispatcher::class)->registerRoutes($router);
app(\App\Lian\Module\Contracts\Dispatcher::class)->registerRoutes($router);


