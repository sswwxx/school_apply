
**柏乐先生CMS安装说明**

1.克隆代码
`git@gitlab.com:fs-internal/fscms.git`

2.检出分支
`git checkout 2.0`

3.进入目录
`cd fscms`

4.安装依赖
`composer install`

5.创建环境变量文件
`cp .env.example .env`

6.修改`.env`

7.合并数据库迁移
`php artisan migrate`

8.填充数据
`php artisan db:seed`

9.同步后台菜单
`php artisan menu:sync`

10.创建磁盘软链接
`php artisan storage:link`


**相关文档**

Laravel https://learnku.com/docs/laravel/5.7

Laravel-admin https://laravel-admin.org/docs/zh/

Blade模板 https://learnku.com/docs/laravel/5.7/blade/2265

Composer https://getcomposer.org/

Composer阿里镜像 https://learnku.com/laravel/t/30710


**系统变量说明**

变量值通过`option($key,$defualt = null)`函数获得

`site_logo` 站点Logo

`site_name` 站点名称

`favicon` 网站icon

`site_icp` 备案信息

`site_copyright` 版权信息



**SEO说明**

SEO信息通过`seo()`函数获取

`seo()->getTitle()` 获取SEO标题

`seo()->getDescription()` 获取SEO描述

`seo()->getKeywords()` 获取SEO关键词

**模板说明**

页面当前内容 通过当前页面的`slug`来获取
例如`slug`为`articles` 则通过`$articles`来获取内容列表
 
全局挂件中的内容通过`slug`获取
页面挂件的内容也通过`slug`获取
多级内容支持使用点语法获取，例如`options('banner.header.name')`
