<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, maximum-scale=1.0,minimum-scale=1.0,user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="Pragma"   content="no-cache"> 

    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0,user-scalable=no,width=device-width"/>
    <meta name="format-detection" content="telephone=no" />
    <meta name="app-mobile-web-app-capable"  content="yes" /> 
    <meta name="app-mobile-web-app-status-bar-style" content="black-translucent" /> 
    <meta name="description" content="{{ seo()->getDescription() }}"/>
    <meta name="keywords" content="{{ seo()->getKeywords() }}"/>
    <link rel="icon" href="{{ option('favicon') }}">
    
    <meta name="author" content="by VINQUI">
    <title>{{ seo()->getTitle() }}</title>
    <!-- Jquery -->
    <script src="{{ asset('site/vinqui/js/jquery-3.4.1/jquery-3.4.1.min.js') }}"></script>
    <!-- Plugin Swiper -->
    <link rel="stylesheet" href="{{ asset('site/vinqui/plugins/swiper-5.2.0/css/swiper.min.css') }}">
    <link rel="stylesheet" href="{{ asset('site/vinqui/plugins/swiper-animate/animate.min.css') }}">

    <!-- public.css -->
    <link rel="stylesheet" href="{{ asset('site/vinqui/css/public.css') }}?v=1.6">
    @yield('head')
    <!--  response.css -->
    <link rel="stylesheet" href="{{ asset('site/vinqui/css/response.css') }}?v=2.1">
</head>
<body>
<!-- header -->
@if(isset($header))

    @php($menus = site_menus())

    @if($header === 'white')
        <header class="header-bg">
            <div class="head-nav">
                <div class="nav_logo_wrap">
                    <!-- Logo -->
                    <div class="logo">
                        <img src="{{ asset('site/vinqui/images/public/logo-white.png') }}" alt="">
                    </div>
                    <!-- toggle button -->
                    <button type="button" class="navbar-mobile-toggle">
                        <span class="icon-bar top-bar"></span>
                        <span class="icon-bar bottom-bar"></span>
                    </button>
                </div>
                <!-- Menu -->
                <div class="nav_main menu">
                    <ul class="clearfix">
                        @foreach($menus as $menu)
                            <li @if($menu['active']) class="active" @endif><a href="{{ $menu['url'] }}">{{ $menu['name'] }}</a></li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </header>
    @elseif($header === 'orange')

        <header class="header-bg">
            <div class="head-nav">
                <div class="nav_logo_wrap">
                    <!-- Logo -->
                    <div class="logo">
                        <img src="{{ asset('site/vinqui/images/public/logo-c.png') }}" alt="">
                    </div>
                    <!-- toggle button -->
                    <button type="button" class="navbar-mobile-toggle">
                        <span class="icon-bar top-bar"></span>
                        <span class="icon-bar bottom-bar"></span>
                    </button>
                </div>
                <!-- Menu -->
                <div class="nav_main menu menu-c">
                    <ul class="clearfix">
                        @foreach($menus as $menu)
                            <li @if($menu['active']) class="active" @endif><a href="{{ $menu['url'] }}">{{ $menu['name'] }}</a></li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </header>

    @endif
@endif

<!-- 首页 -->
<div class="page">
    @yield('body')
    <!-- 备案号 -->
    <div class="recordNumber"><a href="http://www.beian.miit.gov.cn/">备案号：{{ option('site_icp') }}</a></div>
</div>

<!-- <footer></footer> -->
<!-- wow -->
<script src="{{ asset('site/vinqui/plugins/wow/wow.min.js') }}"></script>
<!-- Plugin Swiper -->
<script src="{{ asset('site/vinqui/plugins/swiper-5.2.0/js/swiper.min.js') }}"></script>
<script src="{{ asset('site/vinqui/plugins/swiper-animate/swiper.animate1.0.3.min.js') }}"></script>
<!-- public -->
<script src="{{ asset('site/vinqui/js/public.js') }}?v=1.3"></script>
@yield('foot')

</body>
</html>
