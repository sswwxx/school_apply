<?php

namespace Tests\Browser;

use Faker\Generator;
use Faker\Provider\Base;
use Faker\Provider\Lorem;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class TestAdminAddPosts extends BaseTest
{
    /**
     * A Dusk test example.
     *
     * @throws
     */
    public function testExample()
    {
        $this->browse(function (Browser $browser) {

            $faker = $faker = app(Generator::class);
            $sentence = $faker->sentence(5, true);

            $browser->visit('admin/columns/test/create')
                ->assertSee('测试栏目')
                ->assertSee('创建');

            $browser->type('title', $sentence);
            $browser->attach('thumb', __DIR__ . '/attach/thumb.jpg');

            $browser->type('author', '老罗');

            $browser->type('url', 'http://www.baidu.com');

            $browser->type('introduction', $faker->sentence(50, true));
            $browser->value('textarea[name=content]', $faker->sentence(255, true));

            $browser->click('.btn-primary[type="submit"]');

            $browser->pause(2000);
            $browser->assertSee($sentence);
        });


    }
}
