<?php

namespace Tests\Browser;

use App\Models\Column;
use Laravel\Dusk\Browser;

class TestAdminAddColumn extends BaseTest
{
    /**
     * A Dusk test example.
     *
     * @throws
     */
    public function testExample()
    {
        $this->browse(function (Browser $browser) {

            $browser->visit('admin/auth/columns/create')->assertSee('栏目名称');

            $browser->type('title', '测试栏目');
            $browser->select('type', Column::TYPE_POSTS);

            $browser->type('name', 'test');
            $browser->type('icon', 'fa-optin-monster');
            $browser->select('parent_id', 0);
            $browser->attach('thumb', __DIR__ . '/attach/thumb.jpg');
            $browser->type('introduction', '测试栏目');
            $browser->click('.btn-primary[type="submit"]');

            $browser->pause(2000);
            $browser->assertSee('展开');
            $browser->assertSee('测试栏目');

        });
    }


}
