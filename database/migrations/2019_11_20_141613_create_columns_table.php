<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateColumnsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('columns', function (Blueprint $table) {

            $table->increments('id');
            $table->string('slug', 50)->nullable()->default('');

            $table->string('name', 50);
            $table->integer('order')->default(0);

            $table->string('thumb')->nullable()->default('');
            $table->string('icon', 50)->nullable()->default('');

            $table->integer('parent_id')->default(0);

            $table->string('introduction', 512)->nullable()->default('');

            $table->text('template')->nullable();
            $table->string('type', 50)->default('');

            $table->string('status', 20)->default('SHOW');

            $table->timestamps();

            $table->index('slug');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('columns');
    }
}
