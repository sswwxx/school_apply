<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {

            $table->bigIncrements('id');

            $table->bigInteger('user_id');

            $table->bigInteger('tid')->comment('类型ID');

            $table->string('order_type')->comment('订单类型');

            $table->string('trade_no')->unique()->comment('订单号');

            $table->decimal('discount_fee', 12, 2)->nullable()->default(0.00)->comment('优惠金额');

            $table->decimal('total_fee', 12, 2)->comment('订单金额');

            $table->decimal('paid_fee', 12, 2)->default(0.0)->comment('支付金额');

            $table->string('subject')->comment('主题');

            $table->string('pay_type')->nullable()->default('')->comment('支付类型。支付宝，微信支付');

            $table->string('pay_method')->nullable()->default('')->comment('支付方式。');

            $table->dateTime('paid_at')->nullable()->comment('支付时间');

            $table->string('status')->comment('订单状态');

            $table->decimal('refunded_fee', 12, 2)->default(0)->comment('退款金额');

            $table->dateTime('refunded_at')->nullable()->comment('退款时间');

            $table->timestamps();

            $table->index('tid');

            $table->index(['tid', 'order_type']);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
