<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppliesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applies', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('user_id');

            $table->bigInteger('teacher_id');

            $table->string('status')->comment('状态');

            $table->string('name')->comment('姓名')->nullable()->default('');

            $table->string('gender')->comment('性别')->nullable()->default('');

            $table->string('nation')->comment('名族')->nullable()->default('');

            $table->string('hometown')->comment('籍贯')->nullable()->default('');

            $table->string('edu_level')->comment('文化程度')->nullable()->default('');

            $table->string('political_status')->comment('政治面貌')->nullable()->default('');

            $table->string('id_number')->comment('身份证号')->nullable()->default('');

            $table->string('birthday')->comment('生日')->nullable()->default('');

            $table->string('address')->comment('户籍所在地址')->nullable()->default('');

            $table->string('hk_type')->comment('户籍性质')->nullable()->default('');

            $table->string('company')->comment('工作单位')->nullable()->default('');

            $table->string('company_date')->comment('入职时间')->nullable()->default('');

            $table->string('mobile')->comment('联系电话')->nullable()->default('');

            $table->string('email')->comment('QQ邮箱')->nullable()->default('');

            $table->string('em_name')->comment('紧急联系人')->nullable()->default('');

            $table->string('em_mobile')->comment('紧急联系人电话')->nullable()->default('');


            $table->string('apply_level')->comment('报考层次')->nullable()->default('');

            $table->string('apply_school')->comment('预报名院校')->nullable()->default('');

            $table->string('apply_ind')->comment('预报考专业')->nullable()->default('');

            $table->string('apply_type')->comment('报考类别')->nullable()->default('');

            $table->string('org_school')->comment('原毕业院校')->nullable()->default('');
            $table->string('org_number')->comment('原毕业证书编号')->nullable()->default('');

            $table->longText('ex')->nullable();

            $table->timestamps();
        });



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applies');
    }
}
