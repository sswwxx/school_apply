<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {

            $table->bigIncrements('id');

            $table->bigInteger('post_id');


            $table->bigInteger('option_id')->default(0)->comment('规格ID');
            $table->text('option_info')->nullable()->comment('规格信息');

            $table->decimal('price', 12, 2)->default(0.00)->comment('价格');

            $table->string('code')->nullable()->default('')->comment('编码');

            $table->timestamps();

            $table->index('post_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
