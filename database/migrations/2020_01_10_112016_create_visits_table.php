<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVisitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visits', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('user_id');

            $table->string('path');
            $table->ipAddress('ip');

            $table->string('country')->nullable()->default('');
            $table->string('province')->nullable()->default('');
            $table->string('city')->nullable()->default('');
            $table->string('isp')->nullable()->default('');
            $table->string('area')->nullable()->default('');
            $table->string('agent')->nullable()->default('');
            $table->string('version')->nullable()->default('');
            $table->string('browser')->nullable()->default('');
            $table->string('device')->nullable()->default('');
            $table->boolean('desktop');
            $table->timestamp('created_at');

            $table->index('user_id');
            $table->index(['province', 'city']);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visits');
    }
}
