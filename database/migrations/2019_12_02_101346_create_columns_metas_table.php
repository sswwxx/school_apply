<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateColumnsMetasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('columns_metas', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('column_id');

            $table->string('meta_key');

            $table->longText('meta_value')->nullable();

            $table->index('column_id', 'meta_key');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('columns_metas');
    }
}
