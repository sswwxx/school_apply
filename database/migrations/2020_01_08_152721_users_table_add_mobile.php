<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UsersTableAddMobile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {

            $table->string('mobile', 11)->after('username')->nullable()->default('');

            $table->index('mobile');

        });

        Schema::create('users_codes', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->bigInteger('user_id', false, true);
            $table->string('usage');

            $table->string('code');

            $table->dateTime('expired_at');

            $table->index(['user_id', 'usage']);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('mobile');
        });

        Schema::dropIfExists('users_codes');
    }
}
