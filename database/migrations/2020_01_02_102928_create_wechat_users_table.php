<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWechatUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_wechat', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('openid');
            $table->string('name');
            $table->string('nickname');
            $table->string('avatar', 512);

            $table->tinyInteger('sex');

            $table->string('country')->default('');
            $table->string('province')->default('');
            $table->string('city')->default('');

            $table->bigInteger('user_id');

            $table->timestamps();

            $table->index('openid');
            $table->index('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_wechat');
    }
}
