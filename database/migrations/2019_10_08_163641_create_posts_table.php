<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {

            $table->bigIncrements('id');

            $table->string('title', 128);
            $table->string('introduction', 512)->nullable()->default('');
            $table->string('thumb')->nullable()->default('');

            $table->string('author', 50)->nullable()->default('')->comment('作者');

            $table->integer('order', false, true)->nullable()->default(0);

            $table->bigInteger('column_id');
            $table->bigInteger('user_id');

            $table->longText('content')->nullable();
            $table->integer('view_num', false, true)->default(0);

            $table->boolean('visible')->nullable()->default(1)->comment('可见');
            $table->boolean('top')->nullable()->default(0)->comment('置顶');
            $table->boolean('hot')->nullable()->default(0)->comment('热门');
            $table->boolean('recommend')->nullable()->default(0)->comment('热门');

            $table->dateTime('publish_time');

            $table->timestamps();
            $table->softDeletes();

            $table->index('user_id');
            $table->index('column_id');
            $table->index('publish_time');
            $table->index('title');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
