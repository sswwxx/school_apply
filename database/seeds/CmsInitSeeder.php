<?php

use Encore\Admin\Auth\Database\Role;
use Illuminate\Database\Seeder;

class CmsInitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(\Encore\Admin\Auth\Database\AdminTablesSeeder::class);
        $this->addUserRole();
    }


    protected function addUserRole()
    {
        $roles = [
            [
                'name' => '学生',
                'slug' => 'student',
            ], [
                'name' => '招生老师',
                'slug' => 'zs_teacher',
            ], [
                'name' => '财务',
                'slug' => 'finance',
            ], [
                'name' => '教务',
                'slug' => 'senate',
            ],
        ];

        foreach ($roles as $role) Role::create($role);
    }

}
