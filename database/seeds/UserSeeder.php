<?php

use App\Models\User\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::beginTransaction();
        factory(User::class, 50)->create()->each(function (User $user) {

            $permission = \App\Models\Role::whereSlug('member')->first();
            $user->roles()->save($permission);

        });
        DB::commit();
    }
}
