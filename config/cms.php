<?php

return [

    'admin_column_add_number' => 100000,

    'route' => [
        'web' => [
            'controller' => "ContentController",
        ],
        'module' => [
            'controller' => 'ModuleController',
        ]
    ]
];
