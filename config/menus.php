<?php 
return [
  1 => 
  [
    'id' => 1,
    'parent_id' => 0,
    'order' => 1,
    'title' => '仪表盘',
    'icon' => 'fa-bar-chart',
    'uri' => '/',
    'permission' => NULL,
    'created_at' => NULL,
    'updated_at' => '2019-11-20 17:04:36',
  ],
  2 => 
  [
    'id' => 2,
    'parent_id' => 0,
    'order' => 7,
    'title' => '系统管理',
    'icon' => 'fa-tasks',
    'uri' => '',
    'permission' => NULL,
    'created_at' => NULL,
    'updated_at' => '2020-02-18 04:07:20',
  ],
  3 => 
  [
    'id' => 3,
    'parent_id' => 0,
    'order' => 2,
    'title' => '用户管理',
    'icon' => 'fa-users',
    'uri' => 'auth/users',
    'permission' => NULL,
    'created_at' => NULL,
    'updated_at' => '2020-02-18 04:04:45',
  ],
  7 => 
  [
    'id' => 7,
    'parent_id' => 2,
    'order' => 8,
    'title' => '操作日志',
    'icon' => 'fa-history',
    'uri' => 'auth/logs',
    'permission' => NULL,
    'created_at' => NULL,
    'updated_at' => '2020-02-18 04:07:20',
  ],
  8 => 
  [
    'id' => 8,
    'parent_id' => 3,
    'order' => 6,
    'title' => '学生管理',
    'icon' => 'fa-stumbleupon-circle',
    'uri' => '/students',
    'permission' => '',
    'created_at' => '2020-02-18 04:05:44',
    'updated_at' => '2020-02-18 04:07:20',
  ],
  9 => 
  [
    'id' => 9,
    'parent_id' => 3,
    'order' => 5,
    'title' => '老师管理',
    'icon' => 'fa-bars',
    'uri' => '/teachers',
    'permission' => '',
    'created_at' => '2020-02-18 04:06:13',
    'updated_at' => '2020-02-18 04:07:20',
  ],
  10 => 
  [
    'id' => 10,
    'parent_id' => 3,
    'order' => 4,
    'title' => '财务管理',
    'icon' => 'fa-bars',
    'uri' => '/finances',
    'permission' => '',
    'created_at' => '2020-02-18 04:06:34',
    'updated_at' => '2020-02-18 04:07:20',
  ],
  11 => 
  [
    'id' => 11,
    'parent_id' => 3,
    'order' => 3,
    'title' => '教务管理',
    'icon' => 'fa-bars',
    'uri' => '/senate',
    'permission' => '',
    'created_at' => '2020-02-18 04:06:53',
    'updated_at' => '2020-02-18 04:07:20',
  ],
];