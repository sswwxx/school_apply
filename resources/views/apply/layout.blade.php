<!doctype html>
<html lang="zh-CN">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ $title }}</title>

    <link rel="stylesheet" href="{{ asset('vendor/laravel-admin/AdminLTE/bootstrap/css/bootstrap.min.css') }}">

    <link rel="stylesheet" href="{{ asset('vendor/laravel-admin/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}">

    <script src="{{ asset('vendor/laravel-admin/AdminLTE/plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>
    <script src="{{ asset('vendor/laravel-admin/AdminLTE/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('vendor/laravel-admin/bootstrap-fileinput/js/fileinput.min.js') }}"></script>


    <script src="{{ asset('vendor/laravel-admin/moment/min/moment-with-locales.min.js') }}"></script>
    <script src="{{ asset('vendor/laravel-admin/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>

    <style>
        .input-group-addon {
            display: none;
        }

        .input-group {
            width: 100%;
        }

        input[type=text], select {
            width: 100%;
        }

        .form-group > label.asterisk:before {
            content: "* ";
            color: red;
        }

        pre {
            white-space: pre-wrap;
            word-wrap: break-word;
        }

    </style>

    <script>
        $(function () {
            $('select option:first-child').remove();
        });
    </script>

</head>
<body>

<nav class="navbar navbar-default">
    <div class="container-fluid text-center">
            <span style="font-size: 18px;height: 50px;display: inline-block;line-height: 50px">
                {{ $title }}
            </span>
    </div><!-- /.container-fluid -->
</nav>

@yield('body')

</body>
</html>