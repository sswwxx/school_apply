@extends('apply.layout')

@section('body')

    <div class="text-center">
        <p class="text-success">您的申请已经提交，请等待审核</p>

        <a href="{{ url()->current() }}?step=base">重新编辑</a>

    </div>

@endsection