@extends('apply.layout')

@section('body')

    <div class="container">
        <form action="{{ url()->current() }}" method="post" enctype="multipart/form-data">

            {!! csrf_field() !!}

            @foreach($fields as $field)

                {!! $field->render() !!}

            @endforeach

            <table class="table text-center">
                <tr>
                    <td @if($prev) colspan="2" @endif>
                        <label>
                            <input type="checkbox" required checked>
                            我已阅读并同意<a data-toggle="modal" data-target="#service">《服务条款及学员承诺书》</a>
                        </label>
                    </td>
                </tr>
                <tr>
                    @if($prev)
                        <td>
                            <a class="btn" href="{{ url()->current() }}?step={{ $prev }}">上一步</a>
                        </td>
                    @endif

                    @if($next)
                        <td>
                            <button class="btn">下一步</button>
                        </td>
                    @else
                        <td>
                            <button class="btn">提交信息</button>
                        </td>
                    @endif
                </tr>
            </table>


        </form>
    </div>

    <div class="modal fade" tabindex="-1" role="dialog" id="service">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">服务条款及学员承诺书</h4>
                </div>
                <div class="modal-body">
                    <p>服务条款：</p>
                    <pre>
1）报名时提供的专科毕业证书必须是国民教育系列，教育部电子注册，国家承认的毕业证书。对持不合格证书（如伪造证书、非国民教育系列证书、地方颁发只在地方承认的证书、未按教育部规定进行电子注册的高等教育毕业证书等）或伪造个人身份的学生，无论何时查出，均取消学籍，已经发生的费用不予退还。
2）本人同意贵校有权根据报名人数和录取等情况，自行调剂本人到其他专业报名、学习。
3）报考成人高考的学员：
录取前：
①已报名校企合作占用名额的学员，一律不退费；
②参加统考的学员，扣除实际产生的费用（如：报名费、资料费、礼品费等）,剩余的退费。
录取后：一律不退费。
4)如遇预报名院校学费上调时，学员需自行补齐差价。
                </pre>

                    <p>学员承诺书</p>
                    <pre>
本人报名参加文誉教育培训班，本人已了解培训的宗旨，清楚学校实施培训的目的，在此，本人向学校郑重承诺：
一、我自愿接受学校管理的考勤制度及相关的学习情况考核，并积极配合本校老师的工作；
二、我将努力学习，考试不违纪、不作弊，保证完成课程学习并顺利毕业；
三、不无故缺席，如有特殊情况不能如期到课，我将在事前向老师请假，并提交书面请假条；
四、不无故迟到，如有特殊情况不能准时上课，我将在课前至少半小时向老师请假；
五、上课期间端正学习态度，做到专心致志学习，不玩手机、不打游戏等。
                </pre>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>

    <script>
        $(function () {
            $('#id_number').on('blur', function (el) {
                var card_no = $(el.currentTarget).val();
                var birthday = '';

                if (card_no != null && card_no !== "") {
                    if (card_no.length === 15) {
                        birthday = "19" + card_no.substr(6, 6);
                    } else if (card_no.length === 18) {
                        birthday = card_no.substr(6, 8);
                    }
                }

                birthday = birthday.replace(/(.{4})(.{2})/, "$1-$2-");
                $('#birthday').val(birthday);
            });

            var options = {
                'format':'YYYY-MM-DD',
                'locale':'zh-CN',
                'allowInputToggle':true
            };
            $('#birthday').datetimepicker(options);
            $('#company_date').datetimepicker(options);

        });
    </script>
@endsection