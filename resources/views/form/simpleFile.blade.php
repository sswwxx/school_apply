<div class="form-group">
    <label>{{ $label }}</label>

    @if($value)
        <div style="width: 150px;height: 150px">
            <img style="max-width: 100%;max-height: 100%;vertical-align: middle" src="{{ site_storage($value) }}" alt="">
        </div>
    @endif
    <input type="file" class="{{$class}}" name="{{$name}}" {!! $attributes !!} />
    @include('admin::actions.form.help-block')



</div>
