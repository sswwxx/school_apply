<div class="{{$viewClass['form-group']}} {!! !$errors->has($errorKey) ? '' : 'has-error' !!}">

    <label for="{{$id}}" class="{{$viewClass['label']}} control-label">{{$label}}</label>

    <div class="{{$viewClass['field']}} media-picker" data-multiple="{{ $multiple }}">
        @include('admin::form.error')

        <input type="hidden" name="{{ $name }}[]" value="">
        <div class="media-tools">
            <input type="hidden" value="{{ $mimes }}">
            <span>点击选择{{ $label }}</span>
        </div>
        <div id="{{ $id }}-rows" class="media-rows flex-box sortable-box">
                @foreach($value as $src)

                    <div class="thumbnail media-thumbnail "  data-src="{{ $src }}">
                        <div class="preview"></div>
                        <div class="caption">
                            <p class="media-basename text-center">{{ basename($src) }}</p>
                            <p class="media-thumbnail-tools text-right">
                                <a target="_blank" href="{{ $src }}" class="btn btn-default btn-sm media-show" role="button">
                                    <i class="glyphicon glyphicon-zoom-in"></i>
                                </a>
                                <a href="javascript:" class="btn btn-default btn-sm media-remove" role="button">
                                    <i class="glyphicon glyphicon-trash"></i>
                                </a>

                            </p>
                        </div>
                        <input type="hidden" name="{{ $name }}[]" value="{{ $src }}">
                    </div>

                @endforeach
        </div>
        <div class="clearfix"></div>
        @include('admin::form.help-block')

        <template>

            <div class="thumbnail media-thumbnail">
                <div class="preview"></div>

                <div class="caption">

                    <p class="media-basename text-center"></p>

                    <p class="media-thumbnail-tools text-right">
                        <a target="_blank" href="#" class="btn btn-default btn-sm media-show" role="button">
                            <i class="glyphicon glyphicon-zoom-in"></i>
                        </a>
                        <a href="javascript:" class="btn btn-default btn-sm media-remove" role="button">
                            <i class="glyphicon glyphicon-trash"></i>
                        </a>

                    </p>

                </div>

                <input type="hidden" name="{{ $name }}[]" value="">

            </div>

        </template>

    </div>
</div>
