<script>var CK_UPLOAD_URL = "{{ $uploadUrl }}";</script>
<script src="{{ asset('vendor/ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('vendor/ckeditor/config.js') }}"></script>

<style>
    #editable-page-container {
        position: fixed;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        background-color: #ecf0f5;
        z-index: 9999;
        overflow: auto;
    }

    #editable-page-content {
        margin-top: 100px;
        background-color: white;
    }

    #editable-page-tools {
        text-align: right;
        position: fixed;
        width: 100%;
        height: 60px;
        line-height: 60px;
        z-index: 9999999;
        background-color: white;
        border-top: 1px solid #DDD;
        bottom: 0;
        left: 0;
    }

    #editable-page-tools a {
        color: white;
        display: inline-block;
        padding: 0 30px;
        background-color: #ff5050;
    }

    body {
        width: 100%;
        height: 100%;
        overflow: hidden !important;
    }

</style>

<div id="editable-page-container">
    <div id="editable-page-content">

        @empty($content)
            <h1 style="text-align: center">请在这里编辑内容</h1>
        @else
            {!! $content !!}
        @endempty

    </div>
</div>

<div id="editable-page-tools">
    <a href="javascript:saveEditablePage()"> 保存修改 </a>
</div>

<script>
    var introduction = document.getElementById('editable-page-content');
    introduction.setAttribute('contenteditable', true);

    CKEDITOR.inline('introduction');

    function saveEditablePage() {
        var data = CKEDITOR.instances['editable-page-content'].getData();

        $.ajax({
            url: "{{ $saveUrl }}",
            type: "post",
            data: {
                content: data,
                _token: '{{ csrf_token() }}'
            },
            success: function (res) {
                alert('保存成功！');
            }
        });

    }

    window.onbeforeunload = function (e) {
        var e = window.event || e;
        e.returnValue = ("离开当前页面将无法进行编辑？");
    }

</script>
