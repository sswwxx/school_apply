<iframe id="editable-page-view" src="{{ $url }}" frameborder="0" style="width: 100%;padding: 5px;height: 300px;background: white;"></iframe>

<script>

    $(function () {

        setTimeout(function () {
            var height = $('#pjax-container').height();
            $('#editable-page-view').height(height - 60);
        },1000);


    });
</script>
