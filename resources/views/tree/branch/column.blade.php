<i class='fa {{ $branch['icon'] }}'></i>&nbsp;
<strong>{{ $branch['name'] }}</strong>
<small class="text-info">[{{ $branch['id'] }}]</small>
@if($branch['status'] == 'HIDE')
    <small class="text-danger">[已隐藏]</small>
@endif
