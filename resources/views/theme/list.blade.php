<div class="theme-box">
    <form action="{{ route('theme-change') }}" method="post">
        {!! csrf_field() !!}

        @foreach($themes as $theme)
            <div class="thumbnail text-center ">
                <div class="caption">
                    <h3>{{ $theme->name() }}</h3>
                    <p class="desc text-left">{{ $theme->desc() }}</p>
                    <p class="text-right">
                        @if($current == $theme->slug())
                            <label>
                                <a href="{{ url()->current().'/'.$theme->slug().'/edit' }}">主题设置</a>
                            </label>
                        @else
                            <label>
                                <span class="text-success cur-pointer">使用这个主题</span>
                                <input class="hidden" type="radio" name="name" value="{{ $theme->slug() }}">
                            </label>
                        @endif

                    </p>
                </div>
            </div>
        @endforeach

    </form>

</div>

<script>
    $(function () {
        $('input[name="name"]').change(function () {
            $(this).closest('form').submit();
        });
    });
</script>
