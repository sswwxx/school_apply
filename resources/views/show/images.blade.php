@foreach($lists as $key => $label)

    @php($img = data_get($content,$key))

    @continue(empty($img))

    <div style="margin-bottom: 20px">
        <img style="max-width: 100%" src="{{ site_storage($img) }}" alt="">
        <br>
        <a target="_blank" download="{{ $apply['name'].'_'.$label }}" href="{{ site_storage($img) }}">{{ $label }}</a>

        <hr>
    </div>

@endforeach