<style>
    .files > li {
        float: left;
        width: 150px;
        border: 1px solid #eee;
        margin-bottom: 10px;
        margin-right: 10px;
        position: relative;
    }

    .files > li > .file-select {
        position: absolute;
        top: -4px;
        left: -1px;
    }

    .file-icon {
        text-align: center;
        font-size: 65px;
        color: #666;
        display: block;
        height: 100px;
    }

    .file-info {
        text-align: center;
        padding: 10px;
        background: #f4f4f4;
    }

    .file-name {
        font-weight: bold;
        color: #666;
        display: block;
        overflow: hidden !important;
        white-space: nowrap !important;
        text-overflow: ellipsis !important;
    }

    .file-size {
        color: #999;
        font-size: 12px;
        display: block;
    }

    .files {
        list-style: none;
        margin: 0;
        padding: 0;
    }

    .file-icon.has-img {
        padding: 0;
    }

    .file-icon.has-img > img {
        max-width: 100%;
        height: auto;
        max-height: 92px;
    }

    .main-sidebar, .main-header, .content-header, .main-footer {
        display: none;
    }

    .content-wrapper {
        padding-top: 0 !important;
        margin-left: 0 !important;
    }

    .confirm-box {
        position: fixed;
        left: 0;
        bottom: -60px;
        height: 60px;
        background-color: white;
        text-align: center;
        width: 100%;
        padding: 10px;
        transition: all .3s;
    }

    html {
        background-color: #ecf0f5;
    }
</style>

<script data-exec-on-popstate>

    $(function () {

        $('#moveModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            var name = button.data('name');

            var modal = $(this);
            modal.find('[name=path]').val(name)
            modal.find('[name=new]').val(name)
        });

        $('#urlModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            var url = button.data('url');

            $(this).find('input').val(url)
        });

        $('#file-move').on('submit', function (event) {

            event.preventDefault();

            var form = $(this);

            var path = form.find('[name=path]').val();
            var name = form.find('[name=new]').val();

            $.ajax({
                method: 'put',
                url: '{{ $url['move'] }}',
                data: {
                    path: path,
                    'new': name,
                    _token: LA.token,
                },
                success: function (data) {
                    $.pjax.reload('#pjax-container');

                    if (typeof data === 'object') {
                        if (data.status) {
                            toastr.success(data.message);
                        } else {
                            toastr.error(data.message);
                        }
                    }
                }
            });

            closeModal();
        });

        $('.file-upload').on('change', function () {
            $('.file-upload-form').submit();
        });

        $('#new-folder').on('submit', function (event) {

            event.preventDefault();

            var formData = new FormData(this);

            $.ajax({
                method: 'POST',
                url: '{{ $url['new-folder'] }}',
                data: formData,
                async: false,
                success: function (data) {
                    $.pjax.reload('#pjax-container');

                    if (typeof data === 'object') {
                        if (data.status) {
                            toastr.success(data.message);
                        } else {
                            toastr.error(data.message);
                        }
                    }
                },
                cache: false,
                contentType: false,
                processData: false
            });

            closeModal();
        });

        function closeModal() {
            $("#moveModal").modal('toggle');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
        }

        $('.media-reload').click(function () {
            $.pjax.reload('#pjax-container');
        });

        $('.goto-url button').click(function () {
            var path = $('.goto-url input').val();
            $.pjax({container: '#pjax-container', url: '{{ $url['index'] }}?path=' + path});
        });

        $('.file-select>input').iCheck({checkboxClass: 'icheckbox_minimal-blue'});

        $('.file-select input').iCheck({checkboxClass: 'icheckbox_minimal-blue'}).on('ifChanged', function () {
            var _this = this;
            ifChanged(_this);

            if (parseInt('{{ $multiple }}') === 1) return;
            if (!_this.checked) return;
            $('.file-select input').each(function (id,el) {
                if(el === _this) return;
                $(el).iCheck("uncheck");
            });

        });


        var list = [];

        function ifChanged(el) {
            var url = $(el).val();
            if (el.checked) {
                list.push(url);
            } else {
                var index = list.indexOf(url);
                if (index === -1) return;
                list.splice(index, 1);
            }

            var bottom = list.length > 0 ? 0 : -60;
            $('.confirm-box').css('bottom', bottom);
        }

        $('.confirm-box button').click(function () {

            window.opener.mediaPicked(list);
            window.close();

        });

    });


</script>

<div class="row">
    <!-- /.col -->
    <div class="col-md-12">
        <div class="box box-primary">

            <div class="box-body no-padding">

                <div class="mailbox-controls with-border">
                    <div class="btn-group">
                        <a href="" type="button" class="btn btn-default btn media-reload" title="Refresh">
                            <i class="fa fa-refresh"></i>
                        </a>
                    </div>
                    <!-- /.btn-group -->
                    <label class="btn btn-default btn"{{-- data-toggle="modal" data-target="#uploadModal"--}}>
                        <i class="fa fa-upload"></i>&nbsp;&nbsp;{{ trans('admin.upload') }}
                        <form action="{{ $url['upload'] }}" method="post" class="file-upload-form"
                              enctype="multipart/form-data" pjax-container>
                            <input type="file" name="files[]" class="hidden file-upload" multiple>
                            <input type="hidden" name="dir" value="{{ $url['path'] }}"/>
                            {{ csrf_field() }}
                        </form>
                    </label>

                    <!-- /.btn-group -->
                    <a class="btn btn-default btn" data-toggle="modal" data-target="#newFolderModal">
                        <i class="fa fa-folder"></i>&nbsp;&nbsp;{{ trans('admin.new_folder') }}
                    </a>

                    {{--<form action="{{ $url['index'] }}" method="get" pjax-container>--}}
                    <div class="input-group input-group-sm pull-right goto-url" style="width: 250px;">
                        <input type="text" name="path" class="form-control pull-right"
                               value="{{ '/'.trim($url['path'], '/') }}">

                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-default"><i class="fa fa-arrow-right"></i></button>
                        </div>
                    </div>
                    {{--</form>--}}

                </div>

                <!-- /.mailbox-read-message -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <ol class="breadcrumb" style="margin-bottom: 10px;">

                    <li><a href="{{ route('media-picker') }}"><i class="fa fa-th-large"></i> </a></li>

                    @foreach($nav as $item)
                        <li><a href="{{ $item['url'] }}"> {{ $item['name'] }}</a></li>
                    @endforeach
                </ol>
                <ul class="files clearfix">

                    @if (empty($list))
                        <li style="height: 200px;border: none;"></li>
                    @else
                        @foreach($list as $item)
                            <li>
                                @if(!$item['isDir'])
                                    <span class="file-select">
                                        <input type="checkbox" value="{{ $item['url'] }}"/>
                                    </span>
                                @endif
                                {!! $item['preview'] !!}

                                <div class="file-info">
                                    <a @if($item['isDir']) href="{{ $item['link'] }}" @endif
                                    class="file-name" title="{{ $item['name'] }}">
                                        {{ $item['icon'] }} {{ basename($item['name']) }}
                                    </a>
                                    <span class="file-size">{{ $item['size'] }}&nbsp;</span>
                                </div>
                            </li>
                        @endforeach

                    @endif
                </ul>
                {!! $paginator !!}
            </div>
            <!-- /.box-footer -->
            <!-- /.box-footer -->
        </div>
        <!-- /. box -->
    </div>
    <!-- /.col -->
</div>

<div class="modal fade" id="moveModal" tabindex="-1" role="dialog" aria-labelledby="moveModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="moveModalLabel">Rename & Move</h4>
            </div>
            <form id="file-move">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Path:</label>
                        <input type="text" class="form-control" name="new"/>
                    </div>
                    <input type="hidden" name="path"/>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary btn-sm">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="urlModal" tabindex="-1" role="dialog" aria-labelledby="urlModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="urlModalLabel">Url</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <input type="text" class="form-control"/>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="newFolderModal" tabindex="-1" role="dialog" aria-labelledby="newFolderModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="newFolderModalLabel">New folder</h4>
            </div>
            <form id="new-folder">
                <div class="modal-body">
                    <div class="form-group">
                        <input type="text" class="form-control" name="name"/>
                    </div>
                    <input type="hidden" name="dir" value="{{ $url['path'] }}"/>
                    {{ csrf_field() }}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary btn-sm">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="confirm-box">
    <button type="button" class="btn btn-lg"><i class="glyphicon glyphicon-ok"></i></button>
</div>
