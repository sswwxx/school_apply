@if($paginator['last'] > 1)
    <ul class="pagination pagination-sm no-margin pull-right">
    @php
        /** @var int $current */
        $current = $paginator['current'];
        $start = max($current - 4,1);
        $end = min($start + 7,$paginator['last']);
    @endphp

    <!-- Previous Page Link -->

        @if ($current == 1)
            <li class="page-item disabled"><span class="page-link">&laquo;</span></li>
        @else
            <li class="page-item"><a class="page-link" href="{{ $url.($current - 1) }}" rel="prev">&laquo;</a></li>
        @endif

        @for($page = $start;$page <= $end;$page ++)

            @if ($page == $current)
                <li class="page-item active"><span class="page-link">{{ $page }}</span></li>
            @else
                <li class="page-item"><a class="page-link" href="{{ $url.$page }}">{{ $page }}</a></li>
            @endif
        @endfor

    <!-- Next Page Link -->

        @if ($paginator['last'] != $current)
            <li class="page-item"><a class="page-link" href="{{ $url . ($current + 1) }}" rel="next">&raquo;</a></li>
        @else
            <li class="page-item disabled"><span class="page-link">&raquo;</span></li>
        @endif
    </ul>
@endif
